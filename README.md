# Chat Frontend
## 1. 개발 환경
- OS : Ubuntu 18.04 이상(WSL2 포함), MacOS
- NodeJS : 14 버전

## 2. 설치 및 실행
```bash
> git clone https://gitlab.com/chat25/frontend.git frontend
> cd frontend
```

API URL 설정 변경  
(Default : http://lshchat.kro.kr:8080)
```bash
> vi ./src/config/common.json

{
  "backendUrl": "http://lshchat.kro.kr:8080"  # backendUrl 수정
}
```

개발 서버 실행
```bash
> npm i
> npm start
```