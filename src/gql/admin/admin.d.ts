interface AdminLoginArgs {
  adminId: string
  password: string
}

interface AdminLoginResult {
  result: string
  errCode: 'INVALID_ADMIN' | 'SERVER_ERROR'
}

interface GetAllUsersInfoResult {
  result: {
    userUuid: string
    userId: string
    nickname: string
    name: string
    phone: string
    email: string
    isBan: boolean
  }[]
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface GetUserDetailInfoArgs {
  userUuid: string
}

interface UserDetailInfo {
  uuid: string
  userId: string
  nickname: string
  name: string
  phone: string
  email: string
  birth: string
  profileImage: string
  profileText: string
  registDate: string
  isBan: boolean
  connectionStatus: boolean
}

interface GetUserDetailInfoResult {
  result: UserDetailInfo
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface BanUsersArgs {
  userUuid: string[]
  banDay: number
}

interface BanUsersResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface UnbanUsersArgs {
  userUuid: string[]
}

interface UnbanUsersResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface GetBanHistoriesArgs {
  page: number
  pageSize: number
}

interface BanHistoriesData {
  uuid: string
  userUuid: string
  userId: string
  nickname: string
  name: string
  phone: string
  email: string
  isBan: boolean
  banDate: string
  unbanDate: string
  manualUnban: boolean
  updateDate: string
}

interface GetBanHistoriesResult {
  result: {
    rowCount: number
    page: number
    pageSize: number
    data: BanHistoriesData[]
  }
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface SendNoticeArgs {
  userUuid: string[]
  noticeDate: { start: string, end: string }
  noticeTitle: { ko: string, en: string }
  noticeContents: { ko: string, en: string }
  sendImmediately: boolean
  forceSignout: boolean
}

interface SendNoticeResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface GetNoticeHistoriesArgs {
  page: number
  pageSize: number
}

interface NoticeHistoriesData {
  uuid: string
  userUuid: string
  userId: string
  nickname: string
  noticeTitle: string
  noticeTitleEn: string
  registDate: string
}

interface GetNoticeHistoriesResult {
  result: {
    rowCount: number
    page: number
    pageSize: number
    data: NoticeHistoriesData[]
  }
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface GetNoticeDetailInfoArgs {
  noticeUuid: string
}

interface GetNoticeDetailInfoResult {
  result: {
    uuid: string
    noticeTitle: string
    noticeTitleEn: string
    noticeContents: string
    noticeContentsEn: string
    noticeStartDate: string
    noticeEndDate: string
    registDate: string
    read: boolean
  }
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}