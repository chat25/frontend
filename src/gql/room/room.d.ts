type ChatType = 'join' | 'leave' | 'text' | 'file' | 'image' | 'video' | 'audio';
type RoomType = 'personal' | 'group' | 'open';

interface CreateRoomArgs {
  type: RoomType
  name?: string
  users?: string[]
  maxUser: number
  isPrivate: boolean
  password: string
}

interface CreateRoomResult {
  result: string
  errCode: 'INVALID_TOKEN' | 'TOO_MANY_USER' | 'INVALID_USERS' | 'DUPLICATED_ROOM_NAME' | 'SERVER_ERROR'
}

interface GetPersonalRoomInfoArgs {
  userUuid: string
}

interface RoomInfo {
  uuid: string
  userUuid?: string
  type: RoomType
  originName?: string
  name?: string
  users: Partial<UserInfo>[]
  leaveUsers: Partial<UserInfo>[]
  maxUser?: number
  isPrivate?: boolean
  password?: string
  createDate: string
  blackList?: string[]
  messages: ChatInfo[]
  userNum: number
}

interface GetPersonalRoomInfoResult {
  result: RoomInfo
  errCode: 'INVALID_TOKEN' | 'INVALID_USER' | 'SERVER_ERROR'
}

interface GetRoomInfoArgs {
  roomUuid: string
}

interface GetRoomInfoResult {
  result: RoomInfo
  errCode: 'INVALID_TOKEN' | 'NO_ROOM' | 'SERVER_ERROR'
}

interface RoomListItem {
  uuid: string
  userUuid: string
  type: RoomType
  name: string
  users: Partial<UserInfo>[]
  maxUser: number
  isPrivate: boolean
  password: string
  createDate: String
  joinDate: String
  lastChat: ChatInfo
  userNum?: number
}

interface GetRoomListResult {
  result: {
    roomList: RoomListItem[]
    noReadNumJsonString: string
  }
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface GetMessagesArgs {
  roomUuid: string
  lastChatDate: string
}

interface GetMessagesResult {
  result: ChatInfo[]
  errCode: 'INVALID_TOKEN' | 'NO_ROOM' | 'SERVER_ERROR'
}

interface LeaveRoomArgs {
  roomUuid: string
  userUuid?: string
  blackList?: boolean
}

interface LeaveRoomResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface EnterRoomArgs {
  type?: 'group' | 'open'
  roomUuid: string
  userUuid: string[]
  password?: string
}

interface EnterRoomResult {
  result: string
  errCode: 'INVALID_TOKEN' | 'INVALID_USER' | 'WRONG_PASSWORD' | 'MAX_USER' | 'SERVER_ERROR'
}

interface GetOpenChatRoomArgs {
  roomName: string
  page?: number
}

interface GetOpenChatRoomResult {
  result: RoomListItem[]
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface RenameChatRoomArgs {
  roomUuid: string
  roomName?: string
}

interface RenameChatRoomResult {
  result: string
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface DelegateRoomMasterArgs {
  roomUuid: string
  userUuid: string
}

interface DelegateRoomMasterResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'INVALID_USER' | 'NO_USER' | 'SERVER_ERROR'
}

interface ChangeRoomSettingsArgs {
  roomUuid: string
  name: string
  maxUser: number
  password?: string
  isPrivate: boolean
}

interface ChangeRoomSettingsResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'DUPLICATED_ROOM_NAME' | 'INVALID_MAX_USER' | 'SERVER_ERROR'
}

interface SendUploadChatArgs {
  roomUuid: string
  type: UploadType
  files: FileList
  compress?: boolean
}

interface SendUploadChatResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'INVALID_FILE_TYPE' | 'SERVER_ERROR'
}