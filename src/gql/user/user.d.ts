interface CheckDuplicatedArgs {
  checkType: 'USERID' | 'NICKNAME'
  value: string
}

interface CheckDuplicatedResult {
  result: boolean | null
  errCode: 'SERVER_ERROR' | null
}

type Language = 'ko' | 'en';

interface EmailVerificationArgs {
  email: string
  lang: Language
  checkDuplicate: boolean
}

interface EmailVerificationResult {
  result: string
  errCode: 'DUPLICATE_ERROR' | 'SERVER_ERROR'
}

interface CheckEmailAuthCodeArgs {
  email: string
  authCode: string
  uuid: string
}

interface CheckEmailAuthCodeResult {
  result: {
    success: boolean
    userId: string | null
  }
  errCode: 'INVALID_AUTH_CODE' | 'SERVER_ERROR'
}

interface PhoneVerificationArgs {
  countryCode: string
  phone: string
  lang: Language
  checkDuplicate: boolean
}

interface PhoneVerificationResult {
  result: string
  errCode: 'DUPLICATE_ERROR' | 'SERVER_ERROR'
}

interface CheckPhoneAuthCodeArgs {
  countryCode: string
  phone: string
  authCode: string
  uuid: string
}

interface CheckPhoneAuthCodeResult {
  result: {
    success: boolean
    userId: string | null
  }
  errCode: 'INVALID_AUTH_CODE' | 'SERVER_ERROR'
}

interface SignUpArgs {
  userInfo: {
    userId: string
    password: string
    name: string
    nickname: string
    email: string
    countryCode: string
    phone: string
    birth: string
    profileImage: string
    profileText: string
  }
  uuid: {
    email: string
    phone: string
  }
}

interface SignUpResult {
  result: boolean
  errCode: 'EMAIL_AUTH_ERROR' | 'PHONE_AUTH_ERROR' | 'SERVER_ERROR'
}

interface CheckDuplicatedNameAndEmailArgs {
  name: string
  email: string
}

interface CheckDuplicatedNameAndEmailResult {
  result: boolean
  errCode: 'SERVER_ERROR'
}

interface CheckDuplicatedNameAndPhoneArgs {
  name: string
  countryCode: string
  phone: string
}

interface CheckDuplicatedNameAndPhoneResult {
  result: boolean
  errCode: 'SERVER_ERROR'
}

interface ResetPasswordArgs {
  email?: string
  countryCode?: string
  phone?: string
  password: string
  uuid: string
}

interface ResetPasswordResult {
  result: boolean
  errCode: 'WRONG_EMAIL_OR_PHONE_AUTH' | 'SERVER_ERROR'
}

interface LoginArgs {
  userId: string
  password: string
}

interface UserInfo {
  uuid?: string
  userId: string
  password?: string
  name: string
  nickname: string
  email: string
  countryCode: string
  phone: string
  birth: string
  profileImage: string
  profileText: string
  friends: string[]
  rooms: string[]
  registDate: string
}

interface TokenInfo {
  accessToken: string
  refreshToken: string
}

interface LoginResult {
  result: {
    userInfo?: UserInfo
    token?: TokenInfo
    banDate?: string
    unbanDate?: string
  }
  errCode: 'WRONG_LOGIN_INFO' | 'BANNED_USER' | 'SERVER_ERROR'
}

interface ModifyUserProfileArgs {
  nickname: string
  profileImage: string
  profileText: string
}

interface ModifyUserProfileResult {
  result: UserInfo
  errCode: 'INVALID_TOKEN' | 'INVALID_NICKNAME' | 'SERVER_ERROR'
}

interface ModifyUserInfoArgs {
  userInfo: {
    password?: string
    name?: string
    email?: string
    countryCode?: string
    phone?: string
    birth?: string
  }
  authType: 'EMAIL' | 'PHONE'
  uuid: {
    init: string
    email?: string
    phone?: string
  }
}

interface ModifyUserInfoResult {
  result: UserInfo
  errCode: 'INVALID_TOKEN' | 'WRONG_EMAIL_OR_PHONE_AUTH' | 'SERVER_ERROR'
}

interface RefreshAccessTokenArgs {
  uuid: string
}

interface RefreshAccessTokenResult {
  result: string
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface DeleteUserResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface NoticeData {
  uuid: string
  noticeTitle: string
  noticeTitleEn: string
  noticeContents: string
  noticeContentsEn: string
  noticeStartDate: string
  noticeEndDate: string
}

interface GetNoticesResult {
  result: NoticeData[]
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface ReadNoticeArgs {
  noticeUuid: string
}

interface ReadNoticeResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface GetMyInfoResult {
  result: UserInfo
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}