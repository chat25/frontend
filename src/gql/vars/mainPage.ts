import { makeVar } from '@apollo/client';
import apollo from '@/utils/apolloClient';
import { RENAME_CHAT_ROOM } from '@/gql/room';
import userState, { userVar } from '@/gql/vars/user';
import { GET_NOTICES } from '@/gql/user';

type PageType =
  | 'personalchat'
  | 'groupchat'
  | 'openchat'
  | 'settings';

interface NoticeModalData extends NoticeData {
  checked: boolean
  forceSignout: boolean
}

export interface MainPageVar {
  pageType?: PageType
  roomInfo?: RoomInfo
  uploading: boolean
  uploadSize: number
  noticeModal: NoticeModalData[]
}

const initialState: MainPageVar = {
  pageType: undefined,
  roomInfo: undefined,
  uploading: false,
  uploadSize: 0,
  noticeModal: []
};

const mainPageVar = makeVar<MainPageVar>(initialState);

export const setPageType = (pageType?: PageType) => mainPageVar({
  ...mainPageVar(),
  pageType
});

export const resetPageContainer = () => mainPageVar(initialState);

export const setRoomInfo = (roomInfo?: RoomInfo) => mainPageVar({
  ...mainPageVar(),
  roomInfo
});

export const setUploading = (uploading: boolean) => mainPageVar({
  ...mainPageVar(),
  uploading
});

export const setUploadSize = (uploadSize: number) => mainPageVar({
  ...mainPageVar(),
  uploadSize
});

export const setNoticeModalInfo = (notices: NoticeModalData[]) => mainPageVar({
  ...mainPageVar(),
  noticeModal: notices
});

export const closeNoticeModal = () => {
  if (mainPageVar().noticeModal[0].forceSignout) return window.location.href = '/login';

  mainPageVar({
    ...mainPageVar(),
    noticeModal: mainPageVar().noticeModal.slice(1)
  });
};

/** 유저별 채팅방 이름 변경 */
export const renameRoomName = async (roomUuid: string, roomName?: string) => {
  const { token } = userState.read();
  const { roomInfo } = mainPageVar();
  if (!roomInfo) return;

  try {
    const { data } = await apollo.mutate<{ renameChatRoom: RenameChatRoomResult }, RenameChatRoomArgs>({
      mutation: RENAME_CHAT_ROOM,
      variables: { roomUuid, roomName },
      context: {
        headers: { authorization: `Bearer ${token?.accessToken}` }
      }
    });

    if (!data) throw new Error('UNKOWN_ERROR');

    const { result, errCode } = data.renameChatRoom;

    if (errCode) {
      if (errCode === 'INVALID_TOKEN') return window.location.href = '/login';
      throw errCode;
    }

    if (result) {
      return mainPageVar({
        ...mainPageVar(),
        roomInfo: {
          ...roomInfo,
          name: result
        }
      });
    }
  } catch (err) {
    console.error(err);
  }
};

/** 공지 조회 */
export const getNotices = async (onlyLast: boolean = false, forceSignout: boolean = false) => {
  const { token } = userVar();
  if (!token) return;

  try {
    const { data } = await apollo.query<{ getNotices: GetNoticesResult }>({
      query: GET_NOTICES,
      context: {
        headers: { authorization: `Bearer ${token.accessToken}` }
      }
    });

    const { result, errCode } = data.getNotices;

    if (errCode) {
      if (errCode === 'INVALID_TOKEN') return window.location.href = '/login';
      throw errCode;
    }

    setNoticeModalInfo(onlyLast ? [{ ...result[0], checked: false, forceSignout }] : result.map(d => ({ ...d, checked: false, forceSignout: false })));
  } catch (err) {
    console.error(err);
  }
};

export default mainPageVar;