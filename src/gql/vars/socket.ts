import { makeVar } from '@apollo/client';
import { io, Socket } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';
import _ from 'lodash';
import drawerState, { refreshFriendsList, refreshRoomList, setNoReadNum, setRoomList } from '@/gql/vars/drawer';
import mainPageVar, { getNotices, resetPageContainer, setRoomInfo, setUploadSize } from '@/gql/vars/mainPage';
import userState, { openBanModal, userVar } from '@/gql/vars/user';
import { backendUrl } from '@/utils/apolloClient';

export interface SocketVar {
  socket?: Socket<DefaultEventsMap, DefaultEventsMap>
}

const initialState = {
  socket: undefined
};

const socketVar = makeVar<SocketVar>(initialState);

export const setSocket = (socket?: Socket<DefaultEventsMap, DefaultEventsMap>) => {
  const originSocket = socketVar().socket;
  if (!!originSocket && originSocket.connected) originSocket.disconnect();
  socketVar({ socket });
};

/** 소켓 연결 */
export const connect = () => {
  const { token } = userVar();
  if (!token) return;

  const socket = io(backendUrl, { path: '/sock', extraHeaders: { authorization: `Bearer ${token.accessToken}` } });

  /** 친구 목록 갱신 */
  socket.on('refresh-friends', () => {
    refreshFriendsList();
    socket.emit('join-rooms');
  });

  /** 채팅방 목록 갱신 */
  socket.on('refresh-rooms', () => {
    refreshRoomList();
    socket.emit('join-rooms');
  });

  /** Main Page Container 초기화 */
  socket.on('reset-page-container', () => {
    resetPageContainer();
    refreshFriendsList();
    refreshRoomList();
  });

  /** 메시지 수신 */
  socket.on('receive-msg', (chatInfo: ChatInfo, joinLeaveUserInfo?: Partial<UserInfo>, roomName?: string) => {
    const { userInfo } = userState.read();
    const { roomInfo } = mainPageVar();
    const { roomList, noReadNum } = drawerState.read();

    const isSelectedRoom = roomInfo?.uuid === chatInfo.roomUuid;

    console.log({ chatInfo, joinUserInfo: joinLeaveUserInfo, roomName });

    // 현재 입장한 채팅방에서 메시지를 수신했을 때
    if (isSelectedRoom) {
      if (userInfo && userInfo.uuid) chatInfo.read?.push(userInfo.uuid);
      socket.emit('read-msg', chatInfo.uuid);

      const userIndex = _.findIndex(chatInfo.type === 'join' ? roomInfo.leaveUsers : roomInfo.users, { uuid: chatInfo.userUuid });
      const userInfoClone = _.cloneDeep(roomInfo.users[userIndex]);

      setRoomInfo({
        ...roomInfo,
        name: roomName || roomInfo.name,
        users: chatInfo.type === 'leave' ? [...roomInfo.users.slice(0, userIndex), ...roomInfo.users.slice(userIndex + 1)] :
          (chatInfo.type === 'join' ? roomInfo.users.concat(joinLeaveUserInfo || []) : roomInfo.users),
        leaveUsers: chatInfo.type === 'leave' ? [...roomInfo.leaveUsers, userInfoClone] :
          (chatInfo.type === 'join' ? [...roomInfo.leaveUsers.slice(0, userIndex), ...roomInfo.leaveUsers.slice(userIndex + 1)] : roomInfo.leaveUsers),
        blackList: (chatInfo.type === 'leave' && !!joinLeaveUserInfo && !!joinLeaveUserInfo.uuid && !!roomInfo.blackList) ?
          [...roomInfo.blackList, joinLeaveUserInfo.uuid] :
          roomInfo.blackList,
        messages: [
          ...roomInfo.messages,
          chatInfo
        ],
        userNum: chatInfo.type === 'leave' ? roomInfo.userNum - 1 :
          (chatInfo.type === 'join' ? roomInfo.userNum + 1 : roomInfo.userNum)
      });
    }

    if (['join', 'leave'].indexOf(chatInfo.type) < 0) {
      const roomListClone = _.cloneDeep(roomList);
      const changedRoomInfo = _.find(roomListClone, { uuid: chatInfo.roomUuid });
      if (!changedRoomInfo) return refreshRoomList();

      changedRoomInfo.lastChat = chatInfo;
      roomListClone.sort((o1, o2) => {
        if (o1.lastChat.sendDate > o2.lastChat.sendDate) return -1;
        else if (o1.lastChat.sendDate < o2.lastChat.sendDate) return 1;
        else return 0;
      });

      setRoomList(roomListClone);
      if (isSelectedRoom) setNoReadNum(chatInfo.roomUuid, 0);
      else setNoReadNum(chatInfo.roomUuid, noReadNum[chatInfo.roomUuid] + 1);
    }

    console.log({ roomList: drawerState.read().roomList, noReadNum: drawerState.read().noReadNum });
  });

  /** 오픈 채팅방 방장이 변경되었을 때 */
  socket.on('delegate-room-master', (roomUuid: string, userUuid: string) => {
    const drawerVar = drawerState.getVar();
    const { roomList } = drawerVar();
    const roomListClone = _.cloneDeep(roomList);
    const roomItem = _.find(roomListClone, { uuid: roomUuid });
    if (!roomItem) return;
    roomItem.userUuid = userUuid;

    drawerState.set({
      ...drawerVar(),
      roomList: roomListClone
    });

    const { roomInfo } = mainPageVar();
    if (roomInfo?.uuid === roomUuid) mainPageVar({
      ...mainPageVar(),
      roomInfo: {
        ...roomInfo,
        userUuid
      }
    });
  });

  /** 오픈 채팅방 설정이 변경되었을 때 */
  socket.on('change-room-settings', (newRoomInfo: Partial<ChangeRoomSettingsArgs>, prevRoomName: string) => {
    const {
      roomUuid,
      name,
      maxUser,
      password,
      isPrivate
    } = newRoomInfo;
    if (!roomUuid) return;

    const drawerVar = drawerState.getVar();
    const { roomList } = drawerVar();
    const roomListClone = _.cloneDeep(roomList);
    const roomItem = _.find(roomListClone, { uuid: roomUuid });
    if (!roomItem) return;

    if (roomItem.name === prevRoomName) roomItem.name = name || roomItem.name;
    roomItem.maxUser = maxUser !== undefined ? maxUser : roomItem.maxUser;
    roomItem.password = password || '';
    roomItem.isPrivate = isPrivate !== undefined ? isPrivate : roomItem.isPrivate;

    drawerState.set({
      ...drawerVar(),
      roomList: roomListClone
    });

    const { roomInfo } = mainPageVar();
    if (roomInfo?.uuid === roomUuid) mainPageVar({
      ...mainPageVar(),
      roomInfo: {
        ...roomInfo,
        originName: name,
        name: roomItem.name,
        maxUser: roomItem.maxUser,
        password: roomItem.password,
        isPrivate: roomItem.isPrivate
      }
    });
  });

  /** 업로드 진행 */
  socket.on('uploading', (size: number) => {
    setUploadSize(size);
  });

  /** 사용자 정지 */
  socket.on('ban', (banDate, unbanDate) => {
    console.log({ banDate, unbanDate });
    openBanModal(banDate, unbanDate);
  });

  /** 공지 수신 */
  socket.on('notice', (forceSignout: boolean) => {
    getNotices(true, forceSignout);
  });

  setSocket(socket);
};

/** 소켓 연결 해제 */
export const disconnect = () => {
  setSocket();
};

export default socketVar;