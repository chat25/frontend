import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router';
import styled from 'styled-components';
import {
  Avatar,
  Button,
  IconButton,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Tooltip,
  Typography
} from '@mui/material';
import DatePicker from '@mui/lab/DatePicker';
import * as uuid from 'uuid';
import imageCompression from 'browser-image-compression';
import ReactCrop, { Crop } from 'react-image-crop';
import _ from 'lodash';
import ReplayRoundedIcon from '@mui/icons-material/ReplayRounded';
import DriveFolderUploadRoundedIcon from '@mui/icons-material/DriveFolderUploadRounded';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import PageContainer from '@/components/common/PageContainer';
import Text from '@/components/common/Text';
import Logo from '@/components/common/Logo';
import useTranslation from '@/hooks/useTranslation';
import countryCodes from '@/config/countryCodes.json';
import Modal from '@/components/common/Modal';
import getCroppedImg from '@/utils/getCroppedImg';
import { LineProgress } from '@/components/common/Progress';
import Backdrop from '@/components/common/Backdrop';
import {
  CHECK_DUPICATED_USER_INFO,
  CHECK_EMAIL_AUTHCODE,
  CHECK_PHONE_AUTHCODE,
  EMAIL_VERIFICATION,
  PHONE_VERIFICATION,
  SIGN_UP
} from '@/gql/user';
import validate from '@/utils/validate';
import Timer from '@/components/common/Timer';
import { useReactiveVar } from '@apollo/client';
import commonState from '@/gql/vars/common';
import useApollo from '@/hooks/useApollo';

interface InputInfo {
  error: boolean
  helperText: React.ReactNode
}

interface InputInfos {
  userId: InputInfo
  password: InputInfo
  passwordConfirm: InputInfo
  name: InputInfo
  email: InputInfo
  emailAuthCode: InputInfo
  phone: InputInfo
  phoneAuthCode: InputInfo
  nickname: InputInfo
  birth: InputInfo
}

const Container = styled(PageContainer)`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  flex: 1;
  gap: 30px;
  padding: 15px;
  overflow-y: auto;

  .sign-up-title {
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 15px;

    .logo {
      width: 80px;
      height: 80px;
    }
  }

  .sign-up-wrapper {
    display: flex;
    flex-direction: column;
    width: 460px;
    max-width: 100%;
    gap: 20px;

    .input-wrapper {
      display: flex;
      justify-content: space-between;
      align-items: flex-start;
      gap: 10px;

      .success-helper-text {
        color: ${({ theme }) => theme === 'light' ? '#0c9131' : '#0ed145'};
      }

      .refresh-auth-btn {
        margin-top: 7px;

        .reset-auth-icon {
          fill: ${({ theme }) => theme === 'light' ? '#000' : '#fff'};
        }
      }
    }

    .profile-img-wrapper {
      display: flex;
      justify-content: center;
      margin-bottom: 20px;

      .profile-img-label {
        position: relative;
        display: flex;
        justify-content: center;

        .profile-img {
          width: 150px;
          height: 150px;
          cursor: pointer;
        }

        .reset-profile-img-icon {
          position: absolute;
          width: 25px;
          height: 25px;
          top: 0;
          transform: translate(80px, 0);
          fill: ${({ theme }) => theme === 'light' ? '#3f3f3f' : '#ccc'};
          cursor: pointer;
        }
  
        .upload-icon {
          position: absolute;
          width: 35px;
          height: 35px;
          bottom: 0;
          transform: translate(50px, 0);
          background-color: #dacc0b;
          cursor: pointer;
        }
      }
    }
  }

  .bottom-btn-wrapper {
    width: 460px;
    max-width: 100%;
    display: flex;
    justify-content: flex-end;
    gap: 10px;
  }

  .progress-container {
    position: fixed;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    gap: 20px;
    z-index: 10;
  }
`;

const SignUp = () => {
  /**
   * Step 1. 사용자 개인 정보 입력
   * Step 2. 프로필 정보 입력
   */
  const [step, setStep] = useState<number>(1);
  const [userId, setUserId] = useState<string>('');
  const [isValidUserId, setIsValidUserId] = useState<boolean>(false);
  const [password, setPassword] = useState<string>('');
  const [passwordConfirm, setPasswordConfirm] = useState<string>('');
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [isMailSend, setIsMailSend] = useState<boolean>(false);
  const [emailAuthUuid, setEmailAuthUuid] = useState<string>('');
  const [emailAuthCode, setEmailAuthCode] = useState<string>('');
  const [canResetEmailAuth, setCanResetEmailAuth] = useState<boolean>(false);
  const [isValidEmail, setIsValidEmail] = useState<boolean>(false);
  const [countryCode, setCountryCode] = useState<string>('82');
  const [phone, setPhone] = useState<string>('');
  const [isSmsSend, setIsSmsSend] = useState<boolean>(false);
  const [phoneAuthUuid, setPhoneAuthUuid] = useState<string>('');
  const [phoneAuthCode, setPhoneAuthCode] = useState<string>('');
  const [canResetPhoneAuth, setCanResetPhoneAuth] = useState<boolean>(false);
  const [isValidPhone, setIsValidPhone] = useState<boolean>(false);
  const [birth, setBirth] = useState<string>(new Date().toISOString());
  const [originProfileImage, setOriginProfileImage] = useState<string>('');
  const [profileImage, setProfileImage] = useState<string>('');
  const [profileImageMimeType, setProfileImageMimeType] = useState<string>('');
  const [nickname, setNickname] = useState<string>('');
  const [isValidNickname, setIsValidNickname] = useState<boolean>(false);
  const [compressing, setCompressing] = useState<boolean>(false);
  const [compressProgress, setCompressProgress] = useState<number>(0);
  const [cropModalOpen, setCropModalOpen] = useState<boolean>(false);
  const [crop, setCrop] = useState<Partial<Crop>>({ aspect: 1 });
  const [cropImage, setCropImage] = useState<HTMLImageElement | null>(null);
  const [profileText, setProfileText] = useState<string>('');
  const [completeModalOpen, setCompleteModalOpen] = useState<boolean>(false);
  const [errorModalOpen, setErrorModalOpen] = useState<boolean>(false);
  const [errorModalMessageKey, setErrorModalMessageKey] = useState<string>('');
  const [inputInfo, setInputInfo] = useState<InputInfos>({
    userId: { error: false, helperText: '' },
    password: { error: false, helperText: '' },
    passwordConfirm: { error: false, helperText: '' },
    name: { error: false, helperText: '' },
    email: { error: false, helperText: '' },
    emailAuthCode: { error: false, helperText: '' },
    phone: { error: false, helperText: '' },
    phoneAuthCode: { error: false, helperText: '' },
    nickname: { error: false, helperText: '' },
    birth: { error: false, helperText: '' }
  });

  const { theme, lang } = useReactiveVar(commonState.getVar());

  const _profile_img_file = useRef<HTMLInputElement>(null);

  const t = useTranslation();
  const navigate = useNavigate();
  const apollo = useApollo();

  /** 아이디 변경 이벤트 */
  const handleChangeUserId = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(_.cloneDeep(inputInfo));
    inputInfoClone.userId = { error: false, helperText: '' };

    setUserId(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 비밀번호 변경 이벤트 */
  const handleChangePassword = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(_.cloneDeep(inputInfo));
    inputInfoClone.password = { error: false, helperText: '' };

    setPassword(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 비밀번호 확인 변경 이벤트 */
  const handleChangePasswordConfirm = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(_.cloneDeep(inputInfo));
    inputInfoClone.passwordConfirm = { error: false, helperText: '' };

    setPasswordConfirm(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 이름 변경 이벤트 */
  const handleChangeName = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(_.cloneDeep(inputInfo));
    inputInfoClone.name = { error: false, helperText: '' };

    setName(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 이메일 변경 이벤트 */
  const handleChangeEmail = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.email = { error: false, helperText: '' };

    setEmail(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 이메일 인증코드 변경 이벤트 */
  const handleChangeEmailAuthCode = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { value } = e.target;
    const isNumberString = validate('numberstring', value);
    if ((value !== '' && !isNumberString) || value.length > 6) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.emailAuthCode = { error: false, helperText: '' };

    setEmailAuthCode(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 국가 코드 변경 이벤트 */
  const handleChangeCountryCode = useCallback((e: SelectChangeEvent<string>) => {
    setCountryCode(e.target.value);
  }, []);

  /** 휴대전화번호 변경 이벤트 */
  const handleChangePhone = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { value } = e.target;
    const isNumberString = validate('numberstring', value);
    if (value !== '' && !isNumberString) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.phone = { error: false, helperText: '' };

    setPhone(value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 휴대전화 인증코드 변경 이벤트 */
  const handleChangePhoneAuthCode = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { value } = e.target;
    const isNumberString = validate('numberstring', value);
    if ((value !== '' && !isNumberString) || value.length > 6) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.phoneAuthCode = { error: false, helperText: '' };

    setPhoneAuthCode(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 생일 변경 이벤트 */
  const handleChangeBirth = useCallback((birth: Date | null) => {
    if (!birth) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.birth = { error: false, helperText: '' };

    setBirth(birth.toISOString());
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 프로필 이미지 파일 변경 이벤트 */
  const handleChangeProfileImage: React.ChangeEventHandler<HTMLInputElement> = useCallback(async e => {
    if (!e.target.files || e.target.files.length === 0 || e.target.files[0].type.indexOf('image') < 0) {
      setOriginProfileImage('');
      setProfileImageMimeType('');
      setCrop({ aspect: 1 });
      return;
    }

    const file = e.target.files[0];

    console.log(`0. Origin Image Size: ${file.size} Byte`);
    console.log('1. Compress Start');
    setCompressProgress(0);
    setCompressing(true);
    const compressedFile = await imageCompression(file, {
      maxWidthOrHeight: 400,
      fileType: 'image/jpeg',
      maxSizeMB: 5,
      onProgress: progress => {
        console.log(progress);
        setCompressProgress(progress);
        if (progress === 100) setTimeout(() => {
          setCompressing(false)
        }, 500);
      }
    });
    console.log('2. Get Base64 Image');
    const compressedImage = await imageCompression.getDataUrlFromFile(compressedFile);
    console.log(`3. Compressed Image Size: ${compressedImage.length} Byte`);

    setOriginProfileImage('');
    setCrop({ aspect: 1 });
    setOriginProfileImage(compressedImage);
    setCropModalOpen(true);
  }, []);

  /** 프로필 이미지 편집 모달 확인 */
  const handleConfirmProfileImageCropModal = useCallback(() => {
    const profileImage = getCroppedImg(cropImage, crop, profileImageMimeType);
    console.log(crop);
    if (!profileImage) return;

    if (_profile_img_file.current) {
      _profile_img_file.current.value = '';
    }

    setProfileImage(profileImage);
    setCropModalOpen(false);
    setOriginProfileImage('');
  }, [crop, cropImage, profileImageMimeType]);

  /** 프로필 이미지 편집 모달 닫기 */
  const handleCloseProfileImageCropModal = useCallback(() => {
    if (_profile_img_file.current) {
      _profile_img_file.current.value = '';
    }

    setCropModalOpen(false);
    setOriginProfileImage('');
  }, []);

  /** 닉네임 변경 이벤트 */
  const handleChangeNickname = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(_.cloneDeep(inputInfo));
    inputInfoClone.nickname = { error: false, helperText: '' };

    setNickname(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 상태 메시지 변경 이벤트 */
  const handleChangeProfileText = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setProfileText(e.target.value);
  }, []);

  /** 취소 버튼 클릭 이벤트 */
  const handleClickCancelButton = useCallback(() => {
    navigate('/login');
  }, [navigate]);

  /** 인증코드 초기화 버튼 클릭 이벤트 */
  const handleClickResetButton = useCallback((type: 'userId' | 'email' | 'phone' | 'nickname') => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    switch (type) {
      case 'userId':
        inputInfoClone.userId = { error: false, helperText: '' };
        setUserId('');
        setIsValidUserId(false);
        break;
      case 'email':
        inputInfoClone.email = { error: false, helperText: '' };
        inputInfoClone.emailAuthCode = { error: false, helperText: '' };
        setEmailAuthCode('');
        setEmailAuthUuid('');
        setCanResetEmailAuth(false);
        setIsMailSend(false);
        setIsValidEmail(false);
        break;
      case 'phone':
        inputInfoClone.phone = { error: false, helperText: '' };
        inputInfoClone.phoneAuthCode = { error: false, helperText: '' };
        setPhoneAuthCode('');
        setPhoneAuthUuid('');
        setCanResetPhoneAuth(false);
        setIsSmsSend(false);
        setIsValidPhone(false);
        break;
      case 'nickname':
        inputInfoClone.userId = { error: false, helperText: '' };
        setNickname('');
        setIsValidNickname(false);
    }

    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 아이디, 닉네임 중복 확인 */
  const checkValidUserIdOrNickname = useCallback(async (checkType: 'USERID' | 'NICKNAME', value: string) => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    let isCorrectValue = true;

    if (checkType === 'USERID') {
      if (!validate('userid', value)) {
        inputInfoClone.userId = { error: true, helperText: 'pleaseEnterCorrectUserId' };
        isCorrectValue = false;
      }
    } else {
      if (!validate('nickname', value)) {
        inputInfoClone.nickname = { error: true, helperText: 'pleaseEnterCorrectNickname' };
        isCorrectValue = false;
      }
    }

    if (!isCorrectValue) return setInputInfo(inputInfoClone);

    try {
      const data = await apollo.query<{ checkDuplicatedUserInfo: CheckDuplicatedResult }, CheckDuplicatedArgs>({
        query: CHECK_DUPICATED_USER_INFO,
        variables: { checkType, value }
      });

      if (data.checkDuplicatedUserInfo.errCode) throw data.checkDuplicatedUserInfo.errCode;

      const { checkDuplicatedUserInfo: { result } } = data;

      if (checkType === 'USERID') {
        if (result) {
          inputInfoClone.userId = {
            error: true,
            helperText: 'duplicatedUserId'
          };
        } else {
          inputInfoClone.userId = {
            error: false,
            helperText: <span className='success-helper-text'>{t('usableUserId')}</span>
          };
        }
        setIsValidUserId(!result);
        setInputInfo(inputInfoClone);
      } else {
        if (result) {
          inputInfoClone.nickname = {
            error: true,
            helperText: 'duplicatedNickname'
          };
        } else {
          inputInfoClone.nickname = {
            error: false,
            helperText: <span className='success-helper-text'>{t('usableNickname')}</span>
          };
        }
        setIsValidNickname(!result);
        setInputInfo(inputInfoClone);
      }
    } catch (err) {
      console.error(err);
    }
  }, [inputInfo, apollo, t]);

  /** 이메일 인증 */
  const verifyEmail = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (!validate('email', email)) {
      inputInfoClone.email = { error: true, helperText: 'pleaseEnterCorrectEmail' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.query<{ emailVerification: EmailVerificationResult }, EmailVerificationArgs>({
        query: EMAIL_VERIFICATION,
        variables: { email, lang, checkDuplicate: true }
      });

      if (data.emailVerification.errCode) {
        if (data.emailVerification.errCode === 'DUPLICATE_ERROR') {
          inputInfoClone.email = { error: true, helperText: 'duplicatedEmail' };
          setInputInfo(inputInfoClone);
        }
        throw data.emailVerification.errCode;
      }

      const { emailVerification: { result } } = data;
      inputInfoClone.email = { error: false, helperText: 'waitOneMinuteForResendAuthCode' };
      inputInfoClone.emailAuthCode = { error: false, helperText: '' };

      setEmailAuthUuid(result);
      setIsMailSend(true);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [lang, email, inputInfo, apollo]);

  /** 이메일 인증코드 확인 */
  const checkEmailAuthCode = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (emailAuthCode.length < 6) {
      inputInfoClone.emailAuthCode = { error: true, helperText: 'pleaseEnterAuthCode' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.mutate<{ checkEmailAuthCode: CheckEmailAuthCodeResult }, CheckEmailAuthCodeArgs>({
        mutation: CHECK_EMAIL_AUTHCODE,
        variables: {
          email,
          authCode: emailAuthCode,
          uuid: emailAuthUuid
        }
      });

      if (data.checkEmailAuthCode.errCode) {
        if (data.checkEmailAuthCode.errCode === 'INVALID_AUTH_CODE') {
          inputInfoClone.emailAuthCode = { error: true, helperText: 'wrongAuthCode' };
          setInputInfo(inputInfoClone);
        }
        throw data.checkEmailAuthCode.errCode;
      }

      inputInfoClone.email = {
        error: false,
        helperText: <span className='success-helper-text'>{t('usableEmail')}</span>
      };
      inputInfoClone.emailAuthCode = { error: false, helperText: '' };

      setIsValidEmail(data.checkEmailAuthCode.result.success);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [email, emailAuthCode, emailAuthUuid, inputInfo, apollo, t]);

  /** 휴대전화 인증 */
  const verifyPhone = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (!phone) {
      inputInfoClone.phone = { error: true, helperText: 'pleaseEnterPhone' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.query<{ phoneVerification: PhoneVerificationResult }, PhoneVerificationArgs>({
        query: PHONE_VERIFICATION,
        variables: { countryCode, phone, lang, checkDuplicate: true }
      });

      if (data.phoneVerification.errCode) {
        if (data.phoneVerification.errCode === 'DUPLICATE_ERROR') {
          inputInfoClone.phone = { error: true, helperText: 'duplicatedPhone' };
          setInputInfo(inputInfoClone);
        }
        throw data.phoneVerification.errCode;
      }

      const { phoneVerification: { result } } = data;
      inputInfoClone.phone = { error: false, helperText: 'waitOneMinuteForResendAuthCode' };
      inputInfoClone.phoneAuthCode = { error: false, helperText: '' };

      setPhoneAuthUuid(result);
      setIsSmsSend(true);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [lang, countryCode, phone, inputInfo, apollo]);

  /** 휴대전화 인증코드 확인 */
  const checkPhoneAuthCode = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (phoneAuthCode.length < 6) {
      inputInfoClone.phoneAuthCode = { error: true, helperText: 'pleaseEnterAuthCode' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.mutate<{ checkPhoneAuthCode: CheckPhoneAuthCodeResult }, CheckPhoneAuthCodeArgs>({
        mutation: CHECK_PHONE_AUTHCODE,
        variables: {
          countryCode,
          phone,
          authCode: phoneAuthCode,
          uuid: phoneAuthUuid
        }
      });

      if (data.checkPhoneAuthCode.errCode) {
        if (data.checkPhoneAuthCode.errCode === 'INVALID_AUTH_CODE') {
          inputInfoClone.phoneAuthCode = { error: true, helperText: 'wrongAuthCode' };
          setInputInfo(inputInfoClone);
        }
        throw data.checkPhoneAuthCode.errCode;
      }

      inputInfoClone.phone = {
        error: false,
        helperText: <span className='success-helper-text'>{t('usablePhone')}</span>
      };
      inputInfoClone.phoneAuthCode = { error: false, helperText: '' };

      setIsValidPhone(data.checkPhoneAuthCode.result.success);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [countryCode, phone, phoneAuthCode, phoneAuthUuid, inputInfo, apollo, t]);

  /** 다음 버튼 클릭 */
  const handleClickNextButton = useCallback(() => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    let canMoveNextStep = true;

    if (!isValidUserId) {
      inputInfoClone.userId = { error: true, helperText: 'pleaseCheckUserIdDuplication' };
      canMoveNextStep = false;
    }

    if (!validate('password', password)) {
      inputInfoClone.password = { error: true, helperText: 'pleaseEnterCorrectPassword' };
      canMoveNextStep = false;
    }

    if (password !== passwordConfirm) {
      inputInfoClone.passwordConfirm = { error: true, helperText: 'pleaseEnterSamePassword' };
      canMoveNextStep = false;
    }

    if (!passwordConfirm) {
      inputInfoClone.passwordConfirm = { error: true, helperText: 'pleaseConfirmPassword' };
      canMoveNextStep = false;
    }

    if (!name) {
      inputInfoClone.name = { error: true, helperText: 'pleaseEnterName' };
      canMoveNextStep = false;
    }

    if (!isValidEmail) {
      inputInfoClone.email = { error: true, helperText: 'pleaseVerifyEmail' };
      canMoveNextStep = false;
    }

    if (!isValidPhone) {
      inputInfoClone.phone = { error: true, helperText: 'pleaseVerifyPhone' };
      canMoveNextStep = false;
    }

    if (!birth) {
      inputInfoClone.birth = { error: true, helperText: 'pleaseEnterCorrectBirth' };
      canMoveNextStep = false;
    }

    if (canMoveNextStep) {
      inputInfoClone.password = { error: false, helperText: '' };
      inputInfoClone.passwordConfirm = { error: false, helperText: '' };
      inputInfoClone.name = { error: false, helperText: '' };
      inputInfoClone.birth = { error: false, helperText: '' };
      setStep(2);
    }

    setInputInfo(inputInfoClone);
  }, [isValidUserId, password, passwordConfirm, name, isValidEmail, isValidPhone, birth, inputInfo]);

  /** 완료버튼 클릭 이벤트 */
  const handleClickCompleteButton = useCallback(() => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (!isValidNickname) {
      inputInfoClone.nickname = { error: true, helperText: 'pleaseCheckNicknameDuplication' };
      return setInputInfo(inputInfoClone);
    }

    setCompleteModalOpen(true);
  }, [isValidNickname, inputInfo]);

  /** 완료 모달 확인 버튼 클릭 이벤트 */
  const handleClickCompleteConfirmButton = useCallback(async () => {
    try {
      const data = await apollo.mutate<{ signUp: SignUpResult }, SignUpArgs>({
        mutation: SIGN_UP,
        variables: {
          userInfo: { userId, password, name, nickname, email, countryCode, phone, birth, profileImage, profileText },
          uuid: { email: emailAuthUuid, phone: phoneAuthUuid }
        }
      });

      if (data.signUp.errCode) {
        switch (data.signUp.errCode) {
          case 'EMAIL_AUTH_ERROR': case 'PHONE_AUTH_ERROR':
            setErrorModalMessageKey('emailOrPhoneAuthError');
            break;
          default:
            setErrorModalMessageKey('serverErrorOccurrence');
        }
        return setErrorModalOpen(true);
      }

      if (data.signUp.result) return navigate('/login');

      throw new Error('UNKOWN_ERROR');
    } catch (err) {
      console.error(err);
    }
  }, [userId, password, name, nickname, email, countryCode, phone, birth, profileImage, profileText, emailAuthUuid, phoneAuthUuid, apollo, navigate]);

  /** 인증코드 타이머 종료 이벤트 */
  const handleTimerEnd = useCallback((authCodeType: 'email' | 'phone') => {
    if (authCodeType === 'email') {
      setCanResetEmailAuth(false);
      return setIsMailSend(false);
    }

    setCanResetPhoneAuth(false);
    setIsSmsSend(false);
  }, []);

  /** 이메일 인증코드 타이머 */
  const emailAuthTimer = useMemo(() => (
    <Timer
      seconds={300}
      onEnd={() => handleTimerEnd('email')}
      style={{ display: 'block', width: '41px' }}
    />
  ), [handleTimerEnd]);

  /** 휴대전화 인증코드 타이머 */
  const phoneAuthTimer = useMemo(() => (
    <Timer
      seconds={300}
      onEnd={() => handleTimerEnd('phone')}
      style={{ display: 'block', width: '41px' }}
    />
  ), [handleTimerEnd]);

  /** 이메일 인증코드 전송 1분 후 초기화 버튼 활성 */
  useEffect(() => {
    let timeout: any;
    if (isMailSend) setTimeout(() => {
      setCanResetEmailAuth(true);
    }, 61000);

    return () => {
      clearTimeout(timeout);
    };
  }, [isMailSend]);

  /** 휴대전화 인증코드 전송 1분 후 초기화 버튼 활성 */
  useEffect(() => {
    let timeout: any;
    if (isSmsSend) setTimeout(() => {
      setCanResetPhoneAuth(true);
    }, 61000);

    return () => {
      clearTimeout(timeout);
    };
  }, [isSmsSend]);

  return (
    <Container theme={theme}>
      <div className='sign-up-title'>
        <Logo className='logo'/>
        <Text variant='h4' fontWeight='bold'>{t('signUp')}</Text>
      </div>

      <div className='sign-up-wrapper'>
        {step === 1 && (
          <>
            {/* 아이디 */}
            <div className='input-wrapper'>
              <TextField
                variant='outlined'
                value={userId}
                error={inputInfo.userId.error}
                helperText={typeof inputInfo.userId.helperText === 'string' ? t(inputInfo.userId.helperText) : inputInfo.userId.helperText}
                required
                onChange={handleChangeUserId}
                label={t('userId')}
                disabled={isValidUserId}
                style={{ flex: 1 }}
              />

              <Button
                variant='contained'
                disabled={isValidUserId}
                onClick={() => checkValidUserIdOrNickname('USERID', userId)}
                style={{ height: '56px' }}
              >
                {t('duplicateCheck')}
              </Button>

              {isValidUserId && (
                <IconButton
                  className='refresh-auth-btn'
                  onClick={() => handleClickResetButton('userId')}
                >
                  <ReplayRoundedIcon className='reset-auth-icon'/>
                </IconButton>
              )}
            </div>

            {/* 비밀번호 */}
            <TextField
              variant='outlined'
              value={password}
              error={inputInfo.password.error}
              helperText={typeof inputInfo.password.helperText === 'string' ? t(inputInfo.password.helperText) : inputInfo.password.helperText}
              required
              onChange={handleChangePassword}
              label={t('password')}
              type='password'
            />

            {/* 비밀번호 확인 */}
            <TextField
              variant='outlined'
              value={passwordConfirm}
              error={inputInfo.passwordConfirm.error}
              helperText={typeof inputInfo.passwordConfirm.helperText === 'string' ? t(inputInfo.passwordConfirm.helperText) : inputInfo.passwordConfirm.helperText}
              required
              onChange={handleChangePasswordConfirm}
              label={t('passwordConfirm')}
              type='password'
            />

            {/* 이름 */}
            <TextField
              variant='outlined'
              value={name}
              error={inputInfo.name.error}
              helperText={typeof inputInfo.name.helperText === 'string' ? t(inputInfo.name.helperText) : inputInfo.name.helperText}
              required
              onChange={handleChangeName}
              label={t('name')}
            />

            {/* 이메일 */}
            <div className='input-wrapper'>
              <TextField
                variant='outlined'
                value={email}
                error={inputInfo.email.error}
                helperText={typeof inputInfo.email.helperText === 'string' ? t(inputInfo.email.helperText) : inputInfo.email.helperText}
                disabled={isMailSend || isValidEmail}
                required
                onChange={handleChangeEmail}
                label={t('email')}
                style={{ flex: 1 }}
              />

              {!isValidEmail && (
                <Button
                  variant='contained'
                  onClick={verifyEmail}
                  disabled={isMailSend || isValidEmail}
                  style={{ height: '56px' }}
                >
                  {t('verify')}
                </Button>
              )}

              {isValidEmail && (
                <IconButton
                  className='refresh-auth-btn'
                  onClick={() => handleClickResetButton('email')}
                >
                  <ReplayRoundedIcon className='reset-auth-icon'/>
                </IconButton>
              )}
            </div>

            {/* 이메일 인증 코드 */}
            {isMailSend && !isValidEmail && (
              <div className='input-wrapper'>
                <TextField
                  variant='outlined'
                  value={emailAuthCode}
                  error={inputInfo.emailAuthCode.error}
                  helperText={typeof inputInfo.emailAuthCode.helperText === 'string' ? t(inputInfo.emailAuthCode.helperText) : inputInfo.emailAuthCode.helperText}
                  required
                  onChange={handleChangeEmailAuthCode}
                  label={`${t('email')} ${t('authCode')}`}
                  style={{ flex: 1 }}
                />

                <Button
                  variant='contained'
                  onClick={checkEmailAuthCode}
                  style={{ height: '56px' }}
                >
                  {t('confirm')}
                </Button>

                {canResetEmailAuth && (
                  <IconButton
                    className='refresh-auth-btn'
                    onClick={() => handleClickResetButton('email')}
                  >
                    <ReplayRoundedIcon className='reset-auth-icon'/>
                  </IconButton>
                )}

                <div style={{ paddingTop: '18px' }}>
                  {emailAuthTimer}
                </div>
              </div>
            )}

            {/* 국가코드, 휴대전화번호 */}
            <div className='input-wrapper'>
              <Select
                value={countryCode}
                renderValue={value => `+${value}`}
                disabled={isSmsSend || isValidPhone}
                MenuProps={{ className: theme === 'light' ? 'scroll-bar' : 'scroll-bar-dark' }}
                onChange={handleChangeCountryCode}
                style={{ width: '85px' }}
              >
                {countryCodes.map(d => (
                  <MenuItem key={uuid.v4()} value={d.countryCode + ''}>
                    {`+${d.countryCode} (${lang === 'ko' ? d.country : d.countryEnglish})`}
                  </MenuItem>
                ))}
              </Select>

              <TextField
                variant='outlined'
                label={t('phoneNumberLabel')}
                value={phone}
                error={inputInfo.phone.error}
                helperText={typeof inputInfo.phone.helperText === 'string' ? t(inputInfo.phone.helperText) : inputInfo.phone.helperText}
                required
                disabled={isSmsSend || isValidPhone}
                onChange={handleChangePhone}
                style={{ flex: 1 }}
              />

              {!isValidPhone && (
                <Button
                  variant='contained'
                  disabled={isSmsSend || isValidPhone}
                  onClick={verifyPhone}
                  style={{ height: '56px' }}
                >
                  {t('verify')}
                </Button>
              )}

              {isValidPhone && (
                <IconButton
                  className='refresh-auth-btn'
                  onClick={() => handleClickResetButton('phone')}
                >
                  <ReplayRoundedIcon className='reset-auth-icon'/>
                </IconButton>
              )}
            </div>

            {/* 휴대전화 인증 코드 */}
            {isSmsSend && !isValidPhone && (
              <div className='input-wrapper'>
                <TextField
                  variant='outlined'
                  value={phoneAuthCode}
                  error={inputInfo.phoneAuthCode.error}
                  helperText={typeof inputInfo.phoneAuthCode.helperText === 'string' ? t(inputInfo.phoneAuthCode.helperText) : inputInfo.phoneAuthCode.helperText}
                  required
                  onChange={handleChangePhoneAuthCode}
                  label={`${t('phone')} ${t('authCode')}`}
                  style={{ flex: 1 }}
                />

                <Button
                  variant='contained'
                  onClick={checkPhoneAuthCode}
                  style={{ height: '56px' }}
                >
                  {t('confirm')}
                </Button>

                {canResetPhoneAuth && (
                  <IconButton
                    className='refresh-auth-btn'
                    onClick={() => handleClickResetButton('phone')}
                  >
                    <ReplayRoundedIcon className='reset-auth-icon'/>
                  </IconButton>
                )}

                <div style={{ paddingTop: '18px' }}>
                  {phoneAuthTimer}
                </div>
              </div>
            )}

            {/* 생일 */}
            <DatePicker
              label={t('birth')}
              inputFormat='YYYY-MM-DD'
              mask='____-__-__'
              value={new Date(birth)}
              PaperProps={{ className: theme === 'light' ? 'scroll-bar' : 'scroll-bar-dark' }}
              onChange={handleChangeBirth}
              renderInput={params => (
                <TextField
                  {...params}
                  error={inputInfo.birth.error}
                  helperText={typeof inputInfo.birth.helperText === 'string' ? t(inputInfo.birth.helperText) : inputInfo.birth.helperText}
                  required
                />
              )}
            />
          </>
        )}

        {step === 2 && (
          <>
            {/* 프로필 이미지 */}
            <div className='profile-img-wrapper'>
              <input
                ref={_profile_img_file}
                id='profile-img'
                type='file'
                accept='.jpg, .png'
                onChange={handleChangeProfileImage}
                style={{ display: 'none' }}
              />

              <Tooltip title={t('profileImage')} followCursor>
                <label htmlFor='profile-img' className='profile-img-label'>
                  <Avatar className='profile-img' src={profileImage}/>
                  <CloseRoundedIcon
                    className='reset-profile-img-icon'
                    onClick={e => {
                      e.preventDefault();
                      setProfileImage('');
                    }}
                  />
                  <Avatar className='upload-icon'>
                    <DriveFolderUploadRoundedIcon style={{ fill: '#000', marginLeft: '1px' }}/>
                  </Avatar>
                </label>
              </Tooltip>
            </div>

            {/* 닉네임 */}
            <div className='input-wrapper'>
              <TextField
                variant='outlined'
                value={nickname}
                error={inputInfo.nickname.error}
                helperText={typeof inputInfo.nickname.helperText === 'string' ? t(inputInfo.nickname.helperText) : inputInfo.nickname.helperText}
                disabled={isValidNickname}
                onChange={handleChangeNickname}
                label={t('nickname')}
                style={{ flex: 1 }}
              />

              <Button
                variant='contained'
                disabled={isValidNickname}
                onClick={() => checkValidUserIdOrNickname('NICKNAME', nickname)}
                style={{ height: '56px' }}
              >
                {t('duplicateCheck')}
              </Button>

              {isValidNickname && (
                <IconButton
                  className='refresh-auth-btn'
                  onClick={() => handleClickResetButton('nickname')}
                >
                  <ReplayRoundedIcon className='reset-auth-icon'/>
                </IconButton>
              )}
            </div>

            {/* 상태 메시지 */}
            <TextField
              variant='outlined'
              multiline
              maxRows={5}
              value={profileText}
              onChange={handleChangeProfileText}
              label={t('profileText')}
              style={{ flex: 1 }}
            />
          </>
        )}
      </div>

      {/* 하단 버튼 그룹 */}
      <div className='bottom-btn-wrapper'>
        <Button
          variant='contained'
          onClick={handleClickCancelButton}
          style={{ backgroundColor: theme === 'light' ? '#db3939' : '#ff6969' }}
        >
          {t('cancel')}
        </Button>
        {step === 1 && (
          <Button
            variant='contained'
            color='success'
            onClick={handleClickNextButton}
          >
            {t('next')}
          </Button>
        )}
        {step === 2 && (
          <>
            <Button variant='contained' color='success' onClick={() => setStep(1)}>{t('previous')}</Button>
            <Button variant='contained' onClick={handleClickCompleteButton}>{t('complete')}</Button>
          </>
        )}
      </div>

      {/* 이미지 크롭 모달 */}
      <Modal
        open={cropModalOpen}
        type='alert'
        title={t('editProfileImage')}
        center
        onConfirm={handleConfirmProfileImageCropModal}
        onClose={handleCloseProfileImageCropModal}
      >
        {originProfileImage && (
          <ReactCrop
            src={originProfileImage}
            crop={crop}
            onImageLoaded={image => setCropImage(image)}
            circularCrop
            onChange={newCrop => setCrop(newCrop)}
          />
        )}
      </Modal>

      {/* 이미지 압축 진행률 */}
      {compressing && (
        <div className='progress-container'>
          <Backdrop/>

          <div style={{ width: '400px', maxWidth: '100%' }}>
            <LineProgress
              progress={compressProgress}
              showPercentage
            />
          </div>

          <Typography
            color='#fff'
            style={{ zIndex: 10 }}
          >
            {t('compressing')}
          </Typography>
        </div>
      )}

      {/* 회원가입 완료 확인 모달 */}
      <Modal
        open={completeModalOpen}
        type='confirm'
        title={t('completeSignUp')}
        onConfirm={handleClickCompleteConfirmButton}
        onClose={() => setCompleteModalOpen(false)}
      >
        <div style={{ margin: '10px 0' }}>
          <Text>{t('completeSignUpBody')}</Text>
        </div>
      </Modal>

      {/* 에러 모달 */}
      <Modal
        open={errorModalOpen}
        type='error'
        title={t('errorOccurrence')}
        onClose={() => setErrorModalOpen(false)}
      >
        <div style={{ margin: '10px 0' }}>
          <Text>{t(errorModalMessageKey)}</Text>
        </div>
      </Modal>
    </Container>
  );
};

export default SignUp;