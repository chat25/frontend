import { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';
import { Button, Checkbox, FormControlLabel, TextField } from '@mui/material';
import { useNavigate } from 'react-router';
import _ from 'lodash';
import PageContainer from '@/components/common/PageContainer';
import useTranslation from '@/hooks/useTranslation';
import { Link } from 'react-router-dom';
import Text from '@/components/common/Text';
import Logo from '@/components/common/Logo';
import Modal from '@/components/common/Modal';
import { useReactiveVar } from '@apollo/client';
import commonState from '@/gql/vars/common';
import userState, { login, setToken, setUserInfo } from '@/gql/vars/user';
import { disconnect } from '@/gql/vars/socket';
import Contact from '@/pages/Login/Contact';
import Licenses from '@/pages/Login/Licenses';
import commonConfig from '@/config/common.json';
import autoLoginState, { checkAutoLogin } from '@/gql/vars/autoLogin';

const Container = styled(PageContainer)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
  overflow-y: auto;
  padding: 15px;

  .login-wrapper {
    position: sticky;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 280px;
    max-width: 100%;
    gap: 20px;
    margin: auto;

    .login-title {
      display: flex;
      justify-content: center;
      align-items: center;
      gap: 15px;

      .logo {
        width: 80px;
        height: 80px;
      }
    }

    .link-wrapper {
      display: flex;
      flex-direction: column;
      gap: 15px;
      padding-bottom: 10px;

      .link {
        color: #000;
        font-size: 12px;
        text-underline-position: under;
        color: ${({ theme }) => theme === 'light' ? '#000' : '#fff'};
      }
    }
  }
`;

const Login = () => {
  const [userId, setUserId] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errorModalOpen, setErrorModalOpen] = useState<boolean>(false);
  const [errorModalMessageKey, setErrorModalMessageKey] = useState<string>('');
  const [autoLogin, setAutoLogin] = useState<boolean>(false);
  const [inputInfo, setInputInfo] = useState({
    userId: { error: false, helperText: '' },
    password: { error: false, helperText: '' }
  });

  const { theme } = useReactiveVar(commonState.getVar());
  const { userInfo, token } = useReactiveVar(userState.getVar());
  const { refreshToken: autoLoginRefreshToken } = useReactiveVar(autoLoginState.getVar());

  const t = useTranslation();
  const navigate = useNavigate();

  /** 아이디 변경 이벤트 */
  const handleChangeUserId: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement> = useCallback(e => {
    if (inputInfo.userId.error) {
      const inputInfoClone = _.cloneDeep(inputInfo);
      inputInfoClone.userId = { error: false, helperText: '' };
      setInputInfo(inputInfoClone);
    }

    setUserId(e.target.value);
  }, [inputInfo]);

  /** 비밀번호 변경 이벤트 */
  const handleChangePassword: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement> = useCallback(e => {
    if (inputInfo.password.error) {
      const inputInfoClone = _.cloneDeep(inputInfo);
      inputInfoClone.password = { error: false, helperText: '' };
      setInputInfo(inputInfoClone);
    }

    setPassword(e.target.value);
  }, [inputInfo]);

  /** 로그인 버튼 클릭 이벤트 */
  const handleClickLoginButton: React.MouseEventHandler<HTMLButtonElement> = useCallback(e => {
    e.preventDefault();

    const inputInfoClone = _.cloneDeep(inputInfo);
    let canLogin = true;

    if (!userId) {
      inputInfoClone.userId = { error: true, helperText: t('pleaseEnterUserId') };
      canLogin = false;
    }

    if (!password) {
      inputInfoClone.password = { error: true, helperText: t('pleaseEnterPassword') };
      canLogin = false;
    }

    if (!canLogin) {
      setInputInfo(inputInfoClone);
      return;
    }

    login({ userId, password, autoLogin, setErrorModalOpen, setErrorModalMessageKey });
  }, [userId, password, autoLogin, inputInfo, t]);

  /** 로그인 성공 시 메인 페이지로 이동 */
  useEffect(() => {
    console.log('useEffect', { userInfo, token });
    if (userInfo && token) navigate('/', { replace: true });
  }, [userInfo, token, navigate]);

  /** 페이지 초기 렌더링 시 userInfo, token 삭제 */
  useEffect(() => {
    setUserInfo();
    setToken();
    disconnect();
    checkAutoLogin();
  }, []);

  if (!!autoLoginRefreshToken) return null;

  return (
    <>
      <Container theme={theme}>
        <form className='login-wrapper'>
          {/* 로그인 타이틀 */}
          <div className='login-title'>
            <Logo className='logo'/>
            <Text variant='h3' fontWeight='bold'>Chat</Text>
          </div>

          {/* 아이디 */}
          <TextField
            label={t('userId')}
            variant='standard'
            value={userId}
            error={inputInfo.userId.error}
            helperText={inputInfo.userId.helperText}
            onChange={handleChangeUserId}
          />

          {/* 비밀번호 */}
          <TextField
            label={t('password')}
            variant='standard'
            type='password'
            value={password}
            error={inputInfo.password.error}
            helperText={inputInfo.password.helperText}
            onChange={handleChangePassword}
          />

          {/* 로그인 상태 유지 */}
          <FormControlLabel
            control={(
              <Checkbox
                size='small'
                checked={autoLogin}
                onChange={e => setAutoLogin(e.target.checked)}
              />
            )}
            label={<Text variant='body2'>{t('maintainLoginStatus')}</Text>}
            style={{ alignItems: 'center', gap: '5px' }}
          />

          {/* 계정 찾기, 회원가입 링크 */}
          <div className='link-wrapper'>
            <Link to='/find-account' className='link'>
              <span>{t('forgotIdOrPassword')}</span>
            </Link>

            <Link to='/sign-up' className='link'>
              <span>{t('createAccout')}</span>
            </Link>
          </div>

          {/* 로그인 버튼 */}
          <Button
            variant='outlined'
            type='submit'
            onClick={handleClickLoginButton}
          >
            {t('login')}
          </Button>
        </form>

        {/* 에러 모달 */}
        <Modal
          open={errorModalOpen}
          type='error'
          title={t('errorOccurrence')}
          onClose={() => setErrorModalOpen(false)}
        >
          <div style={{ margin: '10px 0' }}>
            <Text>{t(`${errorModalMessageKey}`)}</Text>
          </div>
        </Modal>
      </Container>

      <PageContainer className='login-footer'>
        {/* 버전 정보 */}
        <div style={{ marginLeft: '10px' }}>
          <Text variant='caption'>v{commonConfig.version}</Text>
        </div>

        {/* 연락처, 라이선스 */}
        <div style={{ display: 'flex', alignItems: 'center', gap: '15px' }}>
          <Contact/>
          <Licenses/>
        </div>
      </PageContainer>
    </>
  );
};

export default Login;
