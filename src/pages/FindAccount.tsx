import { useCallback, useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import {
  Button,
  FormControlLabel,
  IconButton,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  SelectChangeEvent,
  TextField
} from '@mui/material';
import { useNavigate } from 'react-router';
import * as uuid from 'uuid';
import _ from 'lodash';
import PageContainer from '@/components/common/PageContainer';
import Text from '@/components/common/Text';
import useTranslation from '@/hooks/useTranslation';
import Timer from '@/components/common/Timer';
import countryCodes from '@/config/countryCodes.json';
import validate from '@/utils/validate';
import Logo from '@/components/common/Logo';
import {
  CHECK_DUPLICATED_NAME_AND_EMAIL,
  CHECK_DUPLICATED_NAME_AND_PHONE,
  CHECK_EMAIL_AUTHCODE,
  CHECK_PHONE_AUTHCODE,
  EMAIL_VERIFICATION,
  PHONE_VERIFICATION,
  RESET_PASSWORD
} from '@/gql/user';
import ReplayRoundedIcon from '@mui/icons-material/ReplayRounded';
import Modal from '@/components/common/Modal';
import { useReactiveVar } from '@apollo/client';
import commonState from '@/gql/vars/common';
import useApollo from '@/hooks/useApollo';

const Container = styled(PageContainer)`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  flex: 1;
  gap: 30px;
  padding: 15px;
  overflow-y: auto;

  .find-account-title {
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 15px;

    .logo {
      width: 80px;
      height: 80px;
    }
  }

  .find-account-wrapper {
    display: flex;
    flex-direction: column;
    width: 360px;
    max-width: 100%;
    gap: 15px;
    
    .radio-label {
      color: ${({ theme }) => theme === 'light' ? '#000' : '#fff'}
    }

    .phone-input-wrapper {
      display: flex;
      justify-content: space-between;
      align-items: flex-start;
      gap: 10px;
    }

    .authcode-input-wrapper {
      display: flex;
      justify-content: space-between;
      align-items: flex-start;
      gap: 10px;

      .refresh-auth-btn {
        margin-top: 8px;

        .reset-auth-icon {
          fill: ${({ theme }) => theme === 'light' ? '#000' : '#fff'};
        }
      }
    }
  }
`;

const FindAccount = () => {
  /**
   * Step 1. 인증코드 발송 전
   * Step 2. 인증코드 발송 후
   * Step 3. 인증코드 확인 후
   */
  const [step, setStep] = useState<number>(1);
  const [name, setName] = useState<string>('');
  const [authMethod, setAuthMethod] = useState<'EMAIL' | 'PHONE'>('EMAIL');
  const [email, setEmail] = useState<string>('');
  const [countryCode, setCountryCode] = useState<string>('82');
  const [phone, setPhone] = useState<string>('');
  const [authCodeUuid, setAuthCodeUuid] = useState<string>('');
  const [authCode, setAuthCode] = useState<string>('');
  const [canResetAuthCode, setCanResetAuthCode] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [passwordConfirm, setPasswordConfirm] = useState<string>('');
  const [confirmModalOpen, setConfirmModalOpen] = useState<boolean>(false);
  const [errorModalOpen, setErrorModalOpen] = useState<boolean>(false);
  const [inputInfo, setInputInfo] = useState({
    name: { error: false, helperText: '' },
    email: { error: false, helperText: '' },
    phone: { error: false, helperText: '' },
    authCode: { error: false, helperText: '' },
    password: { error: false, helperText: '' },
    passwordConfirm: { error: false, helperText: '' }
  });

  const { theme, lang } = useReactiveVar(commonState.getVar());

  const t = useTranslation();
  const navigate = useNavigate();
  const apollo = useApollo();

  const handleChangeName = useCallback((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.name = { error: false, helperText: '' };

    setName(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 본인인증 수단 선택 이벤트 */
  const handleChangeAuthMethod = useCallback((e: any, value: any) => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.email = { error: false, helperText: '' };
    inputInfoClone.phone = { error: false, helperText: '' };

    setAuthMethod(value);
    setEmail('');
    setCountryCode('82');
    setPhone('');
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 이메일 변경 이벤트 */
  const handleChangeEmail = useCallback((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.email = { error: false, helperText: '' };

    setEmail(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 휴대전화번호 변경 이벤트 */
  const handleChangePhone = useCallback((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const { value } = e.target;
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.phone = { error: false, helperText: '' };

    const isNumberString = validate('numberstring', value);
    if (value === '' || isNumberString) {
      setPhone(value);
      setInputInfo(inputInfoClone);
    }
  }, [inputInfo]);

  /** 국가 코드 변경 이벤트 */
  const handleChangeCountryCode = useCallback((e: SelectChangeEvent<string>) => {
    setCountryCode(e.target.value);
  }, []);

  /** 인증코드 발송 버튼 클릭 이벤트 (이메일 인증) */
  const handleClickSendEmailAuthCodeButton = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    let canMoveNextStep = true;

    if (name) {
      inputInfoClone.name = { error: false, helperText: '' };
    } else {
      inputInfoClone.name = { error: true, helperText: 'pleaseEnterName' };
      canMoveNextStep = false;
    }

    const isCorrectEmail = validate('email', email);

    if (isCorrectEmail) {
      inputInfoClone.email = { error: false, helperText: '' };
    } else {
      inputInfoClone.email = { error: true, helperText: 'pleaseEnterCorrectEmail' };
      canMoveNextStep = false;
    }

    if (!canMoveNextStep) return setInputInfo(inputInfoClone);

    try {
      const data1 = await apollo.query<{ checkDuplicatedNameAndEmail: CheckDuplicatedNameAndEmailResult }, CheckDuplicatedNameAndEmailArgs>({
        query: CHECK_DUPLICATED_NAME_AND_EMAIL,
        variables: { name, email }
      });

      if (data1.checkDuplicatedNameAndEmail.errCode) throw data1.checkDuplicatedNameAndEmail.errCode;

      const { result: isExist } = data1.checkDuplicatedNameAndEmail;

      if (!isExist) {
        inputInfoClone.email = { error: true, helperText: 'notExistUserInfo' };
        return setInputInfo(inputInfoClone);
      }

      const data2 = await apollo.query<{ emailVerification: EmailVerificationResult }, EmailVerificationArgs>({
        query: EMAIL_VERIFICATION,
        variables: { email, lang, checkDuplicate: false }
      });

      if (data2.emailVerification.errCode) {
        if (data2.emailVerification.errCode !== 'DUPLICATE_ERROR') {
          throw data2.emailVerification.errCode;
        }
      }

      const { emailVerification: { result } } = data2;
      inputInfoClone.email = { error: false, helperText: '' };
      inputInfoClone.authCode = { error: false, helperText: '' };

      setAuthCodeUuid(result);
      setStep(2);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [lang, name, email, inputInfo, apollo]);

  /** 인증코드 발송 버튼 클릭 이벤트 (휴대전화 인증) */
  const handleClickSendPhoneAuthCodeButton = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    let canMoveNextStep = true;

    if (name) {
      inputInfoClone.name = { error: false, helperText: '' };
    } else {
      inputInfoClone.name = { error: true, helperText: 'pleaseEnterName' };
      canMoveNextStep = false;
    }

    const isCorrectPhone = validate('numberstring', phone);

    if (isCorrectPhone) {
      inputInfoClone.phone = { error: false, helperText: '' };
    } else {
      inputInfoClone.phone = { error: true, helperText: 'pleaseEnterPhone' };
      canMoveNextStep = false;
    }

    if (!canMoveNextStep) return setInputInfo(inputInfoClone);

    try {
      const data1 = await apollo.query<{ checkDuplicatedNameAndPhone: CheckDuplicatedNameAndPhoneResult }, CheckDuplicatedNameAndPhoneArgs>({
        query: CHECK_DUPLICATED_NAME_AND_PHONE,
        variables: { name, countryCode, phone }
      });

      if (data1.checkDuplicatedNameAndPhone.errCode) throw data1.checkDuplicatedNameAndPhone.errCode;

      const { result: isExist } = data1.checkDuplicatedNameAndPhone;

      if (!isExist) {
        inputInfoClone.phone = { error: true, helperText: 'notExistUserInfo' };
        return setInputInfo(inputInfoClone);
      }

      const data2 = await apollo.query<{ phoneVerification: PhoneVerificationResult }, PhoneVerificationArgs>({
        query: PHONE_VERIFICATION,
        variables: { countryCode, phone, lang, checkDuplicate: false }
      });

      if (data2.phoneVerification.errCode) {
        if (data2.phoneVerification.errCode !== 'DUPLICATE_ERROR') {
          throw data2.phoneVerification.errCode;
        }
      }

      const { phoneVerification: { result } } = data2;
      inputInfoClone.phone = { error: false, helperText: '' };
      inputInfoClone.authCode = { error: false, helperText: '' };

      setAuthCodeUuid(result);
      setStep(2);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [lang, name, countryCode, phone, inputInfo, apollo]);

  /** 인증코드 변경 이벤트 */
  const handleChangeAuthCode = useCallback((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const { value } = e.target;
    const isNumberString = validate('numberstring', value);

    if ((value !== '' && !isNumberString) || value.length > 6) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.authCode = { error: false, helperText: '' };

    setAuthCode(value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 인증코드 초기화 버튼 클릭 이벤트 */
  const handleClickResetAuthButton = useCallback(() => {
    setAuthCodeUuid('');
    setAuthCode('');
    setStep(1);
  }, []);

  /** 인증코드 확인 버튼 클릭 이벤트 */
  const handleClickVerifyAuthCode = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    const isCorrectAuthCode = validate('numberstring', authCode);

    try {
      if (isCorrectAuthCode) {
        if (authMethod === 'EMAIL') {
          const data = await apollo.mutate<{ checkEmailAuthCode: CheckEmailAuthCodeResult }, CheckEmailAuthCodeArgs>({
            mutation: CHECK_EMAIL_AUTHCODE,
            variables: {
              email,
              authCode,
              uuid: authCodeUuid
            }
          });

          if (data.checkEmailAuthCode.errCode) {
            if (data.checkEmailAuthCode.errCode === 'INVALID_AUTH_CODE') {
              inputInfoClone.authCode = { error: true, helperText: 'wrongAuthCode' };
              setInputInfo(inputInfoClone);
            }
            throw data.checkEmailAuthCode.errCode;
          }

          const { userId } = data.checkEmailAuthCode.result;
          setUserId(userId || '');
        } else {
          const data = await apollo.mutate<{ checkPhoneAuthCode: CheckPhoneAuthCodeResult }, CheckPhoneAuthCodeArgs>({
            mutation: CHECK_PHONE_AUTHCODE,
            variables: {
              countryCode,
              phone,
              authCode,
              uuid: authCodeUuid
            }
          });

          if (data.checkPhoneAuthCode.errCode) {
            if (data.checkPhoneAuthCode.errCode === 'INVALID_AUTH_CODE') {
              inputInfoClone.authCode = { error: true, helperText: 'wrongAuthCode' };
              setInputInfo(inputInfoClone);
            }
            throw data.checkPhoneAuthCode.errCode;
          }

          const { userId } = data.checkPhoneAuthCode.result;
          setUserId(userId || '');
        }
  
        inputInfoClone.authCode = { error: false, helperText: '' };
        setStep(step + 1);
      } else {
        inputInfoClone.authCode = { error: true, helperText: 'pleaseEnterAuthCode' };
      }

      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [authMethod, email, countryCode, phone, authCode, authCodeUuid, step, inputInfo, apollo]);

  /** 비밀번호 변경 이벤트 */
  const handleChangePassword = useCallback((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.password = { error: false, helperText: '' };

    setPassword(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 비밀번호 확인 변경 이벤트 */
  const handleChangePasswordConfirm = useCallback((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.passwordConfirm = { error: false, helperText: '' };

    setPasswordConfirm(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 확인 버튼 클릭 이벤트 */
  const handleClickConfirmButton = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    let canResetPassword = true;

    if (password === '' || !validate('password', password)) {
      inputInfoClone.password = { error: true, helperText: 'pleaseEnterCorrectPassword' };
      canResetPassword = false;
    }

    if (passwordConfirm === '') {
      inputInfoClone.passwordConfirm = { error: true, helperText: 'pleaseConfirmPassword' };
      canResetPassword = false;
    }

    if (password !== '' && passwordConfirm !== '' && password !== passwordConfirm) {
      inputInfoClone.password = { error: false, helperText: '' };
      inputInfoClone.passwordConfirm = { error: true, helperText: 'pleaseEnterSamePassword' };
      canResetPassword = false;
    }
    
    setInputInfo(inputInfoClone);
    if (canResetPassword) setConfirmModalOpen(true);
  }, [password, passwordConfirm, inputInfo]);

  /** 비밀번호 초기화 모달 확인 버튼 클릭 이벤트 */
  const handleClickResetPasswordConfirmButton = useCallback(async () => {
    try {
      const data = await apollo.mutate<{ resetPassword: ResetPasswordResult }, ResetPasswordArgs>({
        mutation: RESET_PASSWORD,
        variables: {
          email: authMethod === 'EMAIL' ? email : undefined,
          countryCode: authMethod === 'PHONE' ? countryCode : undefined,
          phone: authMethod === 'PHONE' ? phone : undefined,
          password,
          uuid: authCodeUuid
        }
      });

      if (data.resetPassword.errCode) {
        setErrorModalOpen(true);
        throw data.resetPassword.errCode
      }

      if (data.resetPassword.result) navigate('/login');
    } catch (err) {
      console.error(err);
    }
  }, [authMethod, email, countryCode, phone, password, authCodeUuid, apollo, navigate]);

  /** 취소 버튼 클릭 이벤트 */
  const handleClickCancelButton = useCallback(() => {
    navigate('/login');
  }, [navigate]);

  /** 인증코드 타이머 */
  const authCodeTimer = useMemo(() => (
    <Timer
      seconds={300}
      onEnd={handleClickResetAuthButton}
      style={{ display: 'block', width: '41px' }}
    />
  ), [handleClickResetAuthButton]);

  /** 인증코드 발송 1분 후 초기화 버튼 표시 */
  useEffect(() => {
    let timeout: any;
    if (step === 2) timeout = setTimeout(() => {
      setCanResetAuthCode(true);
    }, 61000);

    return () => {
      clearTimeout(timeout);
    };
  }, [step]);

  return (
    <Container theme={theme}>
      <div className='find-account-title'>
        <Logo className='logo'/>
        <Text variant='h4' fontWeight='bold'>{t('findAccount')}</Text>
      </div>

      {/* 1. 본인인증 수단 */}
      <div className='find-account-wrapper'>
        <Text variant='h6' fontWeight='bold'>1. {t('authenticationMethod')}</Text>

        <TextField
          label={t('name')}
          value={name}
          disabled={step !== 1}
          error={inputInfo.name.error}
          helperText={t(inputInfo.name.helperText)}
          onChange={handleChangeName}
        />

        <RadioGroup value={authMethod} onChange={handleChangeAuthMethod}>
          <FormControlLabel
            className='radio-label'
            value='EMAIL'
            control={<Radio/>}
            label={t('emailAuthentication')}
            disabled={step !== 1}
          />

          {authMethod === 'EMAIL' && (
            <TextField
              label={t('email')}
              value={email}
              disabled={step !== 1}
              error={inputInfo.email.error}
              helperText={t(inputInfo.email.helperText)}
              onChange={handleChangeEmail}
              style={{ marginBottom: '15px' }}
            />
          )}

          <FormControlLabel
            className='radio-label'
            value='PHONE'
            control={<Radio/>}
            label={t('phoneAuthentication')}
            disabled={step !== 1}
          />

          {authMethod === 'PHONE' && (
            <div className='phone-input-wrapper'>
              <Select
                value={countryCode}
                renderValue={value => `+${value}`}
                disabled={step !== 1}
                onChange={handleChangeCountryCode}
                style={{ width: '85px' }}
              >
                {countryCodes.map(d => (
                  <MenuItem key={uuid.v4()} value={d.countryCode + ''}>
                    {`+${d.countryCode} (${lang === 'ko' ? d.country : d.countryEnglish})`}
                  </MenuItem>
                ))}
              </Select>

              <TextField
                label={t('phoneNumberLabel')}
                value={phone}
                error={inputInfo.phone.error}
                helperText={t(inputInfo.phone.helperText)}
                onChange={handleChangePhone}
                style={{ flex: 1 }}
                disabled={step !== 1}
              />
            </div>
          )}
        </RadioGroup>

        <Button
          variant='contained'
          onClick={authMethod === 'EMAIL' ? handleClickSendEmailAuthCodeButton : handleClickSendPhoneAuthCodeButton}
          disabled={step !== 1}
        >
          {t('sendAuthCode')}
        </Button>
      </div>

      {/* 2. 인증코드 입력 */}
      {step > 1 && (
        <div className='find-account-wrapper'>
          <Text variant='h6' fontWeight='bold'>2. {t('enterAuthCode')}</Text>

          <div className='authcode-input-wrapper'>
            <TextField
              label={t('authCode')}
              value={authCode}
              error={inputInfo.authCode.error}
              helperText={inputInfo.authCode.helperText ? t(inputInfo.authCode.helperText) : t('waitOneMinuteForResendAuthCode')}
              onChange={handleChangeAuthCode}
              disabled={step !== 2}
              style={{ flex: 1 }}
            />
            {step === 2 && (
              <>
                {canResetAuthCode && (
                  <IconButton
                    className='refresh-auth-btn'
                    onClick={handleClickResetAuthButton}
                  >
                    <ReplayRoundedIcon className='reset-auth-icon'/>
                  </IconButton>
                )}

                <div style={{ paddingTop: '18px' }}>
                  {authCodeTimer}
                </div>
              </>
            )}
          </div>

          <Button
            variant='contained'
            onClick={handleClickVerifyAuthCode}
            disabled={step !== 2}
          >
            {t('verifyAuthCode')}
          </Button>
        </div>
      )}

      {/* 3. 비밀번호 초기화 */}
      {step > 2 && (
        <div className='find-account-wrapper'>
          <Text variant='h6' fontWeight='bold'>3. {t('resetPassword')}</Text>

          <TextField
            label={t('userId')}
            value={userId}
            color='success'
            focused
          />

          <TextField
            label={t('newPassword')}
            value={password}
            error={inputInfo.password.error}
            helperText={t(inputInfo.password.helperText)}
            type='password'
            onChange={handleChangePassword}
          />

          <TextField
            label={t('newPasswordConfirm')}
            value={passwordConfirm}
            error={inputInfo.passwordConfirm.error}
            helperText={t(inputInfo.passwordConfirm.helperText)}
            type='password'
            onChange={handleChangePasswordConfirm}
          />

          <div style={{ display: 'flex', justifyContent: 'flex-end', gap: '10px' }}>
            <Button
              variant='contained'
              onClick={handleClickCancelButton}
              style={{ backgroundColor: theme === 'light' ? '#db3939' : '#ff6969' }}
            >
              {t('cancel')}
            </Button>

            <Button
              variant='contained'
              onClick={handleClickConfirmButton}
            >
              {t('confirm')}
            </Button>
          </div>
        </div>
      )}

      {/* 비밀번호 초기화 확인 모달 */}
      <Modal
        open={confirmModalOpen}
        type='confirm'
        title={t('resetPassword')}
        onConfirm={handleClickResetPasswordConfirmButton}
        onClose={() => setConfirmModalOpen(false)}
      >
        <div style={{ margin: '10px 0' }}>
          <Text>{t('doYouWantToInitializePassword')}</Text>
        </div>
      </Modal>

      <Modal
        open={errorModalOpen}
        type='error'
        title={t('errorOccurrence')}
        onClose={() => setErrorModalOpen(false)}
      >
        {t('serverErrorOccurrence')}
      </Modal>
    </Container>
  );
};

export default FindAccount;