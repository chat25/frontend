import React, { useCallback, useEffect, useLayoutEffect, useMemo, useRef, useState } from 'react';
import { Button, Menu, MenuItem, TextField } from '@mui/material';
import styled, { keyframes } from 'styled-components';
import moment from 'moment';
import _ from 'lodash';
import AddIcon from '@mui/icons-material/Add';
import SendIcon from '@mui/icons-material/Send';
import ArrowDownwardRoundedIcon from '@mui/icons-material/ArrowDownwardRounded';
import ImageRoundedIcon from '@mui/icons-material/ImageRounded';
import OndemandVideoRoundedIcon from '@mui/icons-material/OndemandVideoRounded';
import AudiotrackRoundedIcon from '@mui/icons-material/AudiotrackRounded';
import AttachFileRoundedIcon from '@mui/icons-material/AttachFileRounded';
import UploadIcon from '@mui/icons-material/Upload';
import Message from '@/components/common/Message';
import useTranslation from '@/hooks/useTranslation';
import Text from '@/components/common/Text';
import { GET_MESSAGES, SEND_UPLOAD_CHAT } from '@/gql/room';
import { useReactiveVar } from '@apollo/client';
import commonState from '@/gql/vars/common';
import userState from '@/gql/vars/user';
import socketVar from '@/gql/vars/socket';
import mainPageVar, { setRoomInfo, setUploading, setUploadSize } from '@/gql/vars/mainPage';
import { setNoReadNum } from '@/gql/vars/drawer';
import Modal from '@/components/common/Modal';
import { LineProgress } from '@/components/common/Progress';
import Backdrop from '@/components/common/Backdrop';
import useApollo from '@/hooks/useApollo';

/** 파일 업로드 확장자 */
const fileExtensions = {
  image: ['.jpg', '.png', '.gif'],
  video: ['.mpg', '.mpeg', '.avi', '.wmv', '.mov', '.rm', '.ram', '.swf', '.flv', '.ogg', '.webm', '.mp4'],
  audio: ['.mid', '.midi', '.rm', '.ram', '.wma', '.aac', '.wav', '.ogg', '.mp3', '.m4a']
};

const uploadIconAnimation = keyframes`
  0% { transform: translate(0, 0); }
  50% { transform: translate(0, -10px); }
  100% { transform: translate(0, 0); }
`;

const Container = styled.div`
  position: relative;
  flex: 1;
  display: flex;
  flex-direction: column;
  width: 100%;
  overflow: hidden;

  .chat-contents {
    flex: 1;
    overflow-y: auto;
  }

  .msg-input-wrapper {
    position: relative;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px;
    background-color: ${({ theme }) => theme === 'light' ? '#fff' : '#1c1c1c'};
    border-top: 1px solid ${({ theme }) => theme === 'light' ? '#bbb' : '#272727'};
    gap: 10px;

    .add-icon-btn {
      width: 40px;
      height: 40px;
      min-width: 0;
      border-radius: 20px;
    }

    .msg-input {
      flex: 1;
    }

    .send-msg-btn {
      width: 50px;
      height: 40px;
      min-width: 0;
      border-radius: 20px;
    }
  }

  .last-msg-preview {
    position: absolute;
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding: 15px;
    gap: 15px;

    .last-msg {
      flex: 1;
      background-color: rgba(0, 0, 0, .7);
      padding: 10px;
      border-radius: 5px;
      overflow: hidden;

      .last-msg-text {
        color: #fff !important;
        overflow: hidden;
        text-overflow: ellipsis;
        word-break: keep-all;
      }
    }

    .down-arrow-btn {
      width: 40px;
      height: 40px;
      min-width: 0;
      border-radius: 20px;
      opacity: .8;
    }
  }

  .file-drag-bg {
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, .7);

    .file-drag-bg-wrapper {
      display: flex;
      flex-direction: column;
      align-items: center;
      pointer-events: none;
      user-select: none;

      .upload-icon {
        width: 100px;
        height: 100px;
        fill: #ddd;
        animation-name: ${uploadIconAnimation};
        animation-duration: 1s;
        animation-iteration-count: infinite;
        animation-timing-function: ease-in-out;
        pointer-events: none;
        user-select: none;
      }
    }
  }

  .progress-container {
    position: fixed;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    gap: 20px;
    z-index: 11;
  }
`;

const StyledMenuItem = styled(MenuItem)`
  gap: 30px;
`;

let scrollTimeout: any = null;

const ChatRoom = () => {
  const [isInit, setIsInit] = useState<boolean>(true);
  const [message, setMessage] = useState<string>('');
  const [isChatScrollBottom, setIsChatScrollBottom] = useState<boolean>(true);
  const [prevScrollHeight, setPrevScrollHeight] = useState<number | null>(null);
  const [msgInputWrapperHeight, setMsgInputWrapperHeight] = useState<number>(61);
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [uploadType, setUploadType] = useState<UploadType | null>(null);
  const [showUploadBg, setShowUploadBg] = useState<boolean>(false);
  const [uploadConfirmModalOpen, setUploadConfirmModalOpen] = useState<boolean>(false);
  const [uploadErrorModalOpen, setUploadErrorModalOpen] = useState<boolean>(false);
  const [uploadErrorMessage, setUploadErrorMessage] = useState<string>('');
  const [uploadTotalSize, setUploadTotalSize] = useState<number>(0);

  const { theme } = useReactiveVar(commonState.getVar());
  const { userInfo } = useReactiveVar(userState.getVar());
  const { socket } = useReactiveVar(socketVar);
  const { roomInfo, uploading, uploadSize } = useReactiveVar(mainPageVar);

  const _chat_contents = useRef<HTMLDivElement>(null);
  const _msg_input_wrapper = useRef<HTMLDivElement>(null);
  const _file_input_ref = useRef<HTMLInputElement>(null);

  const t = useTranslation();
  const apollo = useApollo();

  /** 메시지 Input 변경 이벤트 */
  const handleChangeMessage: React.ChangeEventHandler<HTMLTextAreaElement> = useCallback(e => {
    setMessage(e.target.value);
  }, []);

  /** 메시지 전송 버튼 클릭 이벤트 */
  const handleClickSendMessageButton = useCallback(() => {
    if (!socket || !roomInfo || message === '' || !_chat_contents.current) return;
    const { clientHeight, scrollHeight } = _chat_contents.current;

    socket.emit('text-msg', roomInfo.uuid, message);

    setMessage('');
    setIsChatScrollBottom(true);
    setPrevScrollHeight(null);
    _chat_contents.current.removeAttribute('style');
    _chat_contents.current.scrollTop = scrollHeight - clientHeight;
  }, [socket, roomInfo, message]);

  /** 메시지 Input Key Down 이벤트 */
  const handleKeyDownMessageInput: React.KeyboardEventHandler<HTMLDivElement> = useCallback(e => {
    if (e.shiftKey && e.keyCode === 13) return setMessage(message);
    if (e.keyCode === 13) {
      e.preventDefault();
      handleClickSendMessageButton();
    }
  }, [message, handleClickSendMessageButton]);

  /** 이전 채팅 메시지 조회 */
  const getPrevMessages = useCallback(async () => {
    if (!userInfo || !roomInfo || roomInfo.messages.length < 100 || !_chat_contents.current) return;
    const { scrollHeight } = _chat_contents.current;

    try {
      const data = await apollo.query<{ getMessages: GetMessagesResult }, GetMessagesArgs>({
        query: GET_MESSAGES,
        variables: { roomUuid: roomInfo.uuid, lastChatDate: roomInfo.messages[0].sendDate }
      });

      const { result, errCode } = data.getMessages;

      if (errCode) throw errCode;

      if (result.length === 0) return;

      setPrevScrollHeight(scrollHeight);
      _chat_contents.current.style.scrollBehavior = 'auto';

      setRoomInfo({
        ...roomInfo,
        messages: [
          ...result,
          ...roomInfo.messages
        ]
      });
    } catch (err) {
      console.error(err);
    }
  }, [userInfo, roomInfo, apollo]);

  /** 채팅 내용 스크롤 이벤트 */
  const handleScrollChatContents = useCallback((e: any) => {
    const { scrollTop, clientHeight, scrollHeight } = e.target;
    clearTimeout(scrollTimeout);

    if (scrollTop === 0) {
      getPrevMessages();
      setIsChatScrollBottom(false);
      return;
    }

    setPrevScrollHeight(null);
    e.target.removeAttribute('style');

    scrollTimeout = setTimeout(() => {
      if (scrollTop + clientHeight + 150 >= scrollHeight) return setIsChatScrollBottom(true);
      setIsChatScrollBottom(false);
    }, 100);
  }, [getPrevMessages]);

  /** 마지막 메시지 미리보기 스크롤 최하단 이동 버튼 클릭 이벤트 */
  const handleClickDownArrowButton = useCallback(() => {
    const chatContentsDiv = _chat_contents.current;
    if (!chatContentsDiv) return;

    chatContentsDiv.scrollTop = chatContentsDiv.scrollHeight - chatContentsDiv.clientHeight;
    setIsChatScrollBottom(true);
  }, []);

  /** 사진, 비디오, 오디오, 파일 전송 메뉴 버튼 클릭 이벤트 */
  const handleClickUploadMenuButton = useCallback((newUploadType: UploadType) => {
    if (!_file_input_ref.current) return;
    setUploadType(newUploadType);
    setAnchorEl(null);
    _file_input_ref.current.value = '';
    if (uploadType === newUploadType) _file_input_ref.current.click();
  }, [uploadType]);

  /** 업로드 확인 모달 확인 버튼 클릭 이벤트 */
  const handleClickUploadModalConfirmButton = useCallback(async () => {
    if (!_file_input_ref.current || !roomInfo) return;
    const { files } = _file_input_ref.current;
    if (!files || files.length > 10) return;

    let totalSize = 0;
    for (const file of Array.from(files)) {
      totalSize += file.size;
      if (file.size > 300 * 1024 * 1024) {
        setUploadConfirmModalOpen(false);
        setUploadErrorMessage('uploadMaxFileSizeError');
        setUploadErrorModalOpen(true);
        return;
      }
    }
    setUploadTotalSize(totalSize);
    setUploadSize(0);
    setUploadConfirmModalOpen(false);
    setUploading(true);

    const variables = {
      roomUuid: roomInfo.uuid,
      type: uploadType || 'file',
      files,
      compress: (['image', 'video', 'audio'] as any).indexOf(uploadType) < 0
    };
    console.log(variables);

    try {
      const data = await apollo.mutate<{ sendUploadChat: SendUploadChatResult }, SendUploadChatArgs>({
        mutation: SEND_UPLOAD_CHAT,
        variables
      });

      const { result, errCode } = data.sendUploadChat;

      if (errCode) {
        if (errCode === 'INVALID_FILE_TYPE') {
          setUploadConfirmModalOpen(false);
          setUploadErrorMessage(`uploadTypeError_${uploadType}`);
          setUploadErrorModalOpen(true);
        }
        throw errCode;
      }

      _file_input_ref.current.value = '';
      console.log(result);
    } catch (err: any) {
      console.error(err.toString ? err.toString() : err + '');
      setUploading(false);
    }
  }, [roomInfo, apollo, uploadType]);

  /** 스크롤 최하단 이동 */
  const setScrollBottom = useCallback(() => {
    const chatContentsDiv = _chat_contents.current;
    if (!chatContentsDiv) return;
    const { clientHeight, scrollHeight } = chatContentsDiv;
    chatContentsDiv.scrollTop = scrollHeight - clientHeight;
  }, []);

  /** 채팅 내용 */
  const chatMessages = useMemo(() => {
    if (!userInfo || !roomInfo) return null;

    const result = [];

    if (roomInfo.messages.length > 0) result.push(
      <Message.Center
        key={0}
        contents={moment(roomInfo.messages[0].sendDate).format('YYYY-MM-DD')}
      />
    );

    for (let i = 0; i < roomInfo.messages.length; i++) {
      const prevMessage = roomInfo.messages[i - 1];
      const message = roomInfo.messages[i];
      const nextMessage = roomInfo.messages[i + 1];

      if (['join', 'leave'].indexOf(message.type) > -1) {
        result.push(
          <Message.Center
            key={message.uuid}
            contents={`${(message.type === 'join' ? t('someoneEntered') : t('someoneLeft')).replace('{{nickname}}', message.contents || '(Unknown)')}`}
            color={message.type === 'join' ?
              '#0f0' :
              (theme === 'light' ? '#ff5353' : '#f00')
            }
          />
        );
      } else {
        const messageSendDate = moment(message.sendDate).format('YYYYMMDDHHmm');
        const nextMessageSendDate = nextMessage ? moment(nextMessage.sendDate).format('YYYYMMDDHHmm') : undefined;

        if (message.userUuid === userInfo.uuid) {
          result.push(
            <Message.MyChat
              key={message.uuid}
              uuid={message.uuid}
              type={message.type}
              contents={message.contents || undefined}
              filePath={message.filePath}
              showTime={!nextMessage || ['join', 'leave'].indexOf(nextMessage.type) > -1 || nextMessage.userUuid !== message.userUuid || nextMessageSendDate !== messageSendDate}
              sendDate={message.sendDate}
              setScrollbottom={setScrollBottom}
            />
          );
        } else {
          const otherUserInfo = _.find(roomInfo.users.concat(roomInfo.leaveUsers), { uuid: message.userUuid });
          const prevMessageSendDate = prevMessage ? moment(prevMessage.sendDate).format('YYYYMMDDHHmm') : undefined;

          result.push(
            <Message.OtherChat
              key={message.uuid}
              uuid={message.uuid}
              type={message.type}
              profileImage={otherUserInfo?.profileImage}
              nickname={otherUserInfo?.nickname || '(Unknown)'}
              contents={message.contents || undefined}
              filePath={message.filePath}
              showProfile={!prevMessage || ['join', 'leave'].indexOf(prevMessage.type) > -1 || prevMessage.userUuid !== message.userUuid || prevMessageSendDate !== messageSendDate}
              showTime={!nextMessage || ['join', 'leave'].indexOf(nextMessage.type) > -1 || nextMessage.userUuid !== message.userUuid || nextMessageSendDate !== messageSendDate}
              sendDate={message.sendDate}
              isMaster={roomInfo.userUuid === otherUserInfo?.uuid}
              setScrollbottom={setScrollBottom}
            />
          );
        }
      }

      if (nextMessage) {
        const date1 = moment(message.sendDate).format('YYYY-MM-DD');
        const date2 = moment(nextMessage.sendDate).format('YYYY-MM-DD');

        if (date1 !== date2) {
          result.push(
            <Message.Center
              key={i}
              contents={date2}
            />
          );
        }
      }
    }

    return result;
  }, [theme, userInfo, roomInfo, t, setScrollBottom]);

  /** 채팅방 변경 시 State 초기화 */
  useLayoutEffect(() => {
    const chatContentsDiv = _chat_contents.current;
    if (chatContentsDiv) {
      chatContentsDiv.style.scrollBehavior = 'auto';
      chatContentsDiv.scrollTop = 0;
    }
    setIsInit(true);
    setMessage('');
    setIsChatScrollBottom(true);
    setPrevScrollHeight(null);
    setMsgInputWrapperHeight(61);
    setAnchorEl(null);
  }, [roomInfo?.uuid]);

  /** 채팅메시지 수신 시 스크롤 자동 최하단 이동 */
  useEffect(() => {
    if (!userInfo || !roomInfo) return;

    const chatContentsDiv = _chat_contents.current;
    if (!chatContentsDiv) return;

    const { scrollTop, clientHeight, scrollHeight } = chatContentsDiv;

    if (isInit) {
      chatContentsDiv.scrollTop = scrollHeight - clientHeight;
      chatContentsDiv.removeAttribute('style');
      return setIsInit(false);
    }

    if (prevScrollHeight) {
      chatContentsDiv.scrollTop = scrollHeight - prevScrollHeight;
      return;
    }

    const isChatScrollBottom = scrollHeight - scrollTop - clientHeight < 150;

    if (isChatScrollBottom) {
      chatContentsDiv.scrollTop = scrollHeight - clientHeight;
      return setIsChatScrollBottom(true);
    }
  }, [userInfo, roomInfo, isInit, prevScrollHeight]);

  /** 메시지 입력 시 msg-input-wrapper 높이 체크 */
  useEffect(() => {
    const timeout = setTimeout(() => {
      if (!_msg_input_wrapper.current) return;
      setMsgInputWrapperHeight(_msg_input_wrapper.current.clientHeight);
    }, 10);

    return () => {
      clearTimeout(timeout);
    };
  }, [message]);

  /** 방 입장 시 읽지 않은 메시지 수 초기화 */
  useEffect(() => {
    if (!roomInfo) return;
    const { uuid } = roomInfo;
    setNoReadNum(uuid, 0);
  }, [roomInfo]);

  /** 업로드 메뉴 버튼 클릭 시 실행 */
  useEffect(() => {
    if (!uploadType) return;
    _file_input_ref.current?.click();
  }, [uploadType]);

  /** 업로드 진행 확인 */
  useEffect(() => {
    let timeout: any;
    if (uploading && uploadSize === uploadTotalSize) {
      timeout = setTimeout(() => {
        setUploading(false);
        setUploadSize(0);
        setUploadTotalSize(0);
      }, 500);
    }

    return () => {
      if (timeout) clearTimeout(timeout);
    };
  }, [uploading, uploadSize, uploadTotalSize]);

  if (!userInfo || !roomInfo) return null;

  return (
    <Container
      theme={theme}
      onDragEnter={e => {
        e.preventDefault();
        if (e.dataTransfer.types.indexOf('Files') > -1) {
          setUploadType(null);
          setShowUploadBg(true);
        }
      }}
    >
      {/* 채팅 내용 */}
      <div
        ref={_chat_contents}
        className='chat-contents'
        onScroll={handleScrollChatContents}
        style={{ scrollBehavior: 'auto' }}
      >
        {chatMessages}
      </div>

      {/* 채팅 Input 그룹 */}
      <div
        ref={_msg_input_wrapper}
        className='msg-input-wrapper'
      >
        {/* 이미지, 동영상, 오디오, 파일 전송 메뉴 열기 버튼 */}
        <Button
          className='add-icon-btn'
          variant='outlined'
          type='button'
          onClick={e => setAnchorEl(e.currentTarget)}
        >
          <AddIcon/>
        </Button>

        {/* 메시지 내용 입력 */}
        <TextField
          className='msg-input'
          variant='outlined'
          size='small'
          multiline={true}
          maxRows={5}
          value={message}
          onChange={handleChangeMessage}
          onKeyDown={handleKeyDownMessageInput}
          InputProps={{
            style: {
              minHeight: '40px',
              borderRadius: '20px',
              paddingLeft: '20px',
              paddingRight: '20px',
            }
          }}
        />

        {/* 메시지 전송 버튼 */}
        <Button
          className='send-msg-btn'
          variant='contained'
          onClick={handleClickSendMessageButton}
        >
          <SendIcon/>
        </Button>
      </div>

      {/* 스크롤이 최하단이 아닐 시 마지막 메시지 표시 */}
      {!isChatScrollBottom && _chat_contents.current?.clientHeight !== _chat_contents.current?.scrollHeight && (
        <div
          className='last-msg-preview'
          style={{ bottom: `${msgInputWrapperHeight}px` }}
        >
          <div className='last-msg'>
            <Text className='last-msg-text'>
              {roomInfo.messages[roomInfo.messages.length - 1].contents}
            </Text>
          </div>

          {/* 스크롤 최하단 이동 버튼 */}
          <Button
            className='down-arrow-btn'
            variant='contained'
            onClick={handleClickDownArrowButton}
          >
            <ArrowDownwardRoundedIcon/>
          </Button>
        </div>
      )}

      {/* 채팅입력 우측 + 버튼 */}
      <Menu
        anchorEl={anchorEl}
        open={!!anchorEl}
        anchorOrigin={{ horizontal: 'center', vertical: 'top' }}
        onClose={() => setAnchorEl(null)}
      >
        {/* 사진 전송 메뉴 버튼 */}
        <StyledMenuItem onClick={() => handleClickUploadMenuButton('image')}>
          <ImageRoundedIcon/>
          <Text variant='body1'>{t('image')}</Text>
        </StyledMenuItem>

        {/* 동영상 전송 메뉴 버튼 */}
        <StyledMenuItem onClick={() => handleClickUploadMenuButton('video')}>
          <OndemandVideoRoundedIcon/>
          <Text variant='body1'>{t('video')}</Text>
        </StyledMenuItem>

        {/* 오디오 전송 메뉴 버튼 */}
        <StyledMenuItem onClick={() => handleClickUploadMenuButton('audio')}>
          <AudiotrackRoundedIcon/>
          <Text variant='body1'>{t('audio')}</Text>
        </StyledMenuItem>

        {/* 파일 전송 메뉴 버튼 */}
        <StyledMenuItem onClick={() => handleClickUploadMenuButton('file')}>
          <AttachFileRoundedIcon/>
          <Text variant='body1'>{t('file')}</Text>
        </StyledMenuItem>
      </Menu>

      {/* 업로드 파일 */}
      <input
        ref={_file_input_ref}
        id='upload-files'
        type='file'
        onChange={e => {
          if (!e.target.files || e.target.files.length < 0) return;
          if (e.target.files.length <= 10) return setUploadConfirmModalOpen(true);

          e.target.value = '';
          setUploadErrorMessage('uploadMaxFilesError');
          setUploadErrorModalOpen(true);
        }}
        accept={uploadType && uploadType !== 'file' ? fileExtensions[uploadType].join(',') : undefined}
        multiple
        style={{ display: 'none' }}
      />

      {/* 파일 드래그 화면 */}
      {showUploadBg && (
        <div
          className='file-drag-bg'
          onDragLeave={e => {
            e.preventDefault();
            setShowUploadBg(false);
          }}
          onDragOver={e => {
            e.preventDefault();
            setShowUploadBg(true);
          }}
          onDrop={e => {
            e.preventDefault();
            setShowUploadBg(false);
            if (!_file_input_ref.current) return;

            for (let i = 0; i < e.dataTransfer.items.length; i++) {
              if (!e.dataTransfer.items[i].webkitGetAsEntry()?.isFile) {
                setUploadErrorMessage('cannotUploadDirectory');
                setUploadErrorModalOpen(true);
                return;
              }
            }
            _file_input_ref.current.files = e.dataTransfer.files;
            setUploadConfirmModalOpen(true);
          }}
        >
          <div className='file-drag-bg-wrapper'>
            <UploadIcon className='upload-icon'/>
            <Text color='#fff'>{`${t('file')} ${t('upload')}`}</Text>
          </div>
        </div>
      )}

      {/* 업로드 확인 모달 */}
      <Modal
        open={uploadConfirmModalOpen}
        type='confirm'
        title={uploadType ? `${t(uploadType)} ${t('upload')}` : `${t('file')} ${t('upload')}`}
        onClose={() => setUploadConfirmModalOpen(false)}
        onConfirm={handleClickUploadModalConfirmButton}
      >
        <div style={{ margin: '10px 0' }}>
          <Text>
            {t(`uploadN_${uploadType || 'file'}`).replace('{{n}}', _file_input_ref.current?.files?.length + '')}
          </Text>
        </div>
      </Modal>

      {/* 업로드 에러 Alert 모달 */}
      <Modal
        open={uploadErrorModalOpen}
        type='error'
        title={t('uploadError')}
        onClose={() => setUploadErrorModalOpen(false)}
      >
        <div style={{ margin: '10px 0' }}>
          <Text>
            {t(uploadErrorMessage)}
          </Text>
        </div>
      </Modal>

      {/* 업로드 진행률 */}
      {uploading && (
        <div className='progress-container'>
          <Backdrop/>

          <div style={{ width: '400px', maxWidth: '100%' }}>
            <LineProgress
              progress={uploadTotalSize === 0 ? 0 : uploadSize * 100 / uploadTotalSize}
              showPercentage
            />
          </div>

          <Text
            color='#fff'
            style={{ zIndex: 10 }}
          >
            {t('uploading')}
          </Text>
        </div>
      )}
    </Container>
  );
};

export default ChatRoom;