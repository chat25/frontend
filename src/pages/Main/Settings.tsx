import React, { useCallback, useEffect, useLayoutEffect, useMemo, useState } from 'react';
import { Button, IconButton, MenuItem, Select, SelectChangeEvent, TextField } from '@mui/material';
import styled from 'styled-components';
import DatePicker from '@mui/lab/DatePicker';
import * as uuid from 'uuid';
import _ from 'lodash';
import ReplayRoundedIcon from '@mui/icons-material/ReplayRounded';
import Text from '@/components/common/Text';
import useTranslation from '@/hooks/useTranslation';
import countryCodes from '@/config/countryCodes.json';
import validate from '@/utils/validate';
import {
  CHECK_EMAIL_AUTHCODE,
  CHECK_PHONE_AUTHCODE,
  DELETE_USER,
  EMAIL_VERIFICATION,
  MODIFY_USER_INFO,
  PHONE_VERIFICATION
} from '@/gql/user';
import Modal from '@/components/common/Modal';
import Timer from '@/components/common/Timer';
import { useReactiveVar } from '@apollo/client';
import commonState from '@/gql/vars/common';
import userState, { setUserInfo } from '@/gql/vars/user';
import drawerState from '@/gql/vars/drawer';
import { setPageType } from '@/gql/vars/mainPage';
import { LEAVE_ROOM } from '@/gql/room';
import useApollo from '@/hooks/useApollo';

interface InputInfo {
  error: boolean
  helperText: React.ReactNode
}

interface InputInfos {
  newPassword: InputInfo
  newPasswordConfirm: InputInfo
  name: InputInfo
  email: InputInfo
  emailAuthCode: InputInfo
  phone: InputInfo
  phoneAuthCode: InputInfo
  birth: InputInfo
}

const Container = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  width: 100%;
  overflow: hidden;
  overflow-y: auto;
  padding: 15px;

  .settings-wrapper {
    display: flex;
    flex-direction: column;
    gap: 35px;
    width: 460px;
    max-width: 100%;
    height: fit-content;
    padding: 35px 0;

    .input-wrapper {
      display: flex;
      gap: 10px;

      .refresh-auth-btn {
        width: 35px;
        height: 35px;
        margin-top: 11px;

        .reset-auth-icon {
          fill: ${({ theme }) => theme === 'light' ? '#000' : '#fff'};
        }
      }
    }
  }
`;

const Settings = () => {
  const [newPassword, setNewPassword] = useState<string>('');
  const [newPasswordConfirm, setNewPasswordConfirm] = useState<string>('');
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [emailAuthUuid, setEmailAuthUuid] = useState<string>('');
  const [isValidEmail, setIsValidEmail] = useState<boolean>(false);
  const [isMailSend, setIsMailSend] = useState<boolean>(false);
  const [emailAuthCode, setEmailAuthCode] = useState<string>('');
  const [canResetEmailAuth, setCanResetEmailAuth] = useState<boolean>(false);
  const [countryCode, setCountryCode] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [phoneAuthUuid, setPhoneAuthUuid] = useState<string>('');
  const [isValidPhone, setIsValidPhone] = useState<boolean>(false);
  const [isSmsSend, setIsSmsSend] = useState<boolean>(false);
  const [phoneAuthCode, setPhoneAuthCode] = useState<string>('');
  const [canResetPhoneAuth, setCanResetPhoneAuth] = useState<boolean>(false);
  const [birth, setBirth] = useState<string>('');
  const [modifyCompleteModalOpen, setModifyCompleteModalOpen] = useState<boolean>(false);
  const [leaveConfirmModalOpen, setLeaveConfirmModalOpen] = useState<boolean>(false);
  const [inputInfo, setInputInfo] = useState<InputInfos>({
    newPassword: { error: false, helperText: '' },
    newPasswordConfirm: { error: false, helperText: '' },
    name: { error: false, helperText: '' },
    email: { error: false, helperText: '' },
    emailAuthCode: { error: false, helperText: '' },
    phone: { error: false, helperText: '' },
    phoneAuthCode: { error: false, helperText: '' },
    birth: { error: false, helperText: '' }
  });

  const { theme, lang } = useReactiveVar(commonState.getVar());
  const { userInfo } = useReactiveVar(userState.getVar());
  const { roomList, settingsAuthMethod, settingsAuthUuid } = useReactiveVar(drawerState.getVar());

  const t = useTranslation();
  const apollo = useApollo();

  /** 이메일 인증 필요 여부 */
  const isRequiredEmailAuth = useMemo(() => {
    if (!userInfo) return false;
    return userInfo.email !== email && !isValidEmail;
  }, [userInfo, email, isValidEmail]);

  /** 휴대전화번호 인증 필요 여부 */
  const isRequiredPhoneAuth = useMemo(() => {
    if (!userInfo) return false;
    return (userInfo.countryCode.replace('+', '') !== countryCode.replace('+', '') || userInfo.phone !== phone) && !isValidPhone;
  }, [userInfo, countryCode, phone, isValidPhone]);

  /** 수정된 정보 존재 여부 */
  const canModifyUserInfo = useMemo(() => {
    if (!userInfo) return false;
    return (
      newPassword !== '' ||
      userInfo.name !== name ||
      userInfo.email !== email ||
      userInfo.countryCode.replace('+', '') !== countryCode.replace('+', '') ||
      userInfo.phone !== phone ||
      userInfo.birth !== birth
    );
  }, [userInfo, newPassword, name, email, countryCode, phone, birth]);

  /** 새 비밀번호 변경 이벤트 */
  const handleChangeNewPassword = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(_.cloneDeep(inputInfo));
    inputInfoClone.newPassword = { error: false, helperText: '' };

    setNewPassword(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 새 비밀번호 확인 변경 이벤트 */
  const handleChangeNewPasswordConfirm = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(_.cloneDeep(inputInfo));
    inputInfoClone.newPasswordConfirm = { error: false, helperText: '' };

    setNewPasswordConfirm(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 이름 변경 이벤트 */
  const handleChangeName = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(_.cloneDeep(inputInfo));
    inputInfoClone.name = { error: false, helperText: '' };

    setName(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 이메일 변경 이벤트 */
  const handleChangeEmail = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.email = { error: false, helperText: '' };

    setEmail(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 이메일 인증 */
  const verifyEmail = useCallback(async () => {
    if (!userInfo || email === userInfo.email) return;

    const inputInfoClone = _.cloneDeep(inputInfo);

    if (!validate('email', email)) {
      inputInfoClone.email = { error: true, helperText: 'pleaseEnterCorrectEmail' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.query<{ emailVerification: EmailVerificationResult }, EmailVerificationArgs>({
        query: EMAIL_VERIFICATION,
        variables: { email, lang, checkDuplicate: true }
      });

      if (data.emailVerification.errCode) {
        if (data.emailVerification.errCode === 'DUPLICATE_ERROR') {
          inputInfoClone.email = { error: true, helperText: 'duplicatedEmail' };
          setInputInfo(inputInfoClone);
        }
        throw data.emailVerification.errCode;
      }

      const { emailVerification: { result } } = data;
      inputInfoClone.email = { error: false, helperText: 'waitOneMinuteForResendAuthCode' };
      inputInfoClone.emailAuthCode = { error: false, helperText: '' };

      setEmailAuthUuid(result);
      setIsMailSend(true);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [lang, userInfo, email, inputInfo, apollo]);

  /** 이메일 인증코드 변경 이벤트 */
  const handleChangeEmailAuthCode = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { value } = e.target;
    const isNumberString = validate('numberstring', value);
    if ((value !== '' && !isNumberString) || value.length > 6) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.emailAuthCode = { error: false, helperText: '' };

    setEmailAuthCode(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 이메일 인증코드 확인 */
  const checkEmailAuthCode = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (emailAuthCode.length < 6) {
      inputInfoClone.emailAuthCode = { error: true, helperText: 'pleaseEnterAuthCode' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.mutate<{ checkEmailAuthCode: CheckEmailAuthCodeResult }, CheckEmailAuthCodeArgs>({
        mutation: CHECK_EMAIL_AUTHCODE,
        variables: {
          email,
          authCode: emailAuthCode,
          uuid: emailAuthUuid
        }
      });

      if (data.checkEmailAuthCode.errCode) {
        if (data.checkEmailAuthCode.errCode === 'INVALID_AUTH_CODE') {
          inputInfoClone.emailAuthCode = { error: true, helperText: 'wrongAuthCode' };
          setInputInfo(inputInfoClone);
        }
        throw data.checkEmailAuthCode.errCode;
      }

      inputInfoClone.email = {
        error: false,
        helperText: <span className='success-helper-text'>{t('usableEmail')}</span>
      };
      inputInfoClone.emailAuthCode = { error: false, helperText: '' };

      setIsValidEmail(data.checkEmailAuthCode.result.success);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [email, emailAuthCode, emailAuthUuid, inputInfo, apollo, t]);

  /** 휴대전화 인증코드 확인 */
  const checkPhoneAuthCode = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (phoneAuthCode.length < 6) {
      inputInfoClone.phoneAuthCode = { error: true, helperText: 'pleaseEnterAuthCode' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.mutate<{ checkPhoneAuthCode: CheckPhoneAuthCodeResult }, CheckPhoneAuthCodeArgs>({
        mutation: CHECK_PHONE_AUTHCODE,
        variables: {
          countryCode,
          phone,
          authCode: phoneAuthCode,
          uuid: phoneAuthUuid
        }
      });

      if (data.checkPhoneAuthCode.errCode) {
        if (data.checkPhoneAuthCode.errCode === 'INVALID_AUTH_CODE') {
          inputInfoClone.phoneAuthCode = { error: true, helperText: 'wrongAuthCode' };
          setInputInfo(inputInfoClone);
        }
        throw data.checkPhoneAuthCode.errCode;
      }

      inputInfoClone.phone = {
        error: false,
        helperText: <span className='success-helper-text'>{t('usablePhone')}</span>
      };
      inputInfoClone.phoneAuthCode = { error: false, helperText: '' };

      setIsValidPhone(data.checkPhoneAuthCode.result.success);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [countryCode, phone, phoneAuthCode, phoneAuthUuid, inputInfo, apollo, t]);

  /** 국가 코드 변경 이벤트 */
  const handleChangeCountryCode = useCallback((e: SelectChangeEvent<string>) => {
    setCountryCode(e.target.value);
  }, []);

  /** 휴대전화번호 변경 이벤트 */
  const handleChangePhone = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { value } = e.target;
    const isNumberString = validate('numberstring', value);
    if (value !== '' && !isNumberString) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.phone = { error: false, helperText: '' };

    setPhone(value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 휴대전화 인증 */
  const verifyPhone = useCallback(async () => {
    if (
      !userInfo ||
      (countryCode.replace('+', '') === userInfo.countryCode.replace('+', '') && phone === userInfo.phone)
    ) return;

    const inputInfoClone = _.cloneDeep(inputInfo);

    if (!phone) {
      inputInfoClone.phone = { error: true, helperText: 'pleaseEnterPhone' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.query<{ phoneVerification: PhoneVerificationResult }, PhoneVerificationArgs>({
        query: PHONE_VERIFICATION,
        variables: { countryCode, phone, lang, checkDuplicate: true }
      });

      if (data.phoneVerification.errCode) {
        if (data.phoneVerification.errCode === 'DUPLICATE_ERROR') {
          inputInfoClone.phone = { error: true, helperText: 'duplicatedPhone' };
          setInputInfo(inputInfoClone);
        }
        throw data.phoneVerification.errCode;
      }

      const { phoneVerification: { result } } = data;
      inputInfoClone.phone = { error: false, helperText: 'waitOneMinuteForResendAuthCode' };
      inputInfoClone.phoneAuthCode = { error: false, helperText: '' };

      setPhoneAuthUuid(result);
      setIsSmsSend(true);
      setInputInfo(inputInfoClone);
    } catch (err) {
      console.error(err);
    }
  }, [lang, userInfo, countryCode, phone, inputInfo, apollo]);

  /** 휴대전화 인증코드 변경 이벤트 */
  const handleChangePhoneAuthCode = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { value } = e.target;
    const isNumberString = validate('numberstring', value);
    if ((value !== '' && !isNumberString) || value.length > 6) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.phoneAuthCode = { error: false, helperText: '' };

    setPhoneAuthCode(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 생일 변경 이벤트 */
  const handleChangeBirth = useCallback((birth: Date | null) => {
    if (!birth) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.birth = { error: false, helperText: '' };

    setBirth(birth.toISOString());
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 인증코드 초기화 버튼 클릭 이벤트 */
  const handleClickResetAuthButton = useCallback((authCodeType: 'email' | 'phone') => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    switch (authCodeType) {
      case 'email':
        inputInfoClone.email = { error: false, helperText: '' };
        inputInfoClone.emailAuthCode = { error: false, helperText: '' };
        setEmailAuthCode('');
        setEmailAuthUuid('');
        setInputInfo(inputInfoClone);
        setCanResetEmailAuth(false);
        return setIsMailSend(false);
      default:
        inputInfoClone.phone = { error: false, helperText: '' };
        inputInfoClone.phoneAuthCode = { error: false, helperText: '' };
        setPhoneAuthCode('');
        setPhoneAuthUuid('');
        setInputInfo(inputInfoClone);
        setCanResetPhoneAuth(false);
        return setIsSmsSend(false);
    }
  }, [inputInfo]);

  /** 유저 정보 수정 버튼 클릭 이벤트 */
  const handleClickModifyButton = useCallback(async () => {
    console.log({ settingsAuthMethod, settingsAuthUuid });
    if (!settingsAuthMethod || !settingsAuthUuid) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    let canModify = true;

    if (!validate('password', newPassword) && newPassword !== '') {
      inputInfoClone.newPassword = { error: true, helperText: 'pleaseEnterCorrectPassword' };
      canModify = false;
    }

    if (newPassword !== newPasswordConfirm) {
      inputInfoClone.newPasswordConfirm = { error: true, helperText: 'pleaseEnterSamePassword' };
      canModify = false;
    }

    if (!name) {
      inputInfoClone.name = { error: true, helperText: 'pleaseEnterName' };
      canModify = false;
    }

    if (isRequiredEmailAuth && !isValidEmail) {
      inputInfoClone.email = { error: true, helperText: 'pleaseVerifyEmail' };
      canModify = false;
    }

    if (isRequiredPhoneAuth && !isValidPhone) {
      inputInfoClone.phone = { error: true, helperText: 'pleaseVerifyPhone' };
      canModify = false;
    }

    if (!birth) {
      inputInfoClone.birth = { error: true, helperText: 'pleaseEnterCorrectBirth' };
      canModify = false;
    }

    if (!canModify) return setInputInfo(inputInfoClone);

    try {
      const data = await apollo.mutate<{ modifyUserInfo: ModifyUserInfoResult }, ModifyUserInfoArgs>({
        mutation: MODIFY_USER_INFO,
        variables: {
          userInfo: {
            password: newPassword,
            name,
            email,
            countryCode,
            phone,
            birth
          },
          authType: settingsAuthMethod,
          uuid: {
            init: settingsAuthUuid,
            email: emailAuthUuid || undefined,
            phone: phoneAuthUuid || undefined
          }
        }
      });

      if (data.modifyUserInfo.errCode) throw data.modifyUserInfo.errCode;

      setUserInfo(data.modifyUserInfo.result);
      setModifyCompleteModalOpen(true);
    } catch (err) {
      console.error(err);
    }
  }, [
    settingsAuthMethod, settingsAuthUuid, newPassword, newPasswordConfirm, name, email, countryCode, phone, emailAuthUuid,
    phoneAuthUuid, isRequiredEmailAuth, isValidEmail, isRequiredPhoneAuth, isValidPhone, birth, inputInfo, apollo
  ]);

  /** 수정 완료 모달 확인 버튼 클릭 이벤트 */
  const handleClickModifyCompleteConfirmButton = useCallback(() => {
    setPageType();
    setModifyCompleteModalOpen(false);
  }, []);

  /** 인증코드 타이머 종료 이벤트 */
  const handleTimerEnd = useCallback((authCodeType: 'email' | 'phone') => {
    if (authCodeType === 'email') {
      setCanResetEmailAuth(false);
      return setIsMailSend(false);
    }

    setCanResetPhoneAuth(false);
    setIsSmsSend(false);
  }, []);

  /** 회원탈퇴 모달 확인 버튼 클릭 이벤트 */
  const handleClickLeaveModalConfirmButton = useCallback(async () => {
    try {
      for (const roomInfo of roomList) {
        await apollo.mutate<{ leaveRoom: LeaveRoomResult }, LeaveRoomArgs>({
          mutation: LEAVE_ROOM,
          variables: { roomUuid: roomInfo.uuid, blackList: false }
        });
      }

      const data = await apollo.mutate<{ deleteUser: DeleteUserResult }>({
        mutation: DELETE_USER
      });

      const { errCode } = data.deleteUser;

      if (errCode) throw errCode;

      window.location.href = '/login';
    } catch (err) {
      console.error(err);
    }
  }, [roomList, apollo]);

  /** 이메일 인증코드 타이머 */
  const emailAuthTimer = useMemo(() => (
    <Timer
      seconds={300}
      onEnd={() => handleTimerEnd('email')}
      style={{ display: 'block', width: '41px' }}
    />
  ), [handleTimerEnd]);

  /** 휴대전화 인증코드 타이머 */
  const phoneAuthTimer = useMemo(() => (
    <Timer
      seconds={300}
      onEnd={() => handleTimerEnd('phone')}
      style={{ display: 'block', width: '41px' }}
    />
  ), [handleTimerEnd]);

  /** 이메일 인증코드 전송 1분 후 초기화 버튼 활성 */
  useEffect(() => {
    let timeout: any;
    if (isMailSend) setTimeout(() => {
      setCanResetEmailAuth(true);
    }, 61000);

    return () => {
      clearTimeout(timeout);
    };
  }, [isMailSend]);

  /** 휴대전화 인증코드 전송 1분 후 초기화 버튼 활성 */
  useEffect(() => {
    let timeout: any;
    if (isSmsSend) setTimeout(() => {
      setCanResetPhoneAuth(true);
    }, 61000);

    return () => {
      clearTimeout(timeout);
    };
  }, [isSmsSend]);

  /** 설정 화면 초기화 */
  useLayoutEffect(() => {
    if (!userInfo) return;
    setName(userInfo.name);
    setEmail(userInfo.email);
    setCountryCode(userInfo.countryCode.replace('+', ''));
    setPhone(userInfo.phone);
    setBirth(userInfo.birth);
  }, [userInfo]);

  if (!userInfo) return null;

  return (
    <Container theme={theme}>
      <div className='settings-wrapper'>
        <Text variant='h5'>{t('modifyUserInfo')}</Text>

        {/* 아이디 (수정 불가) */}
        <TextField
          label={t('userId')}
          variant='outlined'
          value={userInfo.userId}
          disabled
        />

        {/* 비밀번호 */}
        <TextField
          label={t('newPassword')}
          value={newPassword}
          type='password'
          variant='outlined'
          error={inputInfo.newPassword.error}
          helperText={typeof inputInfo.newPassword.helperText === 'string' ? t(inputInfo.newPassword.helperText) : inputInfo.newPassword.helperText}
          onChange={handleChangeNewPassword}
        />

        {/* 비밀번호 확인 */}
        <TextField
          label={t('newPasswordConfirm')}
          value={newPasswordConfirm}
          type='password'
          variant='outlined'
          error={inputInfo.newPasswordConfirm.error}
          helperText={typeof inputInfo.newPasswordConfirm.helperText === 'string' ? t(inputInfo.newPasswordConfirm.helperText) : inputInfo.newPasswordConfirm.helperText}
          onChange={handleChangeNewPasswordConfirm}
        />

        {/* 이름 */}
        <TextField
          label={t('name')}
          value={name}
          variant='outlined'
          error={inputInfo.name.error}
          helperText={typeof inputInfo.name.helperText === 'string' ? t(inputInfo.name.helperText) : inputInfo.name.helperText}
          onChange={handleChangeName}
        />

        {/* 이메일 */}
        <div className='input-wrapper'>
          <TextField
            label={t('email')}
            variant='outlined'
            value={email}
            error={inputInfo.email.error}
            helperText={typeof inputInfo.email.helperText === 'string' ? t(inputInfo.email.helperText) : inputInfo.email.helperText}
            disabled={isMailSend || isValidEmail}
            onChange={handleChangeEmail}
            style={{ flex: 1 }}
          />

          <Button
            variant='contained'
            disabled={!isRequiredEmailAuth || isMailSend}
            onClick={verifyEmail}
            style={{ height: '56px' }}
          >
            {t('verify')}
          </Button>
        </div>

        {/* 이메일 인증 코드 */}
        {isMailSend && !isValidEmail && (
          <div className='input-wrapper'>
            <TextField
              variant='outlined'
              value={emailAuthCode}
              error={inputInfo.emailAuthCode.error}
              helperText={typeof inputInfo.emailAuthCode.helperText === 'string' ? t(inputInfo.emailAuthCode.helperText) : inputInfo.emailAuthCode.helperText}
              onChange={handleChangeEmailAuthCode}
              label={`${t('email')} ${t('authCode')}`}
              style={{ flex: 1 }}
            />

            <Button
              variant='contained'
              onClick={checkEmailAuthCode}
              style={{ height: '56px' }}
            >
              {t('confirm')}
            </Button>

            {canResetEmailAuth && (
              <IconButton
                className='refresh-auth-btn'
                onClick={() => handleClickResetAuthButton('email')}
              >
                <ReplayRoundedIcon className='reset-auth-icon'/>
              </IconButton>
            )}

            <div style={{ paddingTop: '18px' }}>
              {emailAuthTimer}
            </div>
          </div>
        )}

        {/* 국가코드, 휴대전화번호 */}
        <div className='input-wrapper'>
          <Select
            value={countryCode}
            renderValue={value => `+${value}`}
            disabled={isSmsSend || isValidPhone}
            onChange={handleChangeCountryCode}
            MenuProps={{ className: theme === 'light' ? 'scroll-bar' : 'scroll-bar-dark' }}
            style={{ width: '85px', height: '56px' }}
          >
            {countryCodes.map(d => (
              <MenuItem key={uuid.v4()} value={d.countryCode + ''}>
                {`+${d.countryCode} (${lang === 'ko' ? d.country : d.countryEnglish})`}
              </MenuItem>
            ))}
          </Select>

          <TextField
            label={t('phoneNumberLabel')}
            variant='outlined'
            value={phone}
            error={inputInfo.phone.error}
            helperText={typeof inputInfo.phone.helperText === 'string' ? t(inputInfo.phone.helperText) : inputInfo.phone.helperText}
            disabled={isSmsSend || isValidPhone}
            onChange={handleChangePhone}
            style={{ flex: 1 }}
          />

          <Button
            variant='contained'
            disabled={!isRequiredPhoneAuth || isSmsSend}
            onClick={verifyPhone}
            style={{ height: '56px' }}
          >
            {t('verify')}
          </Button>
        </div>

        {/* 휴대전화 인증 코드 */}
        {isSmsSend && !isValidPhone && (
          <div className='input-wrapper'>
            <TextField
              variant='outlined'
              value={phoneAuthCode}
              error={inputInfo.phoneAuthCode.error}
              helperText={typeof inputInfo.phoneAuthCode.helperText === 'string' ? t(inputInfo.phoneAuthCode.helperText) : inputInfo.phoneAuthCode.helperText}
              onChange={handleChangePhoneAuthCode}
              label={`${t('phone')} ${t('authCode')}`}
              style={{ flex: 1 }}
            />

            <Button
              variant='contained'
              onClick={checkPhoneAuthCode}
              style={{ height: '56px' }}
            >
              {t('confirm')}
            </Button>

            {canResetPhoneAuth && (
              <IconButton
                className='refresh-auth-btn'
                onClick={() => handleClickResetAuthButton('phone')}
              >
                <ReplayRoundedIcon className='reset-auth-icon'/>
              </IconButton>
            )}

            <div style={{ paddingTop: '18px' }}>
              {phoneAuthTimer}
            </div>
          </div>
        )}

        {/* 생일 */}
        <DatePicker
          label={t('birth')}
          inputFormat='YYYY-MM-DD'
          mask='____-__-__'
          value={new Date(birth)}
          PaperProps={{ className: theme === 'light' ? 'scroll-bar' : 'scroll-bar-dark' }}
          onChange={handleChangeBirth}
          renderInput={params => (
            <TextField
              {...params}
              error={inputInfo.birth.error}
              helperText={typeof inputInfo.birth.helperText === 'string' ? t(inputInfo.birth.helperText) : inputInfo.birth.helperText}
            />
          )}
        />

        {/* 유저 정보 수정 확인 버튼 */}
        <Button
          variant='contained'
          onClick={handleClickModifyButton}
          disabled={!canModifyUserInfo}
        >
          {t('modify')}
        </Button>

        <hr color={theme === 'light' ? '#e0e0e0' : '#777'} style={{ width: '100%' }}/>

        {/* 회원탈퇴 */}
        <Button
          variant='contained'
          onClick={() => setLeaveConfirmModalOpen(true)}
          style={{ backgroundColor: theme === 'light' ? '#db3939' : '#ff6969' }}
        >
          {t('userLeave')}
        </Button>
      </div>

      {/* 유저 정보 수정 완료 모달 */}
      <Modal
        open={modifyCompleteModalOpen}
        type='alert'
        title={t('modifyUserInfo')}
        closeButton={false}
        onConfirm={handleClickModifyCompleteConfirmButton}
      >
        <div style={{ margin: '10px 0' }}>
          <Text>{t('modifyUserInfoComplete')}</Text>
        </div>
      </Modal>

      {/* 회원탈퇴 확인 모달 */}
      <Modal
        open={leaveConfirmModalOpen}
        type='confirm'
        title={t('userLeave')}
        onClose={() => setLeaveConfirmModalOpen(false)}
        onConfirm={handleClickLeaveModalConfirmButton}
        modalStyle={{ width: 'auto' }}
      >
        <div style={{ margin: '10px 0' }}>
          <Text>{t('cannotRecoverAccount')}</Text>
        </div>
      </Modal>
    </Container>
  );
};

export default Settings;