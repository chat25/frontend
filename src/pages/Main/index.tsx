import { useCallback, useEffect, useMemo, useState } from 'react';
import { Alert, Avatar, Button, Checkbox, FormControlLabel, IconButton, ListItemButton, Menu, MenuItem, Slider, Snackbar, Switch, TextField, Tooltip } from '@mui/material';
import { Route, Routes, useNavigate } from 'react-router';
import { useReactiveVar } from '@apollo/client';
import styled from 'styled-components';
import * as uuid from 'uuid';
import moment from 'moment';
import _ from 'lodash';
import MoreVertRoundedIcon from '@mui/icons-material/MoreVertRounded';
import PersonAddAltRoundedIcon from '@mui/icons-material/PersonAddAltRounded';
import ExitToAppRoundedIcon from '@mui/icons-material/ExitToAppRounded';
import LockIcon from '@mui/icons-material/Lock';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import ChevronLeftRoundedIcon from '@mui/icons-material/ChevronLeftRounded';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import SettingsIcon from '@mui/icons-material/Settings';
import GroupRemoveIcon from '@mui/icons-material/GroupRemove';
import CampaignIcon from '@mui/icons-material/Campaign';
import Drawer from '@/components/drawer';
import PageContainer from '@/components/common/PageContainer';
import FriendsList from '@/components/drawer/FriendsList';
import Text from '@/components/common/Text';
import Settings from '@/pages/Main/Settings';
import useTranslation from '@/hooks/useTranslation';
import Logo from '@/components/common/Logo';
import MyRooms from '@/components/drawer/MyRooms';
import ChatRoom from '@/pages/Main/ChatRoom';
import Modal from '@/components/common/Modal';
import commonState from '@/gql/vars/common';
import userState, { setUserInfo } from '@/gql/vars/user';
import mainPageVar, { closeNoticeModal, getNotices, renameRoomName, resetPageContainer, setNoticeModalInfo } from '@/gql/vars/mainPage';
import { connect } from '@/gql/vars/socket';
import { CHANGE_OPEN_CHAT_ROOM_SETTINGS, DELEGATE_ROOM_USER, INVITE_USER, LEAVE_ROOM } from '@/gql/room';
import drawerState, { refreshFriendsList, refreshRoomList, resetSearchOpenChatRoomInfo, setRoomList } from '@/gql/vars/drawer';
import InfiniteScroll from '@/components/common/InfiniteScroll';
import { SEARCH_FRIENDS_INFO } from '@/gql/friends';
import OpenCaht from '@/components/drawer/OpenCaht';
import { ReactComponent as CrownIcon } from '@/assets/svg/crown.svg';
import { READ_NOTICE } from '@/gql/user';
import useApollo from '@/hooks/useApollo';

interface SearchFriendItem extends Friend {
  checked?: boolean
}

const Container = styled(PageContainer)`
  position: relative;
  display: flex;
  flex: 1;
  overflow-y: auto;

  @media screen and (max-width: 620px) {
    overflow-y: unset;

    .main-page-container {
      position: absolute;
      width: 100%;
      height: 100%;
    }
  }

  .main-page-container {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    overflow: hidden;

    .no-page {
      flex: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      gap: 10px;
      width: 100%;
      overflow: hidden;
      overflow-y: auto;
    }

    .main-page-container-header {
      display: flex;
      align-items: center;
      width: 100%;
      padding: 5px;
      background-color: ${({ theme }) => theme === 'light' ? '#1976d2' : '#1c1c1c'};
      gap: 5px;

      .header-left {
        flex: 10;
        display: flex;
        align-items: center;
        gap: 5px;
        overflow: hidden;

        .back-btn {
          padding: 5px;
  
          .back-btn-icon {
            fill: #fff;
          }
        }

        .chat-room-title-wrapper {
          max-width: 50%;
        }

        .chat-room-title {
          overflow: hidden;
          text-overflow: ellipsis;
          word-break: keep-all;
          white-space: nowrap;
          color: #fff !important;
        }

        .chat-room-user-num {
          font-size: 12px;
          margin-left: 10px;
          color: #fff !important;
        }
      }

      .header-right {
        flex: 1;
        display: flex;
        justify-content: flex-end;
        align-items: center;

        .more-btn {
          padding: 5px;

          .more-btn-icon {
            fill: #fff;
          }
        }
      }
    }

    .settings-container {
      flex: 1;
      width: 100%;
      overflow: hidden;
      overflow-y: auto;
    }
  }

  .invite-friends-modal-body {
    display: flex;
    flex-direction: column;
    gap: 25px;

    .input-wrapper {
      display: flex;
      gap: 10px;
      width: 100%;
    }

    .search-user-list {
      height: 300px;
      overflow-y: auto;

      .search-user-item {
        display: flex;
        align-items: center;
        gap: 15px;
      }
    }
  }

  .room-settings-modal-body {
    display: flex;
    flex-direction: column;
    gap: 25px;
    padding: 10px;

    .input-wrapper {
      display: 'flex';
      flex-direction: column;
    }
  }

  .find-member-modal-body {
    display: flex;
    flex-direction: column;
    gap: 25px;

    .input-wrapper {
      display: flex;
      width: 100%;
    }

    .search-user-list {
      height: 300px;
      overflow-y: auto;

      .search-user-item {
        display: flex;
        align-items: center;
        gap: 15px;
      }
    }
  }

  .caution-text {
    color: ${({ theme }) => theme === 'light' ? '#f00' : '#f55353'} !important;
  }
`;

const StyledMenuItem = styled(MenuItem)`
  gap: 30px;
`;

/** 메인 페이지 */
const Main = () => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [exitRoomModalOpen, setExitRoomModalOpen] = useState<boolean>(false);
  const [inviteFriendsModalOpen, setInviteFriendsModalOpen] = useState<boolean>(false);
  const [searchNickname, setSearchNickname] = useState<string>('');
  const [originSearchNickname, setOriginSearchNickname] = useState<string>('');
  const [searchUserList, setSearchUserList] = useState<SearchFriendItem[] |  null>(null);
  const [renameChatRoomModalOpen, setRenameChatRoomModalOpen] = useState<boolean>(false);
  const [newRoomName, setNewRoomName] = useState<string>('');
  const [copyChatRoomCodeToastOpen, setCopyChatRoomCodeToastOpen] = useState<boolean>(false);
  const [roomSettingsModalOpen, setRoomSettingsModalOpen] = useState<boolean>(false);
  const [maxUser, setMaxUser] = useState<number>(10);
  const [newRoomPassword, setNewRoomPassword] = useState<string>('');
  const [isPrivate, setIsPrivate] = useState<boolean>(false);
  const [findRoomMemberModalOpen, setFindRoomMemberModalOpen] = useState<boolean>(false);
  const [filteredMemberList, setFilteredMemberList] = useState<Partial<UserInfo>[]>([]);
  const [findRoomMembersConfirmModalOpen, setFindRoomMembersConfirmModalOpen] = useState<boolean>(false);
  const [findRoomUsersModalType, setFindRoomUsersModalType] = useState<'delegateRoomMaster' | 'kickOut'>('delegateRoomMaster')
  const [selectedUser, setSelectedUser] = useState<Partial<UserInfo> | null>(null);
  const [inputInfo, setInputInfo] = useState({
    newRoomName: { error: false, helperText: '' },
    newRoomPassword: { error: false, helperText: 'pleaseEnterNewPasswordToChange' }
  });

  const { theme, lang } = useReactiveVar(commonState.getVar());
  const { userInfo, token } = useReactiveVar(userState.getVar());
  const { friendsList, roomList } = useReactiveVar(drawerState.getVar());
  const { pageType, roomInfo, noticeModal } = useReactiveVar(mainPageVar);

  const t = useTranslation();
  const navigate = useNavigate();
  const apollo = useApollo();

  /** 채팅방 나가기 메뉴 버튼 클릭 이벤트 */
  const handleClickExitRoomMenu = useCallback(() => {
    setAnchorEl(null);
    setExitRoomModalOpen(true);
  }, []);

  /** 채팅방 나가기 모달 확인 버튼 클릭 이벤트 */
  const handleClickLeaveRoomConfirmButton = useCallback(async (blackList: boolean) => {
    if (!userInfo || !roomInfo) return;

    try {
      const data = await apollo.mutate<{ leaveRoom: LeaveRoomResult }, LeaveRoomArgs>({
        mutation: LEAVE_ROOM,
        variables: { roomUuid: roomInfo.uuid, blackList }
      });

      const { result, errCode } = data.leaveRoom;

      if (errCode) throw errCode;

      if (!result) throw new Error('UNKOWN_ERROR');

      const roomListClone = _.cloneDeep(roomList);
      _.remove(roomListClone, { uuid: roomInfo.uuid });
      console.log({ roomListClone });

      setUserInfo({
        ...userInfo,
        rooms: [
          ...userInfo.rooms.slice(0, userInfo.rooms.indexOf(roomInfo.uuid)),
          ...userInfo.rooms.slice(userInfo.rooms.indexOf(roomInfo.uuid) + 1)
        ]
      });
      setRoomList(roomListClone);
      setExitRoomModalOpen(false);
    } catch (err) {
      console.error(err);
    }
  }, [userInfo, apollo, roomList, roomInfo]);

  /** 친구 초대 버튼 클릭 이벤트 */
  const handleClickInviteFriendsButton = useCallback(() => {
    setInviteFriendsModalOpen(true);
    setSearchUserList(_.cloneDeep(friendsList.map(d => ({ ...d, checked: false }))));
    setSearchNickname('');
    setOriginSearchNickname('');
    setAnchorEl(null);
  }, [friendsList]);

  /** 친구 초대 모달 확인 버튼 클릭 이벤트 */
  const handleClickInviteFriendsModalConfirmButton = useCallback(async () => {
    if (!roomInfo || !searchUserList) return;
    const selectedUserInfo = searchUserList.filter(d => d.checked);
    const selectedUserUuidArr = selectedUserInfo.map(d => d.uuid);
    console.log(selectedUserUuidArr);

    try {
      const data = await apollo.mutate<{ inviteUser: EnterRoomResult }, EnterRoomArgs>({
        mutation: INVITE_USER,
        variables: { roomUuid: roomInfo.uuid, userUuid: selectedUserUuidArr }
      });

      const { errCode } = data.inviteUser;

      if (errCode) throw errCode;

      setInviteFriendsModalOpen(false);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, roomInfo, searchUserList]);

  /** 친구 검색 버튼 클릭 이벤트 */
  const handleClickSearchButton: React.MouseEventHandler<HTMLButtonElement> = useCallback(async e => {
    e.preventDefault();

    try {
      const data = await apollo.query<{ getFriendsInfo: GetFriendsInfoResult }, GetFriendsInfoArgs>({
        query: SEARCH_FRIENDS_INFO,
        variables: { searchNickname, lastNickname: '' }
      });

      const { result, errCode } = data.getFriendsInfo;

      if (errCode) throw errCode;

      setSearchUserList(result.friends.map(d => ({ ...d, checked: false })));
      setOriginSearchNickname(searchNickname);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, searchNickname]);

  /** 친구 검색 목록 Scroll End 이벤트 */
  const handleSearchUserScrollEnd = useCallback(async () => {
    if (!originSearchNickname || !searchUserList) return;

    try {
      const data = await apollo.query<{ getFriendsInfo: GetFriendsInfoResult }, GetFriendsInfoArgs>({
        query: SEARCH_FRIENDS_INFO,
        variables: {
          searchNickname: originSearchNickname,
          lastNickname: searchUserList[searchUserList.length - 1].nickname
        }
      });

      const { result, errCode } = data.getFriendsInfo;

      if (errCode) throw errCode;

      setSearchUserList([...searchUserList, ...result.friends.map(d => ({ ...d, checked: false }))]);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, originSearchNickname, searchUserList]);

  /** 검색한 친구 프로필 클릭 이벤트 */
  const handleClickProfile = useCallback((uuid: string) => {
    const searchUserListClone = _.cloneDeep(searchUserList);
    const profileItem = _.find(searchUserListClone, { uuid });
    if (!profileItem) return;

    profileItem.checked = !profileItem.checked;
    setSearchUserList(searchUserListClone);
  }, [searchUserList]);

  /** 방 이름 변경 메뉴 버튼 클릭 이벤트 */
  const handleClickRenameChatRoomMenuButton = useCallback(() => {
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.newRoomName = { error: false, helperText: '' };
    setAnchorEl(null);
    setRenameChatRoomModalOpen(true);
    setNewRoomName(roomInfo?.name || '');
    setInputInfo(inputInfoClone);
  }, [inputInfo, roomInfo]);

  /** 방 이름 모달 Text Input 변경 이벤트 */
  const handleChangeNewRoomName = useCallback((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    if (inputInfo.newRoomName.error) {
      const inputInfoClone = _.cloneDeep(inputInfo);
      inputInfoClone.newRoomName = { error: false, helperText: '' };
      setInputInfo(inputInfoClone);
    }
    setNewRoomName(e.target.value);
  }, [inputInfo]);

  /** 방 이름 변경 모달 확인 버튼 클릭 이벤트 */
  const handleClickRenameChatRoomModalConfirmButton = useCallback(() => {
    if (!roomInfo) return;
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (newRoomName === roomInfo?.name) {
      inputInfoClone.newRoomName = { error: true, helperText: 'equalChatRoomName' };
      return setInputInfo(inputInfoClone);
    }

    renameRoomName(roomInfo.uuid, newRoomName);
    setRenameChatRoomModalOpen(false);
  }, [newRoomName, roomInfo, inputInfo]);

  /** 채팅방 코드 복사 메뉴 버튼 클릭 이벤트 */
  const handleClickCopyChatRoomCodeButton = useCallback(() => {
    if (!roomInfo) return;
    window.navigator.clipboard.writeText(roomInfo.uuid);
    setCopyChatRoomCodeToastOpen(true);
    setAnchorEl(null);
  }, [roomInfo]);

  /** 방장 위임 메뉴 버튼 클릭 이벤트 */
  const handleClickDelegateRoomMasterButton = useCallback(() => {
    if (!roomInfo) return;
    setFindRoomUsersModalType('delegateRoomMaster');
    setSearchNickname('');
    setFilteredMemberList(roomInfo.users);
    setAnchorEl(null);
    setFindRoomMemberModalOpen(true);
  }, [roomInfo]);

  /** 추방하기 메뉴 버튼 클릭 이벤트 */
  const handleClickKickOutUserButton = useCallback(() => {
    if (!roomInfo) return;
    setFindRoomUsersModalType('kickOut');
    setSearchNickname('');
    setFilteredMemberList(roomInfo.users);
    setAnchorEl(null);
    setFindRoomMemberModalOpen(true);
  }, [roomInfo]);

  /** 방장 위임 확인 모달 확인 버튼 클릭 이벤트 */
  const handleClickDelegateRoomMasterModalConfirmButton = useCallback(async () => {
    if (!roomInfo || !selectedUser || !selectedUser.uuid) return;

    try {
      const data = await apollo.mutate<{ delegateRoomMaster: DelegateRoomMasterResult }, DelegateRoomMasterArgs>({
        mutation: DELEGATE_ROOM_USER,
        variables: { roomUuid: roomInfo.uuid, userUuid: selectedUser.uuid }
      });

      const { errCode } = data.delegateRoomMaster;

      if (errCode) throw errCode;

      setFindRoomMemberModalOpen(false);
      setFindRoomMembersConfirmModalOpen(false);
      setSelectedUser(null);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, roomInfo, selectedUser]);

  /** 추방하기 확인 모달 확인 버튼 클릭 이벤트 */
  const handleClickKickOutUserModalConfirmButton = useCallback(async () => {
    if (!roomInfo || !selectedUser || !selectedUser.uuid) return;

    try {
      const data = await apollo.mutate<{ leaveRoom: LeaveRoomResult }, LeaveRoomArgs>({
        mutation: LEAVE_ROOM,
        variables: {
          roomUuid: roomInfo.uuid,
          userUuid: selectedUser.uuid,
          blackList: true
        }
      });

      const { errCode } = data.leaveRoom;

      if (errCode) throw errCode;

      setFindRoomMembersConfirmModalOpen(false);
      setSelectedUser(null);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, roomInfo, selectedUser]);

  /** 방 설정 메뉴 버튼 클릭 이벤트 */
  const handleClickRoomSettingsButton = useCallback(() => {
    console.log(roomInfo);
    if (!roomInfo || !roomInfo.originName || !roomInfo.maxUser || roomInfo.isPrivate === undefined) return;
    setNewRoomName(roomInfo.originName);
    setMaxUser(roomInfo.maxUser);
    setNewRoomPassword(roomInfo.password || '');
    setIsPrivate(roomInfo.isPrivate);
    setAnchorEl(null);
    setRoomSettingsModalOpen(true);
  }, [roomInfo]);

  /** 방 설정 모달 확인 버튼 클릭 이벤트 */
  const handleClickRoomSettingsConfirmButton = useCallback(async () => {
    if (!roomInfo) return;

    try {
      const data = await apollo.mutate<{ changeRoomSettings: ChangeRoomSettingsResult }, ChangeRoomSettingsArgs>({
        mutation: CHANGE_OPEN_CHAT_ROOM_SETTINGS,
        variables: {
          roomUuid: roomInfo.uuid,
          name: newRoomName,
          maxUser,
          password: newRoomPassword,
          isPrivate
        }
      });

      const { errCode } = data.changeRoomSettings;

      if (errCode) {
        if (errCode === 'DUPLICATED_ROOM_NAME') {
          const inputInfoClone = _.cloneDeep(inputInfo);
          inputInfoClone.newRoomName = { error: true, helperText: 'duplicatedOpenChatRoomName' };
          setInputInfo(inputInfoClone);
        }
        throw errCode;
      }

      setRoomSettingsModalOpen(false);
    } catch (err) {
      console.error(err);
    }
  }, [roomInfo, apollo, newRoomName, maxUser, newRoomPassword, isPrivate, inputInfo]);

  /** 공지 모달 확인 버튼 클릭 이벤트 */
  const handleClickNoticeModalConfirmButton = useCallback(async () => {
    try {
      if (noticeModal[0].checked) {
        const data = await apollo.mutate<{ readNotice: ReadNoticeResult }, ReadNoticeArgs>({
          mutation: READ_NOTICE,
          variables: { noticeUuid: noticeModal[0].uuid }
        });

        const { errCode } = data.readNotice;

        if (errCode) throw errCode;
      }
    } catch (err) {
      console.error(err);
    }
    
    closeNoticeModal();
  }, [noticeModal, apollo]);

  /** 채팅방 제목 */
  const chatRoomTitle = useMemo(() => (
    <Tooltip
      title={(
        <div
          className={`page-container${theme === 'light' ? '' : '-dark'}`}
          style={{
            width: '100%',
            maxHeight: '300px',
            display: 'flex',
            flexDirection: 'column',
            gap: '10px',
            padding: '10px',
            overflowY: 'auto',
            backgroundColor: 'unset'
          }}
        >
          {roomInfo?.users.map(d => {
            const key = uuid.v4();
            return (
              <div
                key={key}
                style={{ display: 'flex', alignItems: 'center', gap: '10px', minWidth: '200px' }}
              >
                <Avatar src={d.profileImage} alt={key}/>
                <Text color='#fff'>{d.nickname}</Text>
              </div>
            );
          })}
        </div>
      )}
      placement='bottom-start'
    >
      <div className='chat-room-title-wrapper'>
        <Text className='chat-room-title' title={roomInfo?.name}>
          {roomInfo?.name}
        </Text>
      </div>
    </Tooltip>
  ), [theme, roomInfo]);

  /** 로그인 상태가 아니면 로그인 페이지로 이동 */
  useEffect(() => {
    console.log({ userInfo, token });
    if (userInfo && token) return;
    navigate('/login');
  }, [userInfo, token, navigate]);

  /** 컴포넌트 언마운트 시 Page Container 초기화 */
  useEffect(() => {
    // 소켓 연결
    connect();
    refreshFriendsList();
    refreshRoomList();
    resetSearchOpenChatRoomInfo();
    getNotices();

    return () => {
      resetPageContainer();
    };
  }, []);

  if (!userInfo) return <Container theme={theme}/>;

  return (
    <Container theme={theme}>
      {/* 친구 목록, 채팅방, 오픈채팅, 설정 Drawer */}
      <Drawer>
        <Routes>
          <Route path='/*' element={<FriendsList/>}/>
          <Route path='/rooms' element={<MyRooms/>}/>
          <Route path='/openchat' element={<OpenCaht/>}/>
        </Routes>
      </Drawer>

      {/* Drawer 우측 컨테이너 (Main Page Container) */}
      <div className='main-page-container'>
        {!pageType && (
          <div className='no-page'>
            <Logo style={{ width: '115px', height: '115px' }}/>
            <Text style={{ userSelect: 'none' }}>{t('pleaseSelectRoom')}</Text>
          </div>
        )}

        {!!pageType && (
          <>
            {/* Main Page Container 헤더 */}
            <div className='main-page-container-header'>
              {/* Main Page Container 헤더 좌측 */}
              <div className='header-left'>
                <IconButton className='back-btn' onClick={() => resetPageContainer()}>
                  <ChevronLeftRoundedIcon className='back-btn-icon'/>
                </IconButton>

                {pageType === 'personalchat' && (
                  // 개인 채팅방 제목
                  chatRoomTitle
                )}

                {pageType === 'groupchat' && (
                  <>
                    {/* 그룹 채팅방 제목 */}
                    {chatRoomTitle}

                    {/* 그룹 채팅방 인원 수 */}
                    <Text className='chat-room-user-num'>
                      ({roomInfo?.userNum || -1})
                    </Text>
                  </>
                )}

                {pageType === 'openchat' && (
                  <>
                    {/* 오픈 채팅방 제목 */}
                    {chatRoomTitle}

                    {/* 오픈 채팅방 인원 수 */}
                    <Text className='chat-room-user-num'>
                      ({roomInfo?.userNum || -1}/{roomInfo?.maxUser || -1})
                    </Text>

                    <div style={{ display: 'flex', alignItems: 'center', marginLeft: '10px', gap: '5px' }}>
                      {/* 방장 아이콘 */}
                      {userInfo?.uuid === roomInfo?.userUuid && (
                        <Tooltip title={t('roomMaster')} arrow>
                          <CrownIcon style={{ width: '15px', height: '15px' }}/>
                        </Tooltip>
                      )}

                      {/* 비공개 채팅방 아이콘 */}
                      {roomInfo?.isPrivate && (
                        <Tooltip title={t('private')} arrow>
                          <VisibilityOffIcon style={{ width: '15px', height: '15px', fill: '#fff' }}/>
                        </Tooltip>
                      )}

                      {/* 비밀번호 사용 아이콘 */}
                      {roomInfo?.password && (
                        <Tooltip title={t('usingPassword')} arrow>
                          <LockIcon style={{ width: '15px', height: '15px', fill: '#fff' }}/>
                        </Tooltip>
                      )}
                    </div>
                  </>
                )}

                {pageType === 'settings' && <Text color='#fff'>{t('settings')}</Text>}
              </div>

              {/* Main Page Container 헤더 우측 */}
              {pageType !== 'settings' && (
                <div className='header-right'>
                  <IconButton
                    className='more-btn'
                    onClick={e => setAnchorEl(e.currentTarget)}
                  >
                    <MoreVertRoundedIcon className='more-btn-icon'/>
                  </IconButton>
                </div>
              )}
            </div>

            {pageType === 'settings' && <Settings/>}
            {pageType === 'personalchat' && <ChatRoom/>}
            {pageType === 'groupchat' && <ChatRoom/>}
            {pageType === 'openchat' && <ChatRoom/>}
          </>
        )}
      </div>

      {pageType !== 'settings' && !!roomInfo && (
        <>
          <Menu
            anchorEl={anchorEl}
            open={!!anchorEl}
            onClose={() => setAnchorEl(null)}
          >
            {roomInfo?.type === 'group' && ([
              // 채팅방 친구 초대 메뉴 버튼
              <StyledMenuItem key={uuid.v4()} onClick={handleClickInviteFriendsButton}>
                <PersonAddAltRoundedIcon/>
                <Text variant='body1'>{t('inviteFriends')}</Text>
              </StyledMenuItem>
            ])}

            {roomInfo?.type === 'open' && (() => {
              const result = [
                // 채팅방 코드 복사 메뉴 버튼
                <StyledMenuItem key={uuid.v4()} onClick={handleClickCopyChatRoomCodeButton}>
                  <ContentCopyIcon/>
                  <Text variant='body1'>{t('copyChatRoomCode')}</Text>
                </StyledMenuItem>
              ];

              if (userInfo.uuid === roomInfo.userUuid) result.push(
                // 방 설정 메뉴 버튼
                <StyledMenuItem key={uuid.v4()} onClick={handleClickRoomSettingsButton}>
                  <SettingsIcon/>
                  <Text variant='body1'>{t('roomSettings')}</Text>
                </StyledMenuItem>,
                // 방장 위임 메뉴 버튼
                <StyledMenuItem key={uuid.v4()} onClick={handleClickDelegateRoomMasterButton}>
                  <PeopleAltIcon/>
                  <Text variant='body1'>{t('delegateRoomMaster')}</Text>
                </StyledMenuItem>,
                // 추방하기
                <StyledMenuItem key={uuid.v4()} onClick={handleClickKickOutUserButton}>
                  <GroupRemoveIcon/>
                  <Text variant='body1'>{t('kickOut')}</Text>
                </StyledMenuItem>
              );

              return result;
            })()}

            {/* 방 이름 변경 메뉴 버튼 */}
            <StyledMenuItem onClick={handleClickRenameChatRoomMenuButton}>
              <ModeEditIcon/>
              <Text variant='body1'>{t('renameChatRoom')}</Text>
            </StyledMenuItem>

            {/* 채팅방 나기기 메뉴 버튼 */}
            <StyledMenuItem onClick={handleClickExitRoomMenu}>
              <ExitToAppRoundedIcon/>
              <Text variant='body1'>{t('exit')}</Text>
            </StyledMenuItem>
          </Menu>

          {roomInfo?.type === 'group' && (
            <>
              {/* 그룹 채팅방 친구 초대 모달 */}
              <Modal
                open={inviteFriendsModalOpen}
                title={t('inviteFriends')}
                type='confirm'
                confirmText={t('invite')}
                onConfirm={handleClickInviteFriendsModalConfirmButton}
                onClose={() => setInviteFriendsModalOpen(false)}
              >
                <div className='invite-friends-modal-body'>
                  {/* 친구 검색 Form */}
                  <form className='input-wrapper' action='#'>
                    <TextField
                      label={t('nickname')}
                      value={searchNickname}
                      onChange={e => setSearchNickname(e.target.value)}
                      style={{ flex: 1 }}
                    />
                    <Button
                      variant='contained'
                      type='submit'
                      onClick={handleClickSearchButton}
                    >
                      {t('search')}
                    </Button>
                  </form>

                  {/* 친구 검색 후 */}
                  {!!searchUserList && (searchUserList.length > 0 ? (
                    <InfiniteScroll
                      className='search-user-list'
                      onScrollEnd={handleSearchUserScrollEnd}
                    >
                      {searchUserList.filter(d => (roomInfo.blackList?.indexOf(d.uuid) || 0) < 0).map(d1 => {
                        const isExistUser = !!_.find(roomInfo?.users, d2 => d1.uuid === d2.uuid);

                        return (
                          <ListItemButton
                            key={uuid.v4()}
                            className='search-user-item'
                            disabled={isExistUser}
                            onClick={() => handleClickProfile(d1.uuid)}
                          >
                            <Checkbox checked={isExistUser || d1.checked} disabled={isExistUser} onChange={() => handleClickProfile(d1.uuid)}/>
                            <Avatar src={d1.profileImage}/>
                            <Text>{d1.nickname}</Text>
                          </ListItemButton>
                        );
                      })}
                    </InfiniteScroll>
                  ) : (
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '300px' }}>
                      <Text>{t('noSearchUser')}</Text>
                    </div>
                  ))}
                </div>
              </Modal>
            </>
          )}

          {pageType === 'openchat' && userInfo.uuid === roomInfo?.userUuid && (
            <>
              {/* 오픈 채팅방 설정 모달 */}
              <Modal
                open={roomSettingsModalOpen}
                title={t('roomSettings')}
                type='confirm'
                onClose={() => setRoomSettingsModalOpen(false)}
                onConfirm={handleClickRoomSettingsConfirmButton}
              >
                <div className='room-settings-modal-body'>
                  {/* 기본 채팅방 이름 */}
                  <div className='input-wrapper'>
                    <Text>{`${t('default')} ${t('chatRoomName')}`}</Text>
                    <TextField
                      error={inputInfo.newRoomName.error}
                      helperText={t(inputInfo.newRoomName.helperText)}
                      value={newRoomName}
                      onChange={handleChangeNewRoomName}
                      style={{ width: '100%', marginTop: '10px' }}
                    />
                  </div>

                  {/* 최대 인원 수 */}
                  <div className='input-wrapper'>
                    <Text>{t('maxUserNum')}</Text>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        padding: '0 10px',
                        gap: '15px'
                      }}
                    >
                      <Slider
                        min={2}
                        max={100}
                        step={1}
                        value={maxUser}
                        onChange={(e, value) => {
                          if (!roomInfo || value < roomInfo.userNum) return;
                          setMaxUser(+value)
                        }}
                        style={{ flex: 1 }}
                      />
                      <TextField
                        size='small'
                        value={maxUser}
                        style={{ width: '60px' }}
                        InputProps={{ readOnly: true }}
                        inputProps={{ style: { textAlign: 'center' } }}
                      />
                    </div>
                  </div>

                  {/* 비밀번호 */}
                  <div className='input-wrapper'>
                    <Text>{t('password')}</Text>
                    <TextField
                      type='password'
                      error={inputInfo.newRoomPassword.error}
                      helperText={t(inputInfo.newRoomPassword.helperText)}
                      value={newRoomPassword}
                      onChange={e => setNewRoomPassword(e.target.value)}
                      style={{ width: '100%', marginTop: '10px' }}
                    />
                  </div>

                  {/* 공개 여부 */}
                  <div className='input-wrapper'>
                    <Text>{t('disclosureScope')}</Text>
                    <FormControlLabel
                      control={<Switch checked={!isPrivate} onChange={(e, checked) => setIsPrivate(!checked)}/>}
                      label={<Text>{isPrivate ? t('private') : t('public')}</Text>}
                      style={{ padding: '0 10px' }}
                    />
                  </div>
                </div>
              </Modal>

              {/* 채팅방 인원 검색 모달 (방장 위임, 추방하기) */}
              <Modal
                open={findRoomMemberModalOpen}
                type='no-footer'
                title={(
                  <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
                    <PersonAddAltRoundedIcon style={{ fill: theme === 'light' ? '#000' : '#fff' }}/>
                    <Text variant='h6'>{t(findRoomUsersModalType)}</Text>
                  </div>
                )}
                onClose={() => setFindRoomMemberModalOpen(false)}
              >
                <div className='find-member-modal-body'>
                  <form className='input-wrapper' action='#'>
                    <TextField
                      label={t('nickname')}
                      value={searchNickname}
                      onChange={e => setSearchNickname(e.target.value)}
                      style={{ flex: 1 }}
                    />
                  </form>

                  <div className='search-user-list'>
                    {filteredMemberList.filter(d => d.nickname ? RegExp(searchNickname).test(d.nickname) : false).map(d => (
                      <ListItemButton
                        key={uuid.v4()}
                        className='search-user-item'
                        disabled={d.uuid === userInfo.uuid}
                        onClick={() => {
                          if (!d.nickname) return;
                          setSelectedUser(d);
                          setFindRoomMembersConfirmModalOpen(true);
                        }}
                      >
                        <Avatar src={d.profileImage}/>
                        <Text>{d.nickname}</Text>
                      </ListItemButton>
                    ))}
                  </div>
                </div>
              </Modal>

              {/* 채팅방 인원 검색 확인 모달 */}
              <Modal
                open={findRoomMembersConfirmModalOpen}
                type='confirm'
                title={t(findRoomUsersModalType)}
                onClose={() => setFindRoomMembersConfirmModalOpen(false)}
                onConfirm={
                  findRoomUsersModalType === 'delegateRoomMaster' ?
                  handleClickDelegateRoomMasterModalConfirmButton :
                  handleClickKickOutUserModalConfirmButton
                }
              >
                <div style={{ margin: '10px 0' }}>
                  {findRoomUsersModalType === 'delegateRoomMaster' && (
                    <Text>
                      {t('delegateRoomMasterTo').replace('{{nickname}}', selectedUser?.nickname || '(Unknown)')}
                    </Text>
                  )}

                  {findRoomUsersModalType === 'kickOut' && (
                    <div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
                      <Text>{t('kickOutUser').replace('{{nickname}}', selectedUser?.nickname || '(Unknown)')}</Text>
                      <Text className='caution-text'>({t('kickOutUserCannotEnterRoom')})</Text>
                    </div>
                  )}
                </div>
              </Modal>
            </>
          )}

          {/* 채팅방 이름 변경 모달 */}
          <Modal
            open={renameChatRoomModalOpen}
            type='no-footer'
            title={t('renameChatRoom')}
            onClose={() => setRenameChatRoomModalOpen(false)}
          >
            <div style={{ padding: '10px' }}>
              <TextField
                label={t('chatRoomName')}
                error={inputInfo.newRoomName.error}
                helperText={t(inputInfo.newRoomName.helperText)}
                value={newRoomName}
                onChange={handleChangeNewRoomName}
                style={{ width: '100%' }}
              />
            </div>

            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '10px' }}>
              <Button
                variant='contained'
                onClick={() => {
                  if (!roomInfo) return;
                  renameRoomName(roomInfo.uuid);
                  setRenameChatRoomModalOpen(false);
                }}
                style={{ backgroundColor: theme === 'light' ? '#db3939' : '#ff6969' }}
              >
                {t('reset')}
              </Button>

              <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
                <Button
                  variant='contained'
                  onClick={() => setRenameChatRoomModalOpen(false)}
                  style={{ backgroundColor: '#999' }}
                >
                  {t('close')}
                </Button>
                <Button
                  variant='contained'
                  onClick={handleClickRenameChatRoomModalConfirmButton}
                >
                  {t('confirm')}
                </Button>
              </div>
            </div>
          </Modal>

          {/* 채팅방 나가기 확인 모달 */}
          <Modal
            open={exitRoomModalOpen}
            type='no-footer'
            title={(
              <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
                <ExitToAppRoundedIcon style={{ fill: theme === 'light' ? '#000' : '#fff' }}/>
                <Text variant='h6'>{t('exit')}</Text>
              </div>
            )}
            onClose={() => setExitRoomModalOpen(false)}
          >
            <div style={{ display: 'flex', flexDirection: 'column', gap: '10px', margin: '24px 0' }}>
              <Text style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
                {t('exitRoomModalMessage')}
              </Text>

              {roomInfo.type !== 'personal' && (
                <Text className='caution-text'>
                  ({t('cannotEnterRoomAfterBlacklist')})
                </Text>
              )}
            </div>

            <div style={{ display: 'flex', justifyContent: 'flex-end', gap: '10px', paddingTop: '10px' }}>
              {/* 닫기 버튼 */}
              <Button
                variant='contained'
                onClick={() => setExitRoomModalOpen(false)}
                style={{ backgroundColor: '#999' }}
              >
                {t('close')}
              </Button>

              {/* 차단 후 나가기 버튼 */}
              {roomInfo.type !== 'personal' && (
                <Button
                  variant='contained'
                  onClick={() => handleClickLeaveRoomConfirmButton(true)}
                  style={{ backgroundColor: theme === 'light' ? '#db3939' : '#ff6969' }}
                >
                  {t('exitAfterBlacklist')}
                </Button>
              )}

              {/* 나가기 버튼 */}
              <Button
                variant='contained'
                onClick={() => handleClickLeaveRoomConfirmButton(false)}
              >
                {t('exit')}
              </Button>
            </div>
          </Modal>

          {/* 클립보드 복사 Toast */}
          <Snackbar
            open={copyChatRoomCodeToastOpen}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            autoHideDuration={3000}
            onClose={() => setCopyChatRoomCodeToastOpen(false)}
          >
            <Alert severity='success' variant='filled'>
              {t('copyChatRoomCodeComplete')}
            </Alert>
          </Snackbar>
        </>
      )}

      {/* 공지 모달 */}
      <Modal
        open={noticeModal.length > 0}
        type='no-footer'
        title={(
          <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <CampaignIcon style={{ fill: theme === 'light' ? '#000' : '#fff' }}/>
            <Text>{noticeModal.length > 0 ? (lang === 'ko' ? noticeModal[0].noticeTitle : noticeModal[0].noticeTitleEn) : ''}</Text>
          </div>
        )}
        onClose={closeNoticeModal}
      >
        {noticeModal.length > 0 && (
          <div style={{ display: 'flex', flexDirection: 'column', gap: '15px', margin: '10px' }}>
            {/* 공지 기간 */}
            <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
              <Text variant='caption'>
                {moment(noticeModal[0].noticeStartDate).format('YYYY-MM-DD HH:mm')}
              </Text>
              <Text variant='caption'>~</Text>
              <Text variant='caption'>
                {moment(noticeModal[0].noticeEndDate).format('YYYY-MM-DD HH:mm')}
              </Text>
            </div>

            {/* 공지 내용 */}
            <Text>
              {lang === 'ko' ? noticeModal[0].noticeContents : noticeModal[0].noticeContentsEn}
            </Text>

            {/* 하단 버튼 그룹 */}
            <div style={{ display: 'flex', justifyContent: 'space-between', alignContent: 'center' }}>
              <FormControlLabel
                control={(
                  <Checkbox
                    checked={noticeModal[0].checked}
                    onChange={e => setNoticeModalInfo([{ ...noticeModal[0], checked: !noticeModal[0].checked }, ...noticeModal.slice(1)])}
                  />
                )}
                label={<Text>{t('dontShowAgain')}</Text>}
              />

              <Button
                variant='contained'
                onClick={handleClickNoticeModalConfirmButton}
              >
                {t('confirm')}
              </Button>
            </div>
          </div>
        )}
      </Modal>
    </Container>
  );
};

export default Main;
