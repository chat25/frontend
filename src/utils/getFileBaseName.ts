const getFileBaseName = ((path: string) => path.substring(path.lastIndexOf('/') + 1));

export default getFileBaseName;