import { Crop } from 'react-image-crop';

const getCroppedImg =(image: HTMLImageElement | null, crop: Partial<Crop>, mimeType: string) => {
  if (
    !image ||
    (!crop.width && crop.width !== 0) ||
    (!crop.height && crop.height !== 0) ||
    (!crop.x && crop.x !== 0) ||
    (!crop.y && crop.y !== 0)
  ) return;

  const canvas = document.createElement('canvas');
  const scaleX = image.naturalWidth / image.width;
  const scaleY = image.naturalHeight / image.height;
  console.log({ scaleX, scaleY })

  canvas.width = crop.width;
  canvas.height = crop.height;
  const ctx = canvas.getContext('2d');
  if (!ctx) return;

  const pixelRatio = window.devicePixelRatio;
  canvas.width = crop.width * pixelRatio;
  canvas.height = crop.height * pixelRatio;
  ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
  ctx.imageSmoothingQuality = 'high';

  ctx.drawImage(
    image,
    crop.x * scaleX,
    crop.y * scaleY,
    crop.width * scaleX,
    crop.height * scaleY,
    0,
    0,
    crop.width,
    crop.height
  );

  const base64Image = canvas.toDataURL(mimeType);

  image.remove();
  canvas.remove();

  return base64Image;
};

export default getCroppedImg;