declare global {
  type ThemeMode = 'light' | 'dark';
  type Language = 'ko' | 'en';

  interface CountryCode {
    region: string
    country: string
    countryEnglish: string
    countryCode: number
  }

  type DrawerMenu =
    | 'friends'
    | 'rooms'
    | 'openchat'
    | 'settings';

  interface ChatInfo {
    uuid: string
    userUuid: string
    roomUuid: string
    type: ChatType
    contents: string | null
    filePath: string
    sendDate: string
    read: string[] | null
  }

  type UploadType = 'image' | 'video' | 'audio' | 'file';
}

export {};