import React from 'react';
import { Box, LinearProgress, Typography } from '@mui/material';

interface LineProgressProps {
  progress: number
  showPercentage?: boolean
}

export const LineProgress: React.FC<LineProgressProps> = props => {
  const {
    progress,
    showPercentage
  } = props;

  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }} style={{ zIndex: 30 }}>
      <Box sx={{ width: '100%', mr: 1 }}>
        <LinearProgress variant='determinate' value={progress}/>
      </Box>
      {showPercentage && (
        <Box sx={{ minWidth: 35, zIndex: 30 }}>
          <Typography variant='body2' color='#fff'>
            {`${Math.round(progress)}%`}
          </Typography>
        </Box>
      )}
    </Box>
  );
};

LineProgress.defaultProps = {
  showPercentage: false
};