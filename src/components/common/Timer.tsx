import { useReactiveVar } from '@apollo/client';
import { useMemo } from 'react';
import CountUp from 'react-countup';
import commonState from '@/gql/vars/common';

interface TimerProps {
  seconds: number
  onEnd?: () => any
  style?: React.CSSProperties
}

const Timer: React.FC<TimerProps> = ({ seconds, onEnd, style }) => {
  const { theme } = useReactiveVar(commonState.getVar());

  const countUp = useMemo(() => (
    <CountUp
      style={style}
      start={seconds}
      end={0}
      duration={seconds}
      delay={0}
      useEasing={false}
      onEnd={onEnd || undefined}
      formattingFn={num => {
        const minNum = Math.floor(num / 60);
        const secNum = num % 60;

        const min = (minNum + '').length < 2 ? `0${minNum}` : `${minNum}`;
        const sec = (secNum + '').length < 2 ? `0${secNum}` : `${secNum}`;

        return `${min}:${sec}`;
      }}
    />
  ), [seconds, onEnd, style]);

  return (
    <div style={{ color: theme === 'light' ? '#000' : '#fff' }}>
      {countUp}
    </div>
  );
};

export default Timer;