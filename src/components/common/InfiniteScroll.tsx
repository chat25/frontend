import React, { useCallback, useMemo } from 'react';
import _ from 'lodash';

type DivProps = React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

interface InfiniteScrollProps extends DivProps {
  onScrollEnd?: (e: React.UIEvent<HTMLDivElement, UIEvent>) => void
}

const InfiniteScroll: React.FC<InfiniteScrollProps> = props => {
  const { onScrollEnd } = props;

  const divProps = useMemo(() => _.pickBy<DivProps>(props, (d, k) => k !== 'onScrollEnd'), [props]);

  const handleScroll: React.UIEventHandler<HTMLDivElement> = useCallback((e: any) => {
    if (!onScrollEnd || !e || !e.target || !e.target.scrollHeight || !e.target.scrollTop || !e.target.clientHeight) return;

    const { scrollHeight, scrollTop, clientHeight } = e.target;
    if (scrollHeight - scrollTop !== clientHeight) return;

    onScrollEnd(e);
  }, [onScrollEnd]);

  return <div {...divProps} onScroll={handleScroll}/>;
};

export default InfiniteScroll;