import React, { useMemo } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { Avatar } from '@mui/material';
import { useReactiveVar } from '@apollo/client';
import * as uuid from 'uuid';
import PlayCircleOutlineIcon from '@mui/icons-material/PlayCircleOutline';
import AudiotrackIcon from '@mui/icons-material/Audiotrack';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import Text from '@/components/common/Text';
import commonState from '@/gql/vars/common';
import { ReactComponent as CrownIcon } from '@/assets/svg/crown.svg';
import { backendUrl } from '@/utils/apolloClient';
import getFileBaseName from '@/utils/getFileBaseName';

interface CenterMessageProps {
  contents: string
  color?: string
}

interface MessageProps {
  uuid?: string
  type: ChatType
  profileImage?: string
  nickname?: string
  contents?: string
  filePath?: string
  showProfile?: boolean
  showTime?: boolean
  sendDate: string
  isMaster?: boolean
  setScrollbottom?: () => void
}

const Container = styled.div<{ color?: string }>`
  display: flex;
  align-items: flex-end;
  width: 100%;
  padding: 10px 20px;
  gap: 10px;

  .center-msg-wrapper {
    height: 26px;
    padding: 3px 10px;
    border-radius: 13px;
    background-color: rgba(0, 0, 0, .7);
    text-align: center;

    .center-contents-text {
      color: ${({ color }) => color || '#fff'} !important;
      white-space: nowrap;
    }
  }

  .msg-wrapper {
    max-width: 45%;
    padding: 10px;
    border-radius: 5px;
    word-break: break-all;
    text-overflow: ellipsis;

    @media screen and (max-width: 620px) {
      .msg-text {
        font-size: 0.8rem;
      }
    }

    .msg-text {
      color: #000 !important;
    }
  }

  @media screen and (max-width: 420px) {
    .msg-wrapper {
      max-width: 70%;
    }
  }

  .profile-wrapper {
    display: flex;
    align-items: center;
    gap: 10px;

    @media screen and (max-width: 620px) {
      .other-user-nickname {
        font-size: 0.8rem;
      }
    }

    .profile-avatar-wrapper {
      position: relative;

      .crown-icon {
        position: absolute;
        width: 20px;
        height: 20px;
        top: -15px;
        left: 50%;
        transform: translate(-50%, 0);
      }
    }
  }

  .other-chat-container {
    display: flex;
    align-items: flex-end;
    width: 100%;
    gap: 10px;
  }

  .media-chat-container {
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 10px;
    cursor: pointer;
    user-select: none;

    .media-chat-title {
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      font-weight: bold;
    }

    .media-icon {
      width: 35px;
      height: 35px;
    }
  }
`;

const Center: React.FC<CenterMessageProps> = props => {
  const {
    contents,
    color
  } = props;

  return (
    <Container style={{ justifyContent: 'center' }} color={color}>
      <div className='center-msg-wrapper'>
        <Text
          variant='caption'
          className='center-contents-text'
        >
          {contents}
        </Text>
      </div>
    </Container>
  );
};

const MyChat: React.FC<MessageProps> = props => {
  const {
    uuid: chatUuid,
    type,
    contents,
    filePath,
    showTime,
    sendDate,
    setScrollbottom
  } = props;

  const { theme } = useReactiveVar(commonState.getVar());

  const fileName = useMemo(() => {
    if (!filePath) return '';
    let result = getFileBaseName(filePath || '');
    result = result.substring(result.indexOf('_') + 1);
    return result;
  }, [filePath]);

  return (
    <Container style={{ justifyContent: 'flex-end' }}>
      {showTime && (
        <Text className='date-text' variant='caption'>
          {moment(sendDate).format('HH:mm')}
        </Text>
      )}

      <div
        className='msg-wrapper'
        style={{
          display: type === 'image' ? 'flex' : undefined,
          justifyContent: type === 'image' ? 'center' : undefined,
          alignItems: type === 'image' ? 'center' : undefined,
          padding: type === 'image' ? '5px' : undefined,
          backgroundColor: theme === 'light' ? '#e1e131' : '#ff0'
        }}
      >
        {type === 'text' && <Text className='msg-text'>{contents}</Text>}
        {type === 'image' && (
          <img
            src={`${backendUrl}${filePath}`}
            alt={uuid.v4()}
            style={{ maxWidth: '100%', cursor: 'pointer' }}
            onClick={() => window.open(`${backendUrl}${filePath}`, type, 'width=auto,height=auto')}
            onLoad={() => setTimeout(setScrollbottom || (() => {}), 100)}
          />
        )}
        {['video', 'audio', 'file'].indexOf(type) > -1 && (
          <div
            className='media-chat-container'
            title={fileName}
            onClick={
              type === 'file' ?
              () => window.open(`${backendUrl}/download/chat/${chatUuid}`) :
              () => window.open(`${backendUrl}${filePath}`, type, 'width=auto,height=auto')
            }
          >
            {type === 'video' && <PlayCircleOutlineIcon className='media-icon'/>}
            {type === 'audio' && <AudiotrackIcon className='media-icon'/>}
            {type === 'file' && <AttachFileIcon className='media-icon'/>}
            <Text className='msg-text media-chat-title'>{fileName}</Text>
          </div>
        )}
      </div>
    </Container>
  );
};

const OtherChat: React.FC<MessageProps> = props => {
  const {
    uuid: chatUuid,
    type,
    profileImage,
    nickname,
    contents,
    filePath,
    showProfile,
    showTime,
    sendDate,
    isMaster,
    setScrollbottom
  } = props;

  const { theme } = useReactiveVar(commonState.getVar());

  const fileName = useMemo(() => {
    if (!filePath) return '';
    let result = getFileBaseName(filePath || '');
    result = result.substring(result.indexOf('_') + 1);
    return result;
  }, [filePath]);

  return (
    <Container style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
      {showProfile && (
        <div className='profile-wrapper'>
          <div className='profile-avatar-wrapper'>
            {isMaster && <CrownIcon className='crown-icon'/>}
            <Avatar src={profileImage}/>
          </div>
          <Text className='other-user-nickname'>{nickname}</Text>
        </div>
      )}

      <div className='other-chat-container'>
        <div
          className='msg-wrapper'
          style={{
            display: type === 'image' ? 'flex' : undefined,
            justifyContent: type === 'image' ? 'center' : undefined,
            alignItems: type === 'image' ? 'center' : undefined,
            padding: type === 'image' ? '5px' : undefined,
            backgroundColor: theme === 'light' ? '#e7e7e7' : '#fff'
          }}
        >
          {type === 'text' && <Text className='msg-text'>{contents}</Text>}
          {type === 'image' && (
            <img
              src={`${backendUrl}${filePath}`}
              alt={uuid.v4()}
              style={{ maxWidth: '100%', cursor: 'pointer' }}
              onClick={() => window.open(`${backendUrl}${filePath}`, type, 'width=auto,height=auto')}
              onLoad={() => setTimeout(setScrollbottom || (() => {}), 100)}
            />
          )}
          {['video', 'audio', 'file'].indexOf(type) > -1 && (
            <div
              className='media-chat-container'
              title={fileName}
              onClick={
                type === 'file' ?
                () => window.open(`${backendUrl}/download/chat/${chatUuid}`) :
                () => window.open(`${backendUrl}${filePath}`, type, 'width=auto,height=auto')
              }
            >
              {type === 'video' && <PlayCircleOutlineIcon className='media-icon'/>}
              {type === 'audio' && <AudiotrackIcon className='media-icon'/>}
              {type === 'file' && <AttachFileIcon className='media-icon'/>}
              <Text className='msg-text media-chat-title'>{fileName}</Text>
            </div>
          )}
        </div>

        {showTime && (
          <Text className='date-text' variant='caption'>
            {moment(sendDate).format('HH:mm')}
          </Text>
        )}
      </div>
    </Container>
  )
};

OtherChat.defaultProps = {
  showProfile: true,
  showTime: true,
  isMaster: false
};

const Message = { Center, MyChat, OtherChat };

export default Message;