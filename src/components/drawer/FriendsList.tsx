import React, { useCallback, useLayoutEffect, useRef, useState } from 'react';
import { Avatar, Button, IconButton, ListItemButton, TextField, Tooltip, Typography } from '@mui/material';
import styled from 'styled-components';
import Text from '@/components/common/Text';
import ReactCrop, { Crop } from 'react-image-crop';
import imageCompression from 'browser-image-compression';
import _ from 'lodash';
import * as uuid from 'uuid';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import AddIcon from '@mui/icons-material/Add';
import PersonAddAltRoundedIcon from '@mui/icons-material/PersonAddAltRounded';
import PersonIcon from '@mui/icons-material/Person';
import DriveFolderUploadRoundedIcon from '@mui/icons-material/DriveFolderUploadRounded';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import DeleteRoundedIcon from '@mui/icons-material/DeleteRounded';
import ErrorRoundedIcon from '@mui/icons-material/ErrorRounded';
import useTranslation from '@/hooks/useTranslation';
import Modal from '@/components/common/Modal';
import getCroppedImg from '@/utils/getCroppedImg';
import Backdrop from '@/components/common/Backdrop';
import { LineProgress } from '@/components/common/Progress';
import validate from '@/utils/validate';
import { CHECK_DUPICATED_USER_INFO, MODIFY_USER_PROFILE } from '@/gql/user';
import { DELETE_FRIEND, FRIEND_REQUEST, FRIEND_RESPONSE, SEARCH_USER_BY_NICKNAME } from '@/gql/friends';
import InfiniteScroll from '@/components/common/InfiniteScroll';
import { GET_PERSONAL_ROOM_INFO } from '@/gql/room';
import { useReactiveVar } from '@apollo/client';
import commonState from '@/gql/vars/common';
import userState, { setUserInfo } from '@/gql/vars/user';
import drawerState, { refreshFriendsList } from '@/gql/vars/drawer';
import { setPageType, setRoomInfo } from '@/gql/vars/mainPage';
import useApollo from '@/hooks/useApollo';

interface InputInfo {
  error: boolean
  helperText: React.ReactNode
}

interface InputInfos {
  nickname: InputInfo
}

const Container = styled.div`
  flex: 1;

  .divider {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 3px 15px;
    background-color: ${({ theme }) => theme === 'light' ? '#1976d2' : '#1c1c1c'};
    user-select: none;

    .divider-title {
      color: #fff !important;
      z-index: 1;
    }

    .divider-icon {
      fill: #fff;
    }
  }

  .user-profile-item-button {
    padding: 0 15px 0 0;

    .user-profile-item {
      display: flex;
      align-items: center;
      width: 100%;
      max-width: 100%;

      .user-profile-img {
        width: 55px;
        height: 55px;
        margin: 15px;
      }
  
      .user-profile-right {
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        gap: 5px;
        max-width: calc(100% - 85px);
  
        .nickname-wrapper {
          .nickname {
            font-size: 18px;
            font-weight: bold;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
          }
        }
  
        .profile-text-wrapper {
          .profile-text {
            font-size: 14px;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
          }
        }
      }
    }
  }

  .profile-modal-body {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 25px;
    width: 100%;
    padding: 15px;

    .profile-img-wrapper {
      position: relative;
      display: flex;
      justify-content: center;
      align-items: flex-end;
      width: 100%;

      .user-profile-img {
        width: 150px;
        height: 150px;
      }

      .edit-profile-btn,
      .delete-friend-btn {
        position: absolute;
        top: -15px;
        right: -15px;
      }

      .reset-profile-img-icon {
        position: absolute;
        width: 25px;
        height: 25px;
        top: 0;
        transform: translate(80px, 0);
        fill: ${({ theme }) => theme === 'light' ? '#3f3f3f' : '#ccc'};
        cursor: pointer;
      }

      .upload-icon {
        position: absolute;
        width: 35px;
        height: 35px;
        bottom: 0;
        transform: translate(50px, 0);
        background-color: #dacc0b;
        cursor: pointer;
      }
    }

    .input-wrapper {
      display: flex;
      gap: 10px;
      width: 100%;

      .success-helper-text {
        color: ${({ theme }) => theme === 'light' ? '#0c9131' : '#0ed145'};
      }
    }

    .profile-modal-input {
      width: 100%;
    }

    .profile-modal-footer {
      width: 100%;
      display: flex;
      justify-content: flex-end;
      gap: 10px;
    }
  }

  .progress-container {
    position: fixed;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    gap: 20px;
    z-index: 30;
  }

  .add-friend-modal-body {
    display: flex;
    flex-direction: column;
    gap: 25px;

    .input-wrapper {
      display: flex;
      gap: 10px;
      width: 100%;
    }

    .search-user-list {
      height: 300px;
      overflow-y: auto;

      .search-user-item {
        display: flex;
        align-items: center;
        gap: 15px;
      }
    }
  }
`;

/** Drawer 친구 목록 리스트 */
const FriendsList = () => {
  const [profileModalOpen, setProfileModalOpen] = useState<boolean>(false);
  const [originSelectedUser, setOriginSelectedUser] = useState<UserInfo | Friend | null>(null);
  const [selectedUser, setSelectedUser] = useState<UserInfo | Friend | null>(null);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const [originProfileImage, setOriginProfileImage] = useState<string>('');
  const [profileImageMimeType, setProfileImageMimeType] = useState<string>('');
  const [compressing, setCompressing] = useState<boolean>(false);
  const [compressProgress, setCompressProgress] = useState<number>(0);
  const [cropModalOpen, setCropModalOpen] = useState<boolean>(false);
  const [crop, setCrop] = useState<Partial<Crop>>({ aspect: 1 });
  const [cropImage, setCropImage] = useState<HTMLImageElement | null>(null);
  const [isValidNickname, setIsValidNickname] = useState<boolean>(false);
  const [showFriendRequestsList, setShowFriendRequestsList] = useState<boolean>(true);
  const [addFriendModalOpen, setAddFriendModalOpen] = useState<boolean>(false);
  const [searchNickname, setSearchNickname] = useState<string>('');
  const [originSearchNickname, setOriginSearchNickname] = useState<string>('');
  const [searchUserList, setSearchUserList] = useState<Friend[] | null>(null);
  const [deleteFriendModalOpen, setDeleteFriendModalOpen] = useState<boolean>(false);
  const [profileTooltip, setProfileTooltip] = useState<'profileImage' | 'modifyProfile' | 'deleteFriend'>('profileImage');
  const [inputInfo, setInputInfo] = useState<InputInfos>({
    nickname: { error: false, helperText: '' }
  });

  const { theme } = useReactiveVar(commonState.getVar());
  const { userInfo, token } = useReactiveVar(userState.getVar());
  const { friendsRequest, friendsList } = useReactiveVar(drawerState.getVar());

  const _profile_img_file = useRef<HTMLInputElement>(null);

  const t = useTranslation();
  const apollo = useApollo();

  /** 사용자 프로필 아이템 클릭 이벤트 */
  const handleClickProfile = useCallback((profileInfo: UserInfo | Friend) => {
    setSelectedUser(profileInfo);
    setOriginSelectedUser(profileInfo);
    setIsEditMode(false);
    setProfileModalOpen(true);
  }, []);

  /** 사용자 프로필 모달 이미지, 닉네임, 상태메시지 변경 이벤트 */
  const handleChangeProfileData = useCallback((key: 'profileImage' | 'nickname' | 'profileText', value: string) => {
    if (!selectedUser) return;
    setSelectedUser({ ...selectedUser, [key]: value });

    if (key !== 'nickname') return;
    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.nickname = { error: false, helperText: '' };
    setInputInfo(inputInfoClone);
  }, [selectedUser, inputInfo]);

  /** 프로필 이미지 파일 변경 이벤트 */
  const handleChangeProfileImage: React.ChangeEventHandler<HTMLInputElement> = useCallback(async e => {
    if (!e.target.files || e.target.files.length === 0 || e.target.files[0].type.indexOf('image') < 0) {
      setOriginProfileImage('');
      setProfileImageMimeType('');
      setCrop({ aspect: 1 });
      return;
    }

    const file = e.target.files[0];

    console.log(`0. Origin Image Size: ${file.size} Byte`);
    console.log('1. Compress Start');
    setCompressProgress(0);
    setCompressing(true);
    const compressedFile = await imageCompression(file, {
      maxWidthOrHeight: 400,
      fileType: 'image/jpeg',
      maxSizeMB: 5,
      onProgress: progress => {
        console.log(progress);
        setCompressProgress(progress);
        if (progress === 100) setTimeout(() => {
          setCompressing(false)
        }, 500);
      }
    });
    console.log('2. Get Base64 Image');
    const compressedImage = await imageCompression.getDataUrlFromFile(compressedFile);
    console.log(`3. Compressed Image Size: ${compressedImage.length} Byte`);

    setCrop({ aspect: 1 });
    setOriginProfileImage(compressedImage);
    setCropModalOpen(true);
  }, []);

  /** 닉네임 중복 체크 */
  const checkValidNickname = useCallback(async () => {
    if (!selectedUser) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    const { nickname } = selectedUser;

    if (!validate('nickname', nickname)) {
      inputInfoClone.nickname = { error: true, helperText: 'pleaseEnterCorrectNickname' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.query<{ checkDuplicatedUserInfo: CheckDuplicatedResult }, CheckDuplicatedArgs>({
        query: CHECK_DUPICATED_USER_INFO,
        variables: { checkType: 'NICKNAME', value: nickname }
      });

      if (data.checkDuplicatedUserInfo.errCode) {
        throw data.checkDuplicatedUserInfo.errCode;
      }

      if (data.checkDuplicatedUserInfo.result) {
        inputInfoClone.nickname = { error: true, helperText: 'duplicatedNickname' };
        return setInputInfo(inputInfoClone);
      }

      inputInfoClone.nickname = {
        error: false,
        helperText: <span className='success-helper-text'>{t('usableNickname')}</span>
      };

      setInputInfo(inputInfoClone);
      setIsValidNickname(true);
    } catch (err) {
      console.error(err);
    }
  }, [selectedUser, inputInfo, t, apollo]);

  /** 사용자 프로필 모달 닫기 이벤트 */
  const handleCloseProfileModal = useCallback(() => {
    setInputInfo({ nickname: { error: false, helperText: '' } });
    setIsValidNickname(false);
    setIsEditMode(false);
    setProfileModalOpen(false);
  }, []);

  /** 내 프로필 수정 취소 버튼 클릭 이벤트 */
  const handleClickEditProfileCancelButton = useCallback(() => {
    setSelectedUser(originSelectedUser);
    setInputInfo({ nickname: { error: false, helperText: '' } });
    setIsValidNickname(false);
    setIsEditMode(false);
  }, [originSelectedUser]);

  /** 내 프로필 수정 확인 버튼 클릭 이벤트 */
  const handleClickEditProfileConfirmButton = useCallback(async () => {
    if (!userInfo || !selectedUser) return;

    if (
      userInfo.nickname === selectedUser.nickname &&
      userInfo.profileImage === selectedUser.profileImage &&
      userInfo.profileText === selectedUser.profileText
    ) {
      return handleClickEditProfileCancelButton();
    }

    const inputInfoClone = _.cloneDeep(inputInfo);

    if (userInfo.nickname !== selectedUser.nickname && !isValidNickname) {
      inputInfoClone.nickname = { error: true, helperText: 'pleaseCheckNicknameDuplication' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data = await apollo.mutate<{ modifyUserProfile: ModifyUserProfileResult }, ModifyUserProfileArgs>({
        mutation: MODIFY_USER_PROFILE,
        variables: {
          nickname: selectedUser.nickname,
          profileImage: selectedUser.profileImage,
          profileText: selectedUser.profileText
        }
      });

      if (data.modifyUserProfile.errCode) throw data.modifyUserProfile.errCode;

      const { result } = data.modifyUserProfile;

      setUserInfo(result);
      setSelectedUser(result);
      setOriginSelectedUser(result);
      setInputInfo({ nickname: { error: false, helperText: '' } });
      setIsValidNickname(false);
      setIsEditMode(false);
    } catch (err) {
      console.error(err);
    }
  }, [userInfo, selectedUser, isValidNickname, inputInfo, apollo, handleClickEditProfileCancelButton]);

  /** 프로필 이미지 편집 모달 확인 */
  const handleConfirmProfileImageCropModal = useCallback(() => {
    const profileImage = getCroppedImg(cropImage, crop, profileImageMimeType);
    console.log(crop);
    if (!profileImage) return;

    if (_profile_img_file.current) {
      _profile_img_file.current.value = '';
    }

    handleChangeProfileData('profileImage', profileImage);
    setCropModalOpen(false);
    setOriginProfileImage('');
  }, [crop, cropImage, profileImageMimeType, handleChangeProfileData]);

  /** 프로필 이미지 편집 모달 닫기 */
  const handleCloseProfileImageCropModal = useCallback(() => {
    if (_profile_img_file.current) {
      _profile_img_file.current.value = '';
    }

    setCropModalOpen(false);
    setOriginProfileImage('');
  }, []);

  /** 친구 추가 버튼 클릭 이벤트 */
  const handleClickAddFriendButton = useCallback(() => {
    setSearchNickname('');
    setOriginSearchNickname('');
    setSearchUserList(null);
    setAddFriendModalOpen(true);
  }, []);

  /** 친구 추가 모달 닫기 */
  const handleCloseAddFriendModal = useCallback(() => {
    setAddFriendModalOpen(false);
  }, []);

  /** 검색 버튼 클릭 */
  const handleClickSearchButton: React.MouseEventHandler<HTMLButtonElement> = useCallback(async e => {
    e.preventDefault();
    if (!searchNickname) return;

    try {
      const data = await apollo.query<{ searchUserByNickname: SearchUserByNicknameResult }, SearchUserByNicknameArgs>({
        query: SEARCH_USER_BY_NICKNAME,
        variables: { nickname: searchNickname, lastNickname: '' }
      });

      const { result, errCode } = data.searchUserByNickname;

      if (errCode) throw errCode;

      setSearchUserList(result);
      setOriginSearchNickname(searchNickname);
      console.log(result);
    } catch (err) {
      console.error(err);
    }
  }, [searchNickname, apollo]);

  /** 친구 요청 버튼 클릭 이벤트 */
  const handleClickFriendRequestButton = useCallback(async () => {
    if (!userInfo || !selectedUser || !selectedUser.uuid || !searchUserList) return;

    try {
      const data = await apollo.mutate<{ friendRequest: FriendRequestResult }, FriendRequestArgs>({
        mutation: FRIEND_REQUEST,
        variables: { requestUserUuid: selectedUser.uuid }
      });

      const { result, errCode } = data.friendRequest;
      if (!result && !!errCode) throw errCode;

      const searchUserListClone = _.cloneDeep(searchUserList);
      _.remove(searchUserListClone, { uuid: selectedUser.uuid });
      setSearchUserList(searchUserListClone);

      handleCloseProfileModal();
    } catch (err) {
      console.error(err);
    }
  }, [userInfo, selectedUser, searchUserList, apollo, handleCloseProfileModal]);

  /** 친구 수락/거절 버튼 클릭 이벤트 */
  const handleFriendResponse = useCallback(async (isAccept: boolean) => {
    if (!selectedUser || !selectedUser.uuid) return;

    try {
      const data = await apollo.mutate<{ friendResponse: FriendResponseResult }, FriendResponseArgs>({
        mutation: FRIEND_RESPONSE,
        variables: { requestUserUuid: selectedUser.uuid, isAccept }
      });

      const { result, errCode } = data.friendResponse;
      if (!result && !!errCode) throw errCode;

      refreshFriendsList();
      handleCloseProfileModal();
    } catch (err) {
      console.error(err);
    }
  }, [selectedUser, apollo, handleCloseProfileModal]);

  /** 친구 삭제 버튼 클릭 */
  const handleClickDeleteFriendButton = useCallback(() => {
    if (!token || !selectedUser || !selectedUser.uuid) return;
    setDeleteFriendModalOpen(true);
  }, [token, selectedUser]);

  /** 친구 삭제 모달 삭제 버튼 클릭 이벤트 */
  const handleClickDeleteFriendConfirmButton = useCallback(async () => {
    if (!selectedUser || !selectedUser.uuid) return;

    try {
      const data = await apollo.mutate<{ deleteFriend: DeleteFriendResult }, DeleteFriendArgs>({
        mutation: DELETE_FRIEND,
        variables: { friendUuid: selectedUser.uuid }
      });

      const { result, errCode }  = data.deleteFriend;
      if (!result && !!errCode) throw errCode;

      setDeleteFriendModalOpen(false);
      handleCloseProfileModal();
      refreshFriendsList();
    } catch (err) {
      console.error(err);
    }
  }, [selectedUser, apollo, handleCloseProfileModal]);

  /** 친구 검색 리스트 Scroll End 이벤트  */
  const handleSearchUserScrollEnd = useCallback(async () => {
    if (!searchUserList) return;

    try {
      const data = await apollo.query<{ searchUserByNickname: SearchUserByNicknameResult }, SearchUserByNicknameArgs>({
        query: SEARCH_USER_BY_NICKNAME,
        variables: {
          nickname: originSearchNickname,
          lastNickname: searchUserList[searchUserList.length - 1].nickname
        }
      });

      const { result, errCode } = data.searchUserByNickname;

      if (errCode) throw errCode;

      if (result.length === 0) return;

      setSearchUserList([...searchUserList, ...result]);
      console.log(result);
    } catch (err) {
      console.error(err);
    }
  }, [originSearchNickname, searchUserList, apollo]);

  /** 채팅하기 버튼 클릭 이벤트 */
  const handleClickChatButton = useCallback(async () => {
    if (!selectedUser || !selectedUser.uuid) return;

    try {
      const data = await apollo.query<{ getPersonalRoomInfo: GetPersonalRoomInfoResult }, GetPersonalRoomInfoArgs>({
        query: GET_PERSONAL_ROOM_INFO,
        variables: { userUuid: selectedUser.uuid }
      });

      const { result, errCode } = data.getPersonalRoomInfo;

      if (errCode) throw errCode;

      console.log(result);

      setPageType('personalchat');
      setRoomInfo(result);
      handleCloseProfileModal();
    } catch (err) {
      console.error(err);
    }
  }, [selectedUser, apollo, handleCloseProfileModal]);

  /** 친구 목록 유저 프로필 생성 */
  const createUserProfileItem = useCallback((userInfo: UserInfo | Friend, key?: string) => (
    <ListItemButton key={key} className='user-profile-item-button' onClick={() => handleClickProfile(userInfo)}>
      <div className='user-profile-item'>
        <Avatar
          className='user-profile-img'
          alt={userInfo.nickname}
          src={userInfo.profileImage}
        />

        <div className='user-profile-right'>
          <div className='nickname-wrapper'>
            <Text className='nickname'>{userInfo.nickname}</Text>
          </div>
          <div className='profile-text-wrapper'>
            <Text className='profile-text'>{userInfo.profileText}</Text>
          </div>
        </div>
      </div>
    </ListItemButton>
  ), [handleClickProfile]);

  /** 사파리 position: fixed, overflow: hidden 잘림 현상 조치 */
  useLayoutEffect(() => {
    const drawerItems = window.document.getElementById('drawer-items');
    if (!drawerItems) return;

    if (profileModalOpen || cropModalOpen || addFriendModalOpen || deleteFriendModalOpen) {
      drawerItems.style.overflow = 'hidden';
      return;
    }

    const timeout = setTimeout(() => drawerItems.removeAttribute('style'), 200);

    return () => {
      clearTimeout(timeout);
    };
  }, [profileModalOpen, cropModalOpen, addFriendModalOpen, deleteFriendModalOpen]);

  if (!userInfo) return null;

  return (
    <Container theme={theme}>
      {/* 내 프로필 */}
      <div className='divider'>
        <Text className='divider-title'>{t('myProfile')}</Text>
      </div>
      {createUserProfileItem(userInfo)}

      {/* 친구 요청 */}
      {friendsRequest.length > 0 && (
        <>
          <div
            className='divider'
            onClick={() => setShowFriendRequestsList(!showFriendRequestsList)}
            style={{ justifyContent: 'flex-start', cursor: 'pointer' }}
          >
            {showFriendRequestsList ? (
              <ArrowDropDownIcon className='divider-icon'/>
            ) : (
              <ArrowRightIcon className='divider-icon'/>
            )}
            <Text className='divider-title'>{`${t('friendRequests')} (${friendsRequest.length})`}</Text>
          </div>
          {showFriendRequestsList && friendsRequest.map(d => createUserProfileItem(d, uuid.v4()))}
        </>
      )}

      {/* 친구 */}
      <div className='divider'>
        <Text className='divider-title'>{`${t('friends')} (${friendsList.length})`}</Text>
        <IconButton style={{ padding: '2px' }} onClick={handleClickAddFriendButton}>
          <AddIcon className='divider-icon' style={{ width: '20px', height: '20px' }}/>
        </IconButton>
      </div>
      {friendsList.map(d => createUserProfileItem(d, uuid.v4()))}

      {/* 친구 찾기 모달 */}
      <Modal
        open={addFriendModalOpen}
        type='no-footer'
        title={(
          <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <PersonAddAltRoundedIcon style={{ fill: theme === 'light' ? '#000' : '#fff' }}/>
            <Text variant='h6'>{t('searchFriends')}</Text>
          </div>
        )}
        onClose={handleCloseAddFriendModal}
      >
        <div className='add-friend-modal-body'>
          <form className='input-wrapper' action='#'>
            <TextField
              label={t('nickname')}
              value={searchNickname}
              onChange={e => setSearchNickname(e.target.value)}
              style={{ flex: 1 }}
            />
            <Button
              variant='contained'
              type='submit'
              onClick={handleClickSearchButton}
            >
              {t('search')}
            </Button>
          </form>

          {!searchUserList && (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '300px' }}>
              <Text>{t('pleaseSearchFriends')}</Text>
            </div>
          )}

          {!!searchUserList && (searchUserList.length > 0 ? (
            <InfiniteScroll className='search-user-list' onScrollEnd={handleSearchUserScrollEnd}>
              {searchUserList.map(d => (
                <ListItemButton
                  key={uuid.v4()}
                  className='search-user-item'
                  onClick={() => handleClickProfile(d)}
                >
                  <Avatar src={d.profileImage}/>
                  <Text>{d.nickname}</Text>
                </ListItemButton>
              ))}
            </InfiniteScroll>
          ) : (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '300px' }}>
              <Text>{t('noSearchUser')}</Text>
            </div>
          ))}
        </div>
      </Modal>

      {/* 사용자 프로필 모달 */}
      <Modal
        open={profileModalOpen}
        type='no-footer'
        title={(
          <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <PersonIcon style={{ fill: theme === 'light' ? '#000' : '#fff' }}/>
            {!!originSelectedUser && <Text variant='h6'>{`${t('profileOf').replace('{{nickname}}', originSelectedUser.nickname)}`}</Text>}
          </div>
        )}
        onClose={handleCloseProfileModal}
      >
        <div className='profile-modal-body'>
          {/* 프로필 이미지 */}
          <Tooltip title={t(profileTooltip)} followCursor>
            <div className='profile-img-wrapper'>
              {isEditMode && (
                <input
                  ref={_profile_img_file}
                  id='profile-img'
                  type='file'
                  accept='.jpg, .png'
                  onChange={handleChangeProfileImage}
                  style={{ display: 'none' }}
                />
              )}
              {!!selectedUser && (
                <>
                  <label htmlFor='profile-img'>
                    <Avatar
                      className='user-profile-img'
                      alt={selectedUser.nickname}
                      src={selectedUser.profileImage}
                      style={{ cursor: isEditMode ? 'pointer' : undefined }}
                    />
                  </label>

                  {/* 내 프로필 수정하기 버튼 */}
                  {userInfo.nickname === selectedUser.nickname && !isEditMode && (
                    <IconButton
                      className='edit-profile-btn'
                      onMouseOver={() => profileTooltip === 'profileImage' ? setProfileTooltip('modifyProfile') : null}
                      onMouseLeave={() => profileTooltip === 'modifyProfile' ? setProfileTooltip('profileImage') : null}
                      onClick={() => {
                        setProfileTooltip('profileImage');
                        setIsEditMode(true);
                      }}
                    >
                      <ModeEditIcon/>
                    </IconButton>
                  )}

                  {/* 친구삭제 버튼 */}
                  {
                    !!userInfo &&
                    !!selectedUser.uuid &&
                    userInfo.friends.indexOf(selectedUser.uuid) > -1 && (
                      <IconButton
                        className='delete-friend-btn'
                        onMouseOver={() => profileTooltip === 'profileImage' ? setProfileTooltip('deleteFriend') : null}
                        onMouseLeave={() => profileTooltip === 'deleteFriend' ? setProfileTooltip('profileImage') : null}
                        onClick={handleClickDeleteFriendButton}
                      >
                        <DeleteRoundedIcon/>
                      </IconButton>
                    )
                  }
                </>
              )}

              {isEditMode && !!selectedUser && (
                <>
                  <CloseRoundedIcon
                    className='reset-profile-img-icon'
                    onClick={() => setSelectedUser({ ...selectedUser, profileImage: '' })}
                  />

                  <Avatar className='upload-icon'>
                    <label htmlFor='profile-img' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', cursor: 'pointer' }}>
                      <DriveFolderUploadRoundedIcon style={{ fill: '#000', marginLeft: '1px' }}/>
                    </label>
                  </Avatar>
                </>
              )}
            </div>
          </Tooltip>

          {!!userInfo && !!selectedUser && (
            <>
              {/* 닉네임 */}
              <div className='input-wrapper'>
                <TextField
                  label={t('nickname')}
                  value={selectedUser.nickname}
                  error={inputInfo.nickname.error}
                  helperText={typeof inputInfo.nickname.helperText === 'string' ? t(inputInfo.nickname.helperText) : inputInfo.nickname.helperText}
                  focused={isEditMode ? undefined : false}
                  InputProps={{ readOnly: !isEditMode }}
                  disabled={isValidNickname}
                  onChange={e => handleChangeProfileData('nickname', e.target.value)}
                  style={{ flex: 1 }}
                />

                {isEditMode && (
                  <Button
                    variant='contained'
                    disabled={isValidNickname || userInfo.nickname === selectedUser.nickname}
                    onClick={checkValidNickname}
                    style={{ height: '56px' }}
                  >
                    {t('duplicateCheck')}
                  </Button>
                )}
              </div>

              {/* 상태 메시지 */}
              <TextField
                className='profile-modal-input'
                label={t('profileText')}
                value={selectedUser.profileText}
                multiline
                maxRows={5}
                focused={isEditMode ? undefined : false}
                InputProps={{ readOnly: !isEditMode }}
                onChange={e => handleChangeProfileData('profileText', e.target.value)}
              />
            </>
          )}

          {/* 프로필 수정 하단 버튼 그룹 */}
          {isEditMode && (
            <div className='profile-modal-footer'>
              <Button
                variant='contained'
                onClick={handleClickEditProfileCancelButton}
                style={{ backgroundColor: theme === 'light' ? '#db3939' : '#ff6969' }}
              >
                {t('cancel')}
              </Button>
              <Button
                variant='contained'
                onClick={handleClickEditProfileConfirmButton}
              >
                {t('confirm')}
              </Button>
            </div>
          )}

          {/* 친구 찾기 유저 프로필 하단 버튼 */}
          {
            !!userInfo &&
            !!selectedUser &&
            !!selectedUser.uuid &&
            userInfo.uuid !== selectedUser.uuid &&
            userInfo.friends.indexOf(selectedUser.uuid) < 0 &&
            friendsRequest.map(d => d.uuid).indexOf(selectedUser.uuid) < 0 && (
              <div className='profile-modal-footer'>
                {/* 친구 요청 버튼 */}
                <Button variant='contained' onClick={handleClickFriendRequestButton}>
                  {t('friendRequests')}
                </Button>
              </div>
            )
          }

          {/* 친구 요청 프로필 하단 버튼 그룹 */}
          {
            !!selectedUser &&
            !!selectedUser.uuid &&
            friendsRequest.map(d => d.uuid).indexOf(selectedUser.uuid) > -1 && (
              <div className='profile-modal-footer'>
                {/* 친구 요청 거절 버튼 */}
                <Button
                  variant='contained'
                  onClick={() => handleFriendResponse(false)}
                  style={{ backgroundColor: theme === 'light' ? '#db3939' : '#ff6969' }}
                >
                  {t('friendDeny')}
                </Button>

                {/* 친구 요청 수락 버튼 */}
                <Button
                  variant='contained'
                  onClick={() => handleFriendResponse(true)}
                >
                  {t('friendAccept')}
                </Button>
              </div>
            )
          }

          {/* 친구 프로필 하단 버튼 그룹 */}
          {
            !!userInfo &&
            !!selectedUser &&
            !!selectedUser.uuid &&
            userInfo.friends.indexOf(selectedUser.uuid) > -1 && (
              <>
                {/* 채팅하기 버튼 */}
                <Button
                  variant='contained'
                  style={{ width: '100%' }}
                  onClick={handleClickChatButton}
                >
                  {t('chatting')}
                </Button>
              </>
            )
          }
        </div>
      </Modal>

      {/* 친구 삭제 확인 모달 */}
      {!!selectedUser && !!selectedUser.uuid && (
        <Modal
          open={deleteFriendModalOpen}
          type='no-footer'
          title={(
            <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
              <ErrorRoundedIcon style={{ fill: theme === 'light' ? '#db3939' : '#ff6969' }}/>
              <Text variant='h6'>{t('deleteFriend')}</Text>
            </div>
          )}
          onClose={() => setDeleteFriendModalOpen(false)}
        >
          <div style={{ margin: '20px 0' }}>
            <Text>{`${t('deleteFriendConfirmMessage').replace('{{nickname}}', selectedUser.nickname)}`}</Text>
          </div>

          <div style={{ display: 'flex', justifyContent: 'flex-end', gap: '10px' }}>
            <Button
              variant='contained'
              onClick={() => setDeleteFriendModalOpen(false)}
              style={{ backgroundColor: '#999' }}
            >
              {t('cancel')}
            </Button>
            <Button
              variant='contained'
              onClick={handleClickDeleteFriendConfirmButton}
              style={{ backgroundColor: theme === 'light' ? '#db3939' : '#ff6969' }}
            >
              {t('delete')}
            </Button>
          </div>
        </Modal>
      )}

      {/* 이미지 크롭 모달 */}
      <Modal
        open={cropModalOpen}
        type='alert'
        title={t('editProfileImage')}
        center
        onConfirm={handleConfirmProfileImageCropModal}
        onClose={handleCloseProfileImageCropModal}
      >
        {originProfileImage && (
          <ReactCrop
            src={originProfileImage}
            crop={crop}
            onImageLoaded={image => setCropImage(image)}
            circularCrop
            onChange={newCrop => setCrop(newCrop)}
          />
        )}
      </Modal>

      {/* 이미지 압축 진행률 */}
      {compressing && (
        <div className='progress-container'>
          <Backdrop/>

          <div style={{ width: '400px', maxWidth: '100%' }}>
            <LineProgress
              progress={compressProgress}
              showPercentage
            />
          </div>

          <Typography
            color='#fff'
            style={{ zIndex: 10 }}
          >
            {t('compressing')}
          </Typography>
        </div>
      )}
    </Container>
  );
};

export default FriendsList;