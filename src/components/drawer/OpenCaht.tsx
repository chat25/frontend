import { useCallback, useLayoutEffect, useState } from 'react';
import { useReactiveVar } from '@apollo/client';
import styled from 'styled-components';
import { Avatar, Button, FormControlLabel, IconButton, ListItemButton, Slider, Switch, TextField, Tooltip } from '@mui/material';
import moment from 'moment';
import _ from 'lodash';
import AddIcon from '@mui/icons-material/Add';
import LockIcon from '@mui/icons-material/Lock';
import useTranslation from '@/hooks/useTranslation';
import commonState from '@/gql/vars/common';
import Text from '@/components/common/Text';
import Modal from '@/components/common/Modal';
import userState from '@/gql/vars/user';
import { CREATE_OPEN_CHAT_ROOM, ENTER_OPEN_CHAT_ROOM, GET_ROOM_INFO } from '@/gql/room';
import { setPageType, setRoomInfo } from '@/gql/vars/mainPage';
import drawerState, { getOpenChatRoomList, setOpenChatRoomName, setOriginOpenChatRoomName } from '@/gql/vars/drawer';
import useApollo from '@/hooks/useApollo';

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  height: 100%;

  .divider {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 3px 15px;
    background-color: ${({ theme }) => theme === 'light' ? '#1976d2' : '#1c1c1c'};
    user-select: none;

    .divider-title {
      color: #fff !important;
      z-index: 1;
    }

    .divider-icon {
      fill: #fff;
    }
  }

  .search-wrapper {
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 10px;
    padding: 10px;

    .search-text-input {
      flex: 1;
    }
  }

  .no-room {
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: center;
    user-select: none;
  }

  .create-chat-room-modal-body {
    display: flex;
    flex-direction: column;
    gap: 25px;
    padding: 10px;

    .input-wrapper {
      display: 'flex';
      flex-direction: column;
    }
  }

  .chat-room-item-btn {
    padding: 0 15px 0 0;

    .chat-room-item {
      display: flex;
      align-items: center;
      width: 100%;
      max-width: 100%;
    }

    .chat-room-img-group {
      display: flex;
      flex-direction: column;
      justify-content: center;
      width: 55px;
      height: 55px;
      margin: 15px;
      gap: 3px;

      .chat-room-img {
        width: 55px;
        height: 55px;
      }

      .chat-room-img-group-row {
        display: flex;
        justify-content: center;
        gap: 3px;

        .chat-room-img-small {
          width: 24.5px;
          height: 24.5px;
        }
      }
    }

    .chat-room-right {
      flex: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
      gap: 5px;
      max-width: calc(100% - 85px);

      .room-name-wrapper {
        .room-name {
          font-size: 18px;
          font-weight: bold;
          white-space: nowrap;
          text-overflow: ellipsis;
          overflow: hidden;
        }
      }
    }
  }
`;

const OpenCaht = () => {
  const [createChatRoomOpen, setCreateChatRoomOpen] = useState<boolean>(false);
  const [newRoomName, setNewRoomName] = useState<string>('');
  const [maxUser, setMaxUser] = useState<number>(10);
  const [newRoomPassword, setNewRoomPassword] = useState<string>('');
  const [isPrivate, setIsPrivate] = useState<boolean>(false);
  const [enterRoomModalOpen, setEnterRoomModalOpen] = useState<boolean>(false);
  const [selectedRoom, setSelectedRoom] = useState<RoomListItem | null>(null);
  const [enterRoomPassword, setEnterRoomPassword] = useState<string>('');
  const [maxUserErrorModalOpen, setMaxUserErrorModalOpen] = useState<boolean>(false);
  const [inputInfo, setInputInfo] = useState({
    newRoomName: { error: false, helperText: '' },
    newRoomPassword: { error: false, helperText: 'pleaseEnterPasswordToUse' },
    enterRoomPassword: { error: false, helperText: '' }
  });

  const { theme } = useReactiveVar(commonState.getVar());
  const { userInfo } = useReactiveVar(userState.getVar());
  const {
    openChatRoomName,
    originOpenChatRoomName,
    searchOpenChatRoomList
  } = useReactiveVar(drawerState.getVar());

  const t = useTranslation();
  const apollo = useApollo();

  /** 오픈 채팅방 생성 버튼 클릭 이벤트 */
  const handleClickCreateChatRoomButton = useCallback(() => {
    setCreateChatRoomOpen(true);
    setNewRoomName('');
    setMaxUser(10);
    setNewRoomPassword('');
    setIsPrivate(false);
  }, []);

  /** 오픈 채팅방 생성 모달 채팅방 이름 변경 이벤트 */
  const handleChangeNewRoomName = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    if (inputInfo.newRoomName.error) {
      const inputInfoClone = _.cloneDeep(inputInfo);
      inputInfoClone.newRoomName = { error: false, helperText: '' };
      setInputInfo(inputInfoClone);
    }
    setNewRoomName(e.target.value);
  }, [inputInfo]);

  /** 오픈 채팅방 생성 모달 확인 버튼 클릭 이벤트 */
  const handleClickCreateChatRoomConfirmButton = useCallback(async () => {
    const inputInfoClone = _.cloneDeep(inputInfo);

    if (!newRoomName) {
      inputInfoClone.newRoomName = { error: true, helperText: 'pleaseEnterRoomName' };
      return setInputInfo(inputInfoClone);
    }

    try {
      const data1 = await apollo.mutate<{ createRoom: CreateRoomResult }, Partial<CreateRoomArgs>>({
        mutation: CREATE_OPEN_CHAT_ROOM,
        variables: {
          name: newRoomName,
          maxUser,
          password: newRoomPassword,
          isPrivate
        }
      });

      const { result: newRoomUuid, errCode: errCode1 } = data1.createRoom;

      if (errCode1) {
        if (errCode1 === 'DUPLICATED_ROOM_NAME') {
          inputInfoClone.newRoomName = { error: true, helperText: 'duplicatedOpenChatRoomName' };
          return setInputInfo(inputInfoClone);
        }
        throw errCode1;
      }

      const data2 = await apollo.query<{ getRoomInfo: GetRoomInfoResult }, GetRoomInfoArgs>({
        query: GET_ROOM_INFO,
        variables: { roomUuid: newRoomUuid }
      });

      const { result, errCode: errCode2 } = data2.getRoomInfo;

      if (errCode2) throw errCode2;

      console.log(result);
      
      setCreateChatRoomOpen(false);
      setPageType(`${result.type}chat`);
      setRoomInfo(result);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, newRoomName, maxUser, newRoomPassword, isPrivate, inputInfo]);

  /** 오픈 채팅방 검색 버튼 클릭 이벤트 */
  const handleClickSearchOpenRoomsButton: React.FormEventHandler<HTMLFormElement> = useCallback(async e => {
    e.preventDefault();
    getOpenChatRoomList(openChatRoomName);
    setOriginOpenChatRoomName(openChatRoomName);
  }, [openChatRoomName]);

  /** 오픈 채팅방 목록 아이템 클릭 이벤트 */
  const handleClickRoomListItem = useCallback((roomListItem: RoomListItem) => {
    setSelectedRoom(roomListItem);
    setEnterRoomModalOpen(true);
    setEnterRoomPassword('');
  }, []);

  /** 오픈 채팅방 참가 모달 확인 버튼 클릭 이벤트 */
  const handleClickEnterRoomModalConfirmButton = useCallback(async () => {
    if (!selectedRoom) return;
    const { uuid: roomUuid } = selectedRoom;
    const userUuid = userInfo?.uuid;
    if (!userUuid) return;

    try {
      const data1 = await apollo.mutate<{ enterRoom: EnterRoomResult }, EnterRoomArgs>({
        mutation: ENTER_OPEN_CHAT_ROOM,
        variables: { roomUuid, userUuid: [userUuid], password: enterRoomPassword }
      });

      const { errCode: errCode1 } = data1.enterRoom;

      if (errCode1) {
        if (errCode1 === 'MAX_USER') return setMaxUserErrorModalOpen(true);
        if (errCode1 === 'WRONG_PASSWORD') {
          const inputInfoClone = _.cloneDeep(inputInfo);
          inputInfoClone.enterRoomPassword = { error: true, helperText: 'wrongPassword' };
          return setInputInfo(inputInfoClone);
        }
        getOpenChatRoomList(originOpenChatRoomName);
        throw errCode1;
      }

      const data2 = await apollo.query<{ getRoomInfo: GetRoomInfoResult }, GetRoomInfoArgs>({
        query: GET_ROOM_INFO,
        variables: { roomUuid }
      });

      const { result, errCode: errCode2 } = data2.getRoomInfo;

      if (errCode2) throw errCode2;

      console.log(data2.getRoomInfo);

      setPageType(`${result.type}chat`);
      setRoomInfo(result);
      setEnterRoomModalOpen(false);
      setMaxUserErrorModalOpen(false);
      getOpenChatRoomList(originOpenChatRoomName);
    } catch (err) {
      console.error(err);
    }
  }, [userInfo, originOpenChatRoomName, apollo, selectedRoom, enterRoomPassword, inputInfo]);

  /** 채팅방 아이템 생성 */
  const createRoomInfoItem = useCallback((roomListItem: RoomListItem, key?: string) => (
    <Tooltip
      key={key}
      title={`${t('lastMessageDate')}: ${moment(roomListItem.lastChat.sendDate).format('YYYY-MM-DD HH:mm')}`}
      placement='right'
      arrow
    >
      <ListItemButton className='chat-room-item-btn' onClick={() => handleClickRoomListItem(roomListItem)}>
        <div className='chat-room-item'>
          <div className='chat-room-img-group'>
            {/* 오픈 채팅방 1인 */}
            {roomListItem.type === 'open' && roomListItem.users.length === 1 && (
              <Avatar
                className='chat-room-img'
                src={roomListItem.users[0].profileImage}
              />
            )}

            {/* 오픈 채팅방 2인 */}
            {roomListItem.type !== 'personal' && roomListItem.users.length === 2 && (
              <div style={{ flexDirection: 'column' }}>
                <div
                  className='chat-room-img-group-row'
                  style={{ justifyContent: 'flex-start' }}
                >
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[0].profileImage}
                  />
                </div>

                <div
                  className='chat-room-img-group-row'
                  style={{ justifyContent: 'flex-end' }}
                >
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[1].profileImage}
                  />
                </div>
              </div>
            )}

            {/* 오픈 채팅방 3인 이상 */}
            {roomListItem.type !== 'personal' && roomListItem.users.length > 2 && (
              <div style={{ flexDirection: 'column' }}>
                <div className='chat-room-img-group-row'>
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[0].profileImage}
                  />
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[1].profileImage}
                  />
                </div>

                <div className='chat-room-img-group-row'>
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[2].profileImage}
                  />
                  {!!roomListItem.users[3] && (
                    <Avatar
                      className='chat-room-img-small'
                      src={roomListItem.users[3].profileImage}
                    />
                  )}
                </div>
              </div>
            )}
          </div>

          {/* 채팅방 이름, 마지막 메시지 */}
          <div className='chat-room-right'>
            <div className='room-name-wrapper'>
              <Text className='room-name'>
                {roomListItem.name}
              </Text>
            </div>
            <div style={{ display: 'flex', alignItems: 'center', gap: '5px' }}>
              {!!roomListItem.password && (
                <LockIcon style={{ width: '15px', height: '15px', fill: theme === 'light' ? '#3f3f3f' : '#ccc' }}/>
              )}
              <Text variant='body2'>
                ({roomListItem.userNum}/{roomListItem.maxUser})
              </Text>
            </div>
          </div>
        </div>
      </ListItemButton>
    </Tooltip>
  ), [theme, t, handleClickRoomListItem]);

  /** 사파리 position: fixed, overflow: hidden 잘림 현상 조치 */
  useLayoutEffect(() => {
    const drawerItems = window.document.getElementById('drawer-items');
    if (!drawerItems) return;

    if (createChatRoomOpen || enterRoomModalOpen || maxUserErrorModalOpen) {
      drawerItems.style.overflow = 'hidden';
      return;
    }

    const timeout = setTimeout(() => drawerItems.removeAttribute('style'), 200);

    return () => {
      clearTimeout(timeout);
    };
  }, [createChatRoomOpen, enterRoomModalOpen, maxUserErrorModalOpen]);

  return (
    <Container theme={theme}>
      {/* 오픈 채팅방 */}
      <div className='divider'>
        <Text className='divider-title'>{t('openChatRoom')}</Text>
        <IconButton style={{ padding: '2px' }} onClick={handleClickCreateChatRoomButton}>
          <AddIcon className='divider-icon' style={{ width: '20px', height: '20px' }}/>
        </IconButton>
      </div>

      {/* 채팅방 검색 */}
      <form className='search-wrapper' onSubmit={handleClickSearchOpenRoomsButton}>
        <TextField
          className='search-text-input'
          size='small'
          label={t('chatRoomName')}
          value={openChatRoomName}
          onChange={e => setOpenChatRoomName(e.target.value)}
        />
        <Button
          className='search-btn'
          variant='contained'
          type='submit'
          disabled={!openChatRoomName}
          style={{ height: '100%' }}
        >
          {t('search')}
        </Button>
      </form>

      {/* 검색 전 */}
      {!searchOpenChatRoomList && (
        <div className='no-room'>
          <Text>{t('pleaseSearchRooms')}</Text>
        </div>
      )}

      {/* 검색된 채팅방이 없을 때 */}
      {!!searchOpenChatRoomList && searchOpenChatRoomList.length === 0 && (
        <div className='no-room'>
          <Text>{t('noSearchChatRooms')}</Text>
        </div>
      )}

      {/* 검색된 채팅방 목록 */}
      {!!searchOpenChatRoomList && searchOpenChatRoomList.length > 0 && (
        <div>
          {searchOpenChatRoomList.map(d => createRoomInfoItem(d, d.uuid))}
        </div>
      )}

      {/* 오픈 채팅방 생성 모달 */}
      <Modal
        open={createChatRoomOpen}
        title={t('createOpenChatRoom')}
        type='confirm'
        onClose={() => setCreateChatRoomOpen(false)}
        onConfirm={handleClickCreateChatRoomConfirmButton}
      >
        <div className='create-chat-room-modal-body'>
          {/* 채팅방 이름 */}
          <div className='input-wrapper'>
            <Text>{t('chatRoomName')}</Text>
            <TextField
              error={inputInfo.newRoomName.error}
              helperText={t(inputInfo.newRoomName.helperText)}
              value={newRoomName}
              onChange={handleChangeNewRoomName}
              style={{ width: '100%', marginTop: '10px' }}
            />
          </div>

          {/* 최대 인원 수 */}
          <div className='input-wrapper'>
            <Text>{t('maxUserNum')}</Text>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: '0 10px',
                gap: '15px'
              }}
            >
              <Slider
                min={2}
                max={100}
                step={1}
                value={maxUser}
                onChange={(e, value) => setMaxUser(+value)}
                style={{ flex: 1 }}
              />
              <TextField
                size='small'
                value={maxUser}
                style={{ width: '60px' }}
                InputProps={{ readOnly: true }}
                inputProps={{ style: { textAlign: 'center' } }}
              />
            </div>
          </div>

          {/* 비밀번호 */}
          <div className='input-wrapper'>
            <Text>{t('password')}</Text>
            <TextField
              type='password'
              error={inputInfo.newRoomPassword.error}
              helperText={t(inputInfo.newRoomPassword.helperText)}
              value={newRoomPassword}
              onChange={e => setNewRoomPassword(e.target.value)}
              style={{ width: '100%', marginTop: '10px' }}
            />
          </div>

          {/* 공개 여부 */}
          <div className='input-wrapper'>
            <Text>{t('disclosureScope')}</Text>
            <FormControlLabel
              control={<Switch checked={!isPrivate} onChange={(e, checked) => setIsPrivate(!checked)}/>}
              label={<Text>{isPrivate ? t('private') : t('public')}</Text>}
              style={{ padding: '0 10px' }}
            />
          </div>
        </div>
      </Modal>

      {/* 오픈 체팅방 참가 확인 모달 */}
      {!!selectedRoom && (
        <>
          <Modal
            open={enterRoomModalOpen}
            type='confirm'
            title={selectedRoom.name}
            onConfirm={handleClickEnterRoomModalConfirmButton}
            onClose={() => setEnterRoomModalOpen(false)}
          >
            <div style={{ display: 'flex', flexDirection: 'column', gap: '15px', padding: '10px' }}>
              <Text>{t('enterOpenChatRoom')}</Text>
              {!!selectedRoom.password && (
                <TextField
                  label={t('password')}
                  type='password'
                  error={inputInfo.enterRoomPassword.error}
                  helperText={t(inputInfo.enterRoomPassword.helperText)}
                  value={enterRoomPassword}
                  onChange={e => setEnterRoomPassword(e.target.value)}
                  style={{ width: '100%' }}
                />
              )}
            </div>
          </Modal>

          {/* 인원 최대 에러 모달 */}
          <Modal
            open={maxUserErrorModalOpen}
            type='error'
            title={selectedRoom.name}
            onClose={() => setMaxUserErrorModalOpen(false)}
          >
            <div style={{ margin: '10px 0' }}>
              <Text>{t('maxUserRoom')}</Text>
            </div>
          </Modal>
        </>
      )}
    </Container>
  );
};

export default OpenCaht;