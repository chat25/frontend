import { useCallback, useLayoutEffect, useState } from 'react';
import {
  Avatar,
  Badge,
  Button,
  Checkbox,
  FormControlLabel,
  IconButton,
  ListItemButton,
  Menu,
  MenuItem,
  Switch,
  TextField,
  Tooltip
} from '@mui/material';
import styled from 'styled-components';
import moment from 'moment';
import * as uuid from 'uuid';
import _ from 'lodash';
import AddIcon from '@mui/icons-material/Add';
import useTranslation from '@/hooks/useTranslation';
import Text from '@/components/common/Text';
import { CREATE_GROUP_CHAT_ROOM, ENTER_OPEN_CHAT_ROOM, GET_ROOM_INFO } from '@/gql/room';
import mainPageVar, { setPageType, setRoomInfo } from '@/gql/vars/mainPage';
import { useReactiveVar } from '@apollo/client';
import commonState from '@/gql/vars/common';
import userState from '@/gql/vars/user';
import drawerState from '@/gql/vars/drawer';
import Modal from '@/components/common/Modal';
import InfiniteScroll from '@/components/common/InfiniteScroll';
import { SEARCH_FRIENDS_INFO } from '@/gql/friends';
import useApollo from '@/hooks/useApollo';

interface SearchFriendItem extends Friend {
  checked: boolean
}

const Container = styled.div`
  flex: 1;

  .divider {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 3px 15px;
    background-color: ${({ theme }) => theme === 'light' ? '#1976d2' : '#1c1c1c'};
    user-select: none;

    .divider-title {
      color: #fff !important;
      z-index: 1;
    }

    .divider-icon {
      fill: #fff;
    }
  }

  .chat-room-item-btn {
    padding: 0 15px 0 0;

    .chat-room-item {
      display: flex;
      align-items: center;
      width: 100%;
      max-width: 100%;
    }

    .chat-room-img-group {
      display: flex;
      flex-direction: column;
      justify-content: center;
      width: 55px;
      height: 55px;
      margin: 15px;
      gap: 3px;

      .chat-room-img {
        width: 55px;
        height: 55px;
      }

      .chat-room-img-group-row {
        display: flex;
        justify-content: center;
        gap: 3px;

        .chat-room-img-small {
          width: 24.5px;
          height: 24.5px;
        }
      }
    }

    .chat-room-right {
      flex: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
      gap: 5px;
      max-width: calc(100% - 85px);

      .room-name-wrapper {
        .room-name {
          font-size: 18px;
          font-weight: bold;
          white-space: nowrap;
          text-overflow: ellipsis;
          overflow: hidden;
        }
      }

      .last-msg-wrapper {
        .last-msg {
          font-size: 14px;
          white-space: nowrap;
          text-overflow: ellipsis;
          overflow: hidden;
        }
      }
    }
  }

  .create-room-modal-body {
    display: flex;
    flex-direction: column;
    gap: 25px;

    .input-wrapper {
      display: flex;
      gap: 10px;
      width: 100%;
    }

    .search-user-list {
      height: 300px;
      overflow-y: auto;

      .search-user-item {
        display: flex;
        align-items: center;
        gap: 15px;
      }
    }
  }
`;

/** Drawer 내 채팅방 리스트 */
const MyRooms = () => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [createGroupRoomModalOpen, setCreateGroupRoomModalOpen] = useState<boolean>(false);
  const [searchNickname, setSearchNickname] = useState<string>('');
  const [originSearchNickname, setOriginSearchNickname] = useState<string>('');
  const [searchUserList, setSearchUserList] = useState<SearchFriendItem[] | null>(null);
  const [joinOpenChatRoomModalOpen, setJoinOpenChatRoomModalOpen] = useState<boolean>(false);
  const [joinOpenChatRoomCode, setJoinOpenChatRoomCode] = useState<string>('');
  const [openChatRoomPasswordUsage, setOpenChatRoomPasswordUsage] = useState<boolean>(false);
  const [joinOpenChatRoomPassword, setJoinOpenChatRoomPassword] = useState<string>('');
  const [alertModalOpen, setAlertModalOpen] = useState<boolean>(false);
  const [alertModalContents, setAlertModalContents] = useState<string>('');

  const { theme } = useReactiveVar(commonState.getVar());
  const { userInfo } = useReactiveVar(userState.getVar());
  const { friendsList, roomList, noReadNum } = useReactiveVar(drawerState.getVar());
  const { roomInfo } = useReactiveVar(mainPageVar);

  const t = useTranslation();
  const apollo = useApollo();

  /** 그룹 채팅방 추가 메뉴 버튼 클릭 이벤트 */
  const handleClickAddGroupChatRoomButton = useCallback(() => {
    setAnchorEl(null);
    setCreateGroupRoomModalOpen(true);
    setSearchUserList(_.cloneDeep(friendsList.map(d => ({ ...d, checked: false }))));
    setSearchNickname('');
    setOriginSearchNickname('');
  }, [friendsList]);

  /** 오픈 채팅방 참가 메뉴 버튼 클릭 이벤트 */
  const handleClickJoinOpenChatRoomButton = useCallback(() => {
    setAnchorEl(null);
    setJoinOpenChatRoomModalOpen(true);
    setJoinOpenChatRoomCode('');
    setOpenChatRoomPasswordUsage(false);
    setJoinOpenChatRoomPassword('');
  }, []);

  /** 채팅방 리스트 아이템 클릭 이벤트 */
  const handleClickRoomListItem = useCallback(async (roomUuid: string) => {
    if (roomInfo && roomInfo.uuid === roomUuid) return;

    try {
      const data = await apollo.query<{ getRoomInfo: GetRoomInfoResult }, GetRoomInfoArgs>({
        query: GET_ROOM_INFO,
        variables: { roomUuid }
      });

      const { result, errCode } = data.getRoomInfo;

      if (errCode) throw errCode;

      console.log(data.getRoomInfo);

      setPageType(`${result.type}chat`);
      setRoomInfo(result);
    } catch (err) {
      console.error(err);
    }
  }, [roomInfo, apollo]);

  /** 채팅방 아이템 생성 */
  const createRoomInfoItem = useCallback((roomListItem: RoomListItem, key?: string) => (
    <Tooltip
      key={key}
      title={`${t('lastMessageDate')}: ${moment(roomListItem.lastChat.sendDate).format('YYYY-MM-DD HH:mm')}`}
      placement='right'
      arrow
    >
      <ListItemButton className='chat-room-item-btn' onClick={() => handleClickRoomListItem(roomListItem.uuid)}>
        <div className='chat-room-item'>
          <div className='chat-room-img-group'>
            {/* 개인채팅방 이미지 */}
            {roomListItem.type === 'personal' && (
              <Badge
                badgeContent={noReadNum[roomListItem.uuid]}
                componentsProps={{ badge: { style: { backgroundColor: '#f00', color: '#fff' } } }}
              >
                <Avatar
                  className='chat-room-img'
                  src={_.find(roomListItem.users, d => d.uuid !== userInfo?.uuid)?.profileImage}
                />
              </Badge>
            )}

            {/* 그룹, 오픈 채팅방 1인 */}
            {roomListItem.type !== 'personal' && roomListItem.users.length === 1 && (
              <Badge
                badgeContent={noReadNum[roomListItem.uuid]}
                componentsProps={{ badge: { style: { backgroundColor: '#f00', color: '#fff' } } }}
              >
                <Avatar
                  className='chat-room-img'
                  src={roomListItem.users[0].profileImage}
                />
              </Badge>
            )}

            {/* 그룹, 오픈 채팅방 2인 */}
            {roomListItem.type !== 'personal' && roomListItem.users.length === 2 && (
              <Badge
                badgeContent={noReadNum[roomListItem.uuid]}
                componentsProps={{ badge: { style: { backgroundColor: '#f00', color: '#fff' } } }}
                sx={{ flexDirection: 'column' }}
              >
                <div
                  className='chat-room-img-group-row'
                  style={{ justifyContent: 'flex-start' }}
                >
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[0].profileImage}
                  />
                </div>

                <div
                  className='chat-room-img-group-row'
                  style={{ justifyContent: 'flex-end' }}
                >
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[1].profileImage}
                  />
                </div>
              </Badge>
            )}

            {/* 그룹, 오픈 채팅방 3인 이상 */}
            {roomListItem.type !== 'personal' && roomListItem.users.length > 2 && (
              <Badge
                badgeContent={noReadNum[roomListItem.uuid]}
                componentsProps={{ badge: { style: { backgroundColor: '#f00', color: '#fff' } } }}
                sx={{ flexDirection: 'column' }}
              >
                <div className='chat-room-img-group-row'>
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[0].profileImage}
                  />
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[1].profileImage}
                  />
                </div>

                <div className='chat-room-img-group-row'>
                  <Avatar
                    className='chat-room-img-small'
                    src={roomListItem.users[2].profileImage}
                  />
                  {!!roomListItem.users[3] && (
                    <Avatar
                      className='chat-room-img-small'
                      src={roomListItem.users[3].profileImage}
                    />
                  )}
                </div>
              </Badge>
            )}
          </div>

          {/* 채팅방 이름, 마지막 메시지 */}
          <div className='chat-room-right'>
            <div className='room-name-wrapper'>
              <Text className='room-name'>
                {(() => {
                  if (roomListItem.name) return roomListItem.name;
                  if (roomListItem.type === 'personal') return roomListItem.users.map(d => d.nickname).filter(d => !!d && d !== userInfo?.nickname).join(', ');
                  return roomListItem.users.map(d => d.nickname).filter(d => !!d).join(', ');
                })()}
              </Text>
            </div>
            <div className='last-msg-wrapper'>
              <Text className='last-msg'>
                {roomListItem.lastChat.type === 'text' && roomListItem.joinDate < roomListItem.lastChat.sendDate ? roomListItem.lastChat.contents : ''}
                {roomListItem.lastChat.type !== 'text' && t(roomListItem.lastChat.type)}
              </Text>
            </div>
          </div>
        </div>
      </ListItemButton>
    </Tooltip>
  ), [userInfo, noReadNum, t, handleClickRoomListItem]);

  /** 친구 검색 버튼 클릭 이벤트 */
  const handleClickSearchButton: React.MouseEventHandler<HTMLButtonElement> = useCallback(async e => {
    e.preventDefault();

    try {
      const data = await apollo.query<{ getFriendsInfo: GetFriendsInfoResult }, GetFriendsInfoArgs>({
        query: SEARCH_FRIENDS_INFO,
        variables: { searchNickname, lastNickname: '' }
      });

      const { result, errCode } = data.getFriendsInfo;

      if (errCode) throw errCode;

      setSearchUserList(result.friends.map(d => ({ ...d, checked: false })));
      setOriginSearchNickname(searchNickname);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, searchNickname]);

  /** 친구 검색 목록 Scroll End 이벤트 */
  const handleSearchUserScrollEnd = useCallback(async () => {
    if (!originSearchNickname || !searchUserList) return;

    try {
      const data = await apollo.query<{ getFriendsInfo: GetFriendsInfoResult }, GetFriendsInfoArgs>({
        query: SEARCH_FRIENDS_INFO,
        variables: {
          searchNickname: originSearchNickname,
          lastNickname: searchUserList[searchUserList.length - 1].nickname
        }
      });

      const { result, errCode } = data.getFriendsInfo;

      if (errCode) throw errCode;

      setSearchUserList([...searchUserList, ...result.friends.map(d => ({ ...d, checked: false }))]);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, originSearchNickname, searchUserList]);

  /** 검색한 친구 프로필 클릭 이벤트 */
  const handleClickProfile = useCallback((uuid: string) => {
    const searchUserListClone = _.cloneDeep(searchUserList);
    const profileItem = _.find(searchUserListClone, { uuid });
    if (!profileItem) return;

    profileItem.checked = !profileItem.checked;
    setSearchUserList(searchUserListClone);
  }, [searchUserList]);

  /** 채팅방 생성 모달 초대 버튼 클릭 이벤트 */
  const handleClickCreateRoomModalConfirmButton = useCallback(async () => {
    if (!searchUserList) return;

    const selectedFriends = searchUserList.filter(d => d.checked).map(d => d.uuid);
    console.log(selectedFriends);

    if (selectedFriends.length === 0) {
      setAlertModalContents('selectFriends');
      setAlertModalOpen(true);
      return;
    }

    try {
      const data = await apollo.mutate<{ createRoom: CreateRoomResult }, Partial<CreateRoomArgs>>({
        mutation: CREATE_GROUP_CHAT_ROOM,
        variables: { users: selectedFriends }
      });

      const { result, errCode } = data.createRoom;

      if (errCode) throw errCode;

      if (!result) new Error('UNKNOWN_ERROR');

      setCreateGroupRoomModalOpen(false);
      handleClickRoomListItem(result);
    } catch (err) {
      console.error(err);
    }
  }, [apollo, searchUserList, handleClickRoomListItem]);

  /** 오픈 채팅방 참가 모달 확인 버튼 클릭 이벤트 */
  const handleClickJoinChatRoomModalConfirmButton = useCallback(async () => {
    if (!userInfo || !userInfo.uuid) return;

    try {
      const data1 = await apollo.mutate<{ enterRoom: EnterRoomResult }, EnterRoomArgs>({
        mutation: ENTER_OPEN_CHAT_ROOM,
        variables: {
          roomUuid: joinOpenChatRoomCode,
          userUuid: [userInfo.uuid],
          password: joinOpenChatRoomPassword
        }
      });

      const { errCode: errCode1 } = data1.enterRoom;

      if (errCode1) {
        if (errCode1 === 'INVALID_USER') {
          setAlertModalContents('alreadyEnterRoom');
          return setAlertModalOpen(true);
        }
        if (errCode1 === 'MAX_USER') {
          setAlertModalContents('maxUserRoom');
          return setAlertModalOpen(true);
        }
        if (errCode1 === 'WRONG_PASSWORD') {
          setAlertModalContents('wrongPassword');
          return setAlertModalOpen(true);
        }
        if (errCode1 === 'SERVER_ERROR') {
          setAlertModalContents('cannotEnterChatRoom');
          return setAlertModalOpen(true);
        }
        throw errCode1;
      }

      const data2 = await apollo.query<{ getRoomInfo: GetRoomInfoResult }, GetRoomInfoArgs>({
        query: GET_ROOM_INFO,
        variables: { roomUuid: joinOpenChatRoomCode }
      });

      const { result, errCode: errCode2 } = data2.getRoomInfo;

      if (errCode2) throw errCode2;

      setPageType(`${result.type}chat`);
      setRoomInfo(result);
      setJoinOpenChatRoomModalOpen(false);
      setAlertModalOpen(false);
    } catch (err) {
      console.error(err);
    }
  }, [userInfo, apollo, joinOpenChatRoomCode, joinOpenChatRoomPassword]);

  /** 사파리 position: fixed, overflow: hidden 잘림 현상 조치 */
  useLayoutEffect(() => {
    const drawerItems = window.document.getElementById('drawer-items');
    if (!drawerItems) return;

    if (createGroupRoomModalOpen || joinOpenChatRoomModalOpen || alertModalOpen) {
      drawerItems.style.overflow = 'hidden';
      return;
    }

    const timeout = setTimeout(() => drawerItems.removeAttribute('style'), 200);

    return () => {
      clearTimeout(timeout);
    };
  }, [createGroupRoomModalOpen, joinOpenChatRoomModalOpen, alertModalOpen]);

  return (
    <Container theme={theme}>
      {/* 내 채팅방 */}
      <div className='divider'>
        <Text className='divider-title'>{t('myRooms')}</Text>
        <IconButton style={{ padding: '2px' }} onClick={e => setAnchorEl(e.currentTarget)}>
          <AddIcon className='divider-icon' style={{ width: '20px', height: '20px' }}/>
        </IconButton>
      </div>

      {roomList.map(d => createRoomInfoItem(d, uuid.v4()))}

      {/* 채팅방 추가 메뉴 */}
      <Menu
        anchorEl={anchorEl}
        open={!!anchorEl}
        onClose={() => setAnchorEl(null)}
      >
        {[
          // 그룹 채팅방 생성
          <MenuItem key={uuid.v4()} onClick={handleClickAddGroupChatRoomButton}>
            <Text variant='body1'>{t('createGroupChatRoom')}</Text>
          </MenuItem>,
          // 오픈 채팅방 참가
          <MenuItem key={uuid.v4()} onClick={handleClickJoinOpenChatRoomButton}>
            <Text variant='body1'>{t('joinOpenChatRoom')}</Text>
          </MenuItem>
        ]}
      </Menu>

      {/* 채팅방 생성 모달 */}
      <Modal
        open={createGroupRoomModalOpen}
        title={t('createGroupChatRoom')}
        type='confirm'
        confirmText={t('invite')}
        onConfirm={handleClickCreateRoomModalConfirmButton}
        onClose={() => setCreateGroupRoomModalOpen(false)}
      >
        <div className='create-room-modal-body'>
          {/* 친구 검색 Form */}
          <form className='input-wrapper' action='#'>
            <TextField
              label={t('nickname')}
              value={searchNickname}
              onChange={e => setSearchNickname(e.target.value)}
              style={{ flex: 1 }}
            />
            <Button
              variant='contained'
              type='submit'
              onClick={handleClickSearchButton}
            >
              {t('search')}
            </Button>
          </form>

          {/* 친구 검색 전 */}
          {!searchUserList && (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '300px' }}>
              <Text>{t('pleaseSearchFriends')}</Text>
            </div>
          )}

          {/* 친구 검색 후 */}
          {!!searchUserList && (searchUserList.length > 0 ? (
            <InfiniteScroll
              className='search-user-list'
              onScrollEnd={handleSearchUserScrollEnd}
            >
              {searchUserList.map(d => (
                <ListItemButton
                  key={uuid.v4()}
                  className='search-user-item'
                  onClick={() => handleClickProfile(d.uuid)}
                >
                  <Checkbox checked={d.checked} onChange={() => handleClickProfile(d.uuid)}/>
                  <Avatar src={d.profileImage}/>
                  <Text>{d.nickname}</Text>
                </ListItemButton>
              ))}
            </InfiniteScroll>
          ) : (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '300px' }}>
              <Text>{t('noSearchUser')}</Text>
            </div>
          ))}
        </div>
      </Modal>

      {/* 오픈 채팅방 참가 모달 */}
      <Modal
        open={joinOpenChatRoomModalOpen}
        type='confirm'
        title={t('joinOpenChatRoom')}
        onConfirm={handleClickJoinChatRoomModalConfirmButton}
        onClose={() => setJoinOpenChatRoomModalOpen(false)}
      >
        <div style={{ display: 'flex', flexDirection: 'column', gap: '15px', padding: '10px' }}>
          {/* 오픈 채팅방 코드 (UUID) */}
          <TextField
            label={t('openChatRoomCode')}
            value={joinOpenChatRoomCode}
            onChange={e => setJoinOpenChatRoomCode(e.target.value)}
            style={{ width: '100%' }}
          />

          {/* 오픈 채팅방 비밀번호 */}
          <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <TextField
              label={t('password')}
              type='password'
              disabled={!openChatRoomPasswordUsage}
              value={joinOpenChatRoomPassword}
              onChange={e => setJoinOpenChatRoomPassword(e.target.value)}
              style={{ width: '100%' }}
            />
            <FormControlLabel
              control={(
                <Switch
                  checked={openChatRoomPasswordUsage}
                  onChange={() => {
                    setOpenChatRoomPasswordUsage(!openChatRoomPasswordUsage);
                    setJoinOpenChatRoomPassword('');
                  }}
                />
              )}
              label={<Text style={{ wordBreak: 'keep-all' }}>{t('use')}</Text>}
            />
          </div>
        </div>
      </Modal>

      {/* Alert 모달 */}
      <Modal
        open={alertModalOpen}
        type='alert'
        title={t('createGroupChatRoom')}
        closeButton={false}
        onConfirm={() => setAlertModalOpen(false)}
      >
        <div style={{ margin: '10px 0' }}>
          <Text>{t(alertModalContents)}</Text>
        </div>
      </Modal>
    </Container>
  );
};

export default MyRooms;