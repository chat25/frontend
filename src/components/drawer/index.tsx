import React, { useCallback, useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import ChatBubbleIcon from '@mui/icons-material/ChatBubble';
import ForumIcon from '@mui/icons-material/Forum';
import SettingsIcon from '@mui/icons-material/Settings';
import _ from 'lodash';
import ReplayRoundedIcon from '@mui/icons-material/ReplayRounded';
import { Badge, Button, FormControlLabel, IconButton, Radio, RadioGroup, TextField, Tooltip } from '@mui/material';
import useTranslation from '@/hooks/useTranslation';
import { useNavigate, useLocation } from 'react-router';
import Modal from '@/components/common/Modal';
import validate from '@/utils/validate';
import Timer from '@/components/common/Timer';
import { CHECK_EMAIL_AUTHCODE, CHECK_PHONE_AUTHCODE, EMAIL_VERIFICATION, PHONE_VERIFICATION } from '@/gql/user';
import { GET_FRIENDS_INFO } from '@/gql/friends';
import InfiniteScroll from '@/components/common/InfiniteScroll';
import { useReactiveVar } from '@apollo/client';
import commonState from '@/gql/vars/common';
import userState from '@/gql/vars/user';
import drawerState, { getOpenChatRoomList, setDrawerOpen, setFriendsList, setFriendsRequest, setSettingsAuthInfo } from '@/gql/vars/drawer';
import mainPageVar, { setPageType } from '@/gql/vars/mainPage';
import Backdrop from '@/components/common/Backdrop';
import useApollo from '@/hooks/useApollo';

interface DrawerProps {
  open: boolean
}

const Container = styled.div<DrawerProps>`
  position: relative;
  display: flex;
  flex-direction: column;
  width: ${({ open }) => open ? '300px' : '0'};
  min-height: 100%;
  max-width: 100%;
  left: ${({ open }) => open ? '0' : '-300px'};
  border-right: 1px solid ${({ theme }) => theme === 'light' ? '#bbb' : '#272727'};
  background-color: ${({ theme }) => theme === 'light' ? '#fff' : '#2b2b2b'};
  transition: ${({ open }) => open ? 'left 0.4s ease-out, width 0.3s ease-in' : 'left 0.3s ease-in, width 0.4s ease-out'};
  z-index: 10;

  .drawer-items {
    flex: 1;
    width: 300px;
    max-width: 100%;
    overflow-x: hidden;
    overflow-y: auto;
    background-color: ${({ theme }) => theme === 'light' ? '#fff' : '#2b2b2b'};
    z-index: 10;
  }

  .drawer-menu {
    display: flex;
    justify-content: space-around;
    align-items: center;
    min-width: 299px;
    max-width: 100%;
    border-top: 1px solid ${({ theme }) => theme === 'light' ? '#bbb' : '#222'};
    background-color: ${({ theme }) => theme === 'light' ? '#fff' : '#262626'};
    overflow: hidden;
    z-index: 9;

    .menu-icon-btn {
      margin: 5px;
      padding: 10px;
      
      .menu-icon {
        width: 35px;
        height: 35px;
        fill: ${({ theme }) => theme === 'light' ? '#3f3f3f' : '#ccc'};
      }
    }
  }

  .settings-auth-modal {
    display: flex;
    flex-direction: column;
    gap: 15px;

    .radio-label {
      color: ${({ theme }) => theme === 'light' ? '#000' : '#fff'}
    }

    .authcode-input-wrapper {
      display: flex;
      gap: 10px;

      .refresh-auth-btn {
        width: 35px;
        height: 35px;
        margin-top: 11px;

        .reset-auth-icon {
          fill: ${({ theme }) => theme === 'light' ? '#000' : '#fff'};
        }
      }
    }
  }
`;

/** Common Drawer Component */
const Drawer: React.FC<{ children?: React.ReactNode }> = ({ children }) => {
  const [containerHeight, setContainerHeight] = useState<number>(0);
  const [containerOverflow, setContainerOverflow] = useState<'hidden' | undefined>('hidden');
  const [settingsAuthModalOpen, setSettingsAuthModalOpen] = useState<boolean>(false);
  const [authMethod, setAuthMethod] = useState<'EMAIL' | 'PHONE'>('EMAIL');
  const [isSendAuthCode, setIsSendAuthCode] = useState<boolean>(false);
  const [authCode, setAuthCode] = useState<string>('');
  const [authCodeUuid, setAuthCodeUuid] = useState<string>('');
  const [canResetAuthCode, setCanResetAuthCode] = useState<boolean>(false);
  const [backdropDisplay, setBackdropDisplay] = useState<'none' | undefined>('none');
  const [backdropOpacity, setBackdropOpacity] = useState<number>(0);
  const [inputInfo, setInputInfo] = useState({
    authCode: { error: false, helperText: '' }
  });

  const { theme, lang } = useReactiveVar(commonState.getVar());
  const { userInfo } = useReactiveVar(userState.getVar());
  const {
    drawerOpen,
    friendsRequest,
    friendsList,
    noReadNum,
    originOpenChatRoomName,
    searchOpenChatRoomList,
    searchOpenChatRoomPage
  } = useReactiveVar(drawerState.getVar());
  const { pageType, roomInfo } = useReactiveVar(mainPageVar);

  const t = useTranslation();
  const navigate = useNavigate();
  const location = useLocation();
  const apollo = useApollo();

  /** Drawer 메뉴 변경 */
  const changeDrawerMenu = useCallback(async (menu: DrawerMenu) => {
    if (menu === 'settings' && pageType !== 'settings') {
      setAuthMethod('EMAIL');
      setIsSendAuthCode(false);
      setAuthCode('');
      return setSettingsAuthModalOpen(true);
    }

    navigate(`/${menu}`, { replace: true });
  }, [pageType, navigate]);

  /** 설정 본인 인증 모달 닫기 */
  const handleCloseSettingsAuthModal = useCallback(() => {
    setSettingsAuthModalOpen(false);
  }, []);

  /** 인증코드 발송 버튼 클릭 이벤트 */
  const handleClickSendAuthCodeButton = useCallback(async () => {
    if (!userInfo) return;

    const inputInfoClone = _.cloneDeep(inputInfo);

    try {
      if (authMethod === 'EMAIL') {
        const data = await apollo.query<{ emailVerification: EmailVerificationResult }, EmailVerificationArgs>({
          query: EMAIL_VERIFICATION,
          variables: {
            email: userInfo.email,
            lang,
            checkDuplicate: false
          }
        });

        if (data.emailVerification.errCode) {
          if (data.emailVerification.errCode !== 'DUPLICATE_ERROR') {
            throw data.emailVerification.errCode;
          }
        }

        const { emailVerification: { result } } = data;
        inputInfoClone.authCode = { error: false, helperText: '' };

        setAuthCodeUuid(result);
        setInputInfo(inputInfoClone);
      } else {
        const data = await apollo.query<{ phoneVerification: PhoneVerificationResult }, PhoneVerificationArgs>({
          query: PHONE_VERIFICATION,
          variables: {
            countryCode: userInfo.countryCode,
            phone: userInfo.phone,
            lang,
            checkDuplicate: false
          }
        });

        if (data.phoneVerification.errCode) {
          if (data.phoneVerification.errCode !== 'DUPLICATE_ERROR') {
            throw data.phoneVerification.errCode;
          }
        }

        const { phoneVerification: { result } } = data;
        inputInfoClone.authCode = { error: false, helperText: '' };

        setAuthCodeUuid(result);
        setInputInfo(inputInfoClone);
      }

      setIsSendAuthCode(true);
    } catch (err) {
      console.error(err);
    }

    setCanResetAuthCode(false);
    setIsSendAuthCode(true);
  }, [lang, userInfo, authMethod, inputInfo, apollo]);

  /** 인증코드 변경 이벤트 */
  const handleChangeAuthCode = useCallback((e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { value } = e.target;
    const isNumberString = validate('numberstring', value);
    if ((value !== '' && !isNumberString) || value.length > 6) return;

    const inputInfoClone = _.cloneDeep(inputInfo);
    inputInfoClone.authCode = { error: false, helperText: '' };

    setAuthCode(e.target.value);
    setInputInfo(inputInfoClone);
  }, [inputInfo]);

  /** 인증코드 확인 버튼 클릭 이벤트 */
  const handleClickVerifyAuthCode = useCallback(async () => {
    if (!userInfo) return;

    const inputInfoClone = _.cloneDeep(inputInfo);

    try {
      if (authMethod === 'EMAIL') {
        const data = await apollo.mutate<{ checkEmailAuthCode: CheckEmailAuthCodeResult }, CheckEmailAuthCodeArgs>({
          mutation: CHECK_EMAIL_AUTHCODE,
          variables: {
            email: userInfo.email,
            authCode,
            uuid: authCodeUuid
          }
        });

        if (data.checkEmailAuthCode.errCode) {
          if (data.checkEmailAuthCode.errCode === 'INVALID_AUTH_CODE') {
            inputInfoClone.authCode = { error: true, helperText: 'wrongAuthCode' };
            setInputInfo(inputInfoClone);
          }
          throw data.checkEmailAuthCode.errCode;
        }

        if (data.checkEmailAuthCode.result.success) {
          setSettingsAuthModalOpen(false);
          setSettingsAuthInfo(authMethod, authCodeUuid);
          console.log({ authMethod, authCodeUuid });
          setPageType('settings');
        }
      } else {
        const data = await apollo.mutate<{ checkPhoneAuthCode: CheckPhoneAuthCodeResult }, CheckPhoneAuthCodeArgs>({
          mutation: CHECK_PHONE_AUTHCODE,
          variables: {
            countryCode: userInfo.countryCode,
            phone: userInfo.phone,
            authCode,
            uuid: authCodeUuid
          }
        });

        if (data.checkPhoneAuthCode.errCode) {
          if (data.checkPhoneAuthCode.errCode === 'INVALID_AUTH_CODE') {
            inputInfoClone.authCode = { error: true, helperText: 'wrongAuthCode' };
            setInputInfo(inputInfoClone);
          }
          throw data.checkPhoneAuthCode.errCode;
        }

        if (data.checkPhoneAuthCode.result.success) {
          setSettingsAuthModalOpen(false);
          setSettingsAuthInfo(authMethod, authCodeUuid);
          console.log({ authMethod, authCodeUuid });
          setPageType('settings');
        }
      }
    } catch (err) {
      console.error(err);
    }
  }, [userInfo, authMethod, authCode, authCodeUuid, inputInfo, apollo]);

  /** Drawer Scroll End 이벤트 */
  const handleDrawerScrollEnd = useCallback(async () => {
    try {
      if (['/', '/friends'].indexOf(location.pathname) > -1) {
        if (!userInfo) return;

        const data = await apollo.query<{ getFriendsInfo: GetFriendsInfoResult }, GetFriendsInfoArgs>({
          query: GET_FRIENDS_INFO,
          variables: { lastNickname: friendsList[friendsList.length - 1].nickname }
        });

        const { result, errCode } = data.getFriendsInfo;

        if (errCode) throw errCode;

        if (result.friends.length === 0 && result.friendRequests.length === 0) return;

        setFriendsRequest(result.friendRequests);
        setFriendsList([...friendsList, ...result.friends]);
      } else if (location.pathname === '/openchat') {
        if (!searchOpenChatRoomList) return;
        getOpenChatRoomList(originOpenChatRoomName, searchOpenChatRoomPage + 1);
      }
    } catch (err) {
      console.error(err);
    }
  }, [userInfo, friendsList, location, apollo, searchOpenChatRoomList, originOpenChatRoomName, searchOpenChatRoomPage]);

  /** 인증 코드 초기화 버튼 클릭 이벤트 */
  const handleClickResetAuthButton = useCallback(() => {
    setAuthCode('');
    setIsSendAuthCode(false);
  }, []);

  /** 메시지를 읽지 않은 채팅방 UUID 리스트 */
  const noReadRoomUuidArr = useMemo(() => Object.keys(noReadNum), [noReadNum]);

  /** 인증코드 타이머 */
  const authTimer = useMemo(() => (
    <Timer
      seconds={300}
      onEnd={handleClickResetAuthButton}
      style={{ display: 'block', width: '41px' }}
    />
  ), [handleClickResetAuthButton]);

  /** 인증코드 전송 1분 후 초기화 버튼 활성 */
  useEffect(() => {
    let timeout: any;
    if (isSendAuthCode) timeout = setTimeout(() => {
      setCanResetAuthCode(true);
    }, 61000);

    return () => {
      clearTimeout(timeout);
    };
  }, [isSendAuthCode]);

  /** drawerOpen 변경 시 */
  useEffect(() => {
    if (drawerOpen) {
      setBackdropDisplay(undefined);
      setTimeout(() => setContainerOverflow(undefined), 300);
      setTimeout(() => setBackdropOpacity(1), 10);
    } else {
      setBackdropOpacity(0);
      setContainerOverflow('hidden');
      setTimeout(() => setBackdropDisplay('none'), 200);
    }
  }, [drawerOpen]);

  /** MainPage 변경 시 */
  useEffect(() => {
    if (document.body.clientWidth > 620) return;
    if (!!pageType) setDrawerOpen(false);
  }, [pageType, roomInfo?.uuid]);

  /** 컴포넌트 마운트 시 */
  useEffect(() => {
    setContainerHeight(window.innerHeight - 67);
    const onResize = () => setContainerHeight(window.innerHeight - 67);
    window.addEventListener('resize', onResize);
    window.addEventListener('orientationchange', onResize);

    return () => {
      window.removeEventListener('resize', onResize);
      window.removeEventListener('orientationchange', onResize);
    };
  }, []);

  return (
    <Container open={drawerOpen} theme={theme} style={{ height: containerHeight, overflow: containerOverflow }}>
      {/* Drawer 아이템 리스트 */}
      <InfiniteScroll
        id='drawer-items'
        className='drawer-items'
        onScrollEnd={handleDrawerScrollEnd}
      >
        {children}
      </InfiniteScroll>

      {/* Drawer 하단 메뉴 */}
      <div className='drawer-menu'>
        {/* 친구 목록 */}
        <Tooltip title={t('friendsList')}>
          <IconButton id='friends-list' className='menu-icon-btn' onClick={() => changeDrawerMenu('friends')}>
            <Badge
              badgeContent={friendsRequest.length}
              componentsProps={{ badge: { style: { backgroundColor: '#f00', color: '#fff' } } }}
            >
              <PeopleAltIcon className='menu-icon'/>
            </Badge>
          </IconButton>
        </Tooltip>

        {/* 내 채팅방 */}
        <Tooltip title={t('myRooms')}>
          <IconButton id='my-rooms' className='menu-icon-btn' onClick={() => changeDrawerMenu('rooms')}>
            <Badge
              badgeContent={noReadRoomUuidArr.length > 0 ? noReadRoomUuidArr.map(d => noReadNum[d]).reduce((d1, d2) => d1 + d2) : undefined}
              componentsProps={{ badge: { style: { backgroundColor: '#f00', color: '#fff' } } }}
            >
              <ChatBubbleIcon className='menu-icon'/>
            </Badge>
          </IconButton>
        </Tooltip>

        {/* 오픈 채팅 */}
        <Tooltip title={t('openChat')}>
          <IconButton id='open-chat' className='menu-icon-btn' onClick={() => changeDrawerMenu('openchat')}>
            <ForumIcon className='menu-icon'/>
          </IconButton>
        </Tooltip>

        {/* 설정 */}
        <Tooltip title={t('settings')}>
          <IconButton id='settings' className='menu-icon-btn' onClick={() => changeDrawerMenu('settings')}>
            <SettingsIcon className='menu-icon'/>
          </IconButton>
        </Tooltip>
      </div>

      {/* 본인 인증 모달 */}
      <Modal
        open={settingsAuthModalOpen}
        type='no-footer'
        title={t('identityVerification')}
        onClose={handleCloseSettingsAuthModal}
      >
        <div className='settings-auth-modal'>
          {/* 인증 수단 선택 */}
          <RadioGroup value={authMethod} onChange={(e, value: any) => setAuthMethod(value)}>
            <FormControlLabel
              className='radio-label'
              label={t('emailAuthentication')}
              value='EMAIL'
              disabled={isSendAuthCode}
              control={<Radio/>}
            />
            <FormControlLabel
              className='radio-label'
              label={t('phoneAuthentication')}
              value='PHONE'
              disabled={isSendAuthCode}
              control={<Radio/>}
            />
          </RadioGroup>

          {/* 인증코드 발송 버튼 */}
          {!isSendAuthCode && (
            <Button
              variant='contained'
              onClick={handleClickSendAuthCodeButton}
            >
              {t('sendAuthCode')}
            </Button>
          )}

          {isSendAuthCode && (
            <div className='authcode-input-wrapper'>
              {/* 인증 코드 */}
              <TextField
                label={t('authCode')}
                variant='outlined'
                value={authCode}
                error={inputInfo.authCode.error}
                helperText={t(inputInfo.authCode.helperText)}
                onChange={handleChangeAuthCode}
                style={{ flex: 1 }}
              />

              {/* 인증 확인 버튼 */}
              <Button
                variant='contained'
                onClick={handleClickVerifyAuthCode}
                style={{ height: '56px' }}
              >
                {t('verify')}
              </Button>

              {/* 인증코드 초기화 버튼 */}
              {canResetAuthCode && (
                <IconButton
                  className='refresh-auth-btn'
                  onClick={handleClickResetAuthButton}
                >
                  <ReplayRoundedIcon className='reset-auth-icon'/>
                </IconButton>
              )}

              <div style={{ paddingTop: '20px' }}>
                {authTimer}
              </div>
            </div>
          )}
        </div>
      </Modal>

      {document.body.clientWidth <= 620 && (
        <Backdrop
          onClick={() => setDrawerOpen(false)}
          style={{
            display: backdropDisplay,
            opacity: backdropOpacity
          }}
        />
      )}
    </Container>
  );
};

export default Drawer;